import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/reusableWidgets/bottomNavBar.dart';
import 'package:home/screens/settings_screen.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/images.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    initPrefs();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: kPrimaryColor,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: const [
              SizedBox(
                height: 40,
              ),
              CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor)
              ),
            ],
          ),
        ),
      ),
    );
  }
  
  initPrefs() async {
    final mqttProvider = Provider.of<MqttProvider>(context, listen: false);
    await mqttProvider.main(context);
    Future.delayed(const Duration(seconds: 3), () => _route(getUserToken()));
  }

  void _route(value) {
    //check user registered or not
    if (value != null) {
      if(getBatteryIsDeactivated() ?? false){
        Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsScreen()));
      }else{
        //start MQTT
        if(getLocationId() != -1 && getIsPowerCellIPaired()) {
          final mqttProvider = Provider.of<MqttProvider>(context, listen: false);
          mqttProvider.main(context);
        }
        Navigator.push(context, MaterialPageRoute(builder: (context) => BottomNavBar()));
      }

    } else {
      Navigator.push(context, MaterialPageRoute(builder: (context) => BottomNavBar()));
    }
  }
}
