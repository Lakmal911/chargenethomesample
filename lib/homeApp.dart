import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:home/splash_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';

class HomeApp extends StatelessWidget {
  const HomeApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizerUtil.setScreenSize(constraints, orientation);
            return MaterialApp(
              title: lAppName,
              theme: ThemeData(
                  primaryColor: kPrimaryColor,
                  scaffoldBackgroundColor: kScaffoldBackgroundColor,
                  canvasColor: kPrimaryColor,
                  // fontFamily: "Gilroy",
                  pageTransitionsTheme: const PageTransitionsTheme(
                  builders: {
                    TargetPlatform.android: SharedAxisPageTransitionsBuilder(
                      transitionType: SharedAxisTransitionType.horizontal,
                    ),
                    TargetPlatform.iOS: SharedAxisPageTransitionsBuilder(
                      transitionType: SharedAxisTransitionType.horizontal,
                    ),
                  },
                ),
              ),
              home: const SplashScreen(),
            );
          }
        );
      }
    );
  }
}