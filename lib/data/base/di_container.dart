import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:home/data/repository/dio/dio_client.dart';
import 'package:home/data/repository/dio/logging_interceptor.dart';
import 'package:home/providers/batteryCellProvider.dart';
import 'package:home/providers/deviceProvider.dart';
import 'package:home/providers/locationProvider.dart';
import 'package:home/providers/userProvider.dart';
import 'package:home/services/batteryCellService.dart';
import 'package:home/services/deviceService.dart';
import 'package:home/services/locationService.dart';
import 'package:home/services/userService.dart';

import '../../utils/base.dart';

//sl = service locator di = dependency injection
final sl = GetIt.instance;

//initialize services
Future<void> init() async {
  // Core ex - network, inputs...etc
  sl.registerLazySingleton(() => DioClient(Base.baseURL, sl(), loggingInterceptor: sl(), sharedPreferences: sl()));

  // External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => Dio());
  sl.registerLazySingleton(() => LoggingInterceptor());

  // Repository
  // Data sources
  // Use cases

  //feature - providers
  sl.registerFactory(() => UserProvider(userService: UserService(dioClient: sl())));
  sl.registerFactory(() => UserService(dioClient: sl()));

  sl.registerFactory(() => LocationProvider(locationService: LocationService(dioClient: sl())));
  sl.registerFactory(() => LocationService(dioClient: sl()));

  sl.registerFactory(() => DeviceProvider(deviceService: DeviceService(dioClient: sl())));
  sl.registerFactory(() => DeviceService(dioClient: sl()));

  sl.registerFactory(() => BatteryCellProvider(batteryCellService: BatteryCellService(dioClient: sl())));
  sl.registerFactory(() => BatteryCellService(dioClient: sl()));
}