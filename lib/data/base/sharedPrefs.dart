import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:home/utils/app_constants.dart';

final SharedPreferences prefs = GetIt.instance();

void saveDeviceRegistered(bool value){
  prefs.setBool(lDeviceRegistered, value);
}

bool getDeviceRegistered() {
  return prefs.getBool(lDeviceRegistered) ?? false;
}

void saveUserToken(String value){
  prefs.setString(lToken, value);
}

String? getUserToken() {
  return prefs.getString(lToken);
}

void saveUserLoginId(int value){
  prefs.setInt(lUserLoginId, value);
}

int getUserLoginId() {
  return prefs.getInt(lUserLoginId) ?? -1;
}

void saveUserId(int value){
  prefs.setInt(lUserId, value);
}

int getUserId() {
  return prefs.getInt(lUserId) ?? -1;
}

void saveUserName(String value){
  prefs.setString(lName, value);
}

String? getUserName() {
  return prefs.getString(lName);
}

void saveBatteryId(int value){
  prefs.setInt(lBatteryId, value);
}

int getBatteryId() {
  return prefs.getInt(lBatteryId) ?? -1;
}

void savePowerCellSerialNumber(String value){
  prefs.setString(lPowerCellSerialNumber, value);
}

String? getPowerCellSerialNumber() {
  return prefs.getString(lPowerCellSerialNumber);
}

void saveIsPowerCellIPaired(bool value){
  prefs.setBool(lIsPowerCellPaired, value);
}

bool getIsPowerCellIPaired() {
  return prefs.getBool(lIsPowerCellPaired) ?? false;
}

void saveLocationId(int value){
  prefs.setInt(lLocationId, value);
}

int getLocationId() {
  return prefs.getInt(lLocationId) ?? -1;
}

void saveLocationName(String value){
  prefs.setString(lLocationName, value);
}

String? getLocationName() {
  return prefs.getString(lLocationName);
}

void saveBatteryIsDeactivated(bool value){
  prefs.setBool(lBatteryIsDeactivated, value);
}

bool? getBatteryIsDeactivated() {
  return prefs.getBool(lBatteryIsDeactivated);
}

//Battery info
void saveCommandStatus(String value){
  prefs.setString("commandStatus", value);
}
String? getCommandStatus() {
  return prefs.getString("commandStatus");
}

void saveCurrent(String value){
  prefs.setString("current", value);
}
String? getCurrent() {
  return prefs.getString("current");
}

void saveVoltage(String value){
  prefs.setString("voltage", value);
}
String? getVoltage() {
  return prefs.getString("voltage");
}

void savePower(String value){
  prefs.setString("power", value);
}
String? getPower() {
  return prefs.getString("power");
}

void saveTemperature(String value){
  prefs.setString("temperature", value);
}
String? getTemperature() {
  return prefs.getString("temperature");
}

void saveSOC(String value){
  prefs.setString("soc", value);
}
String? getSOC() {
  return prefs.getString("soc");
}

void saveSOH(String value){
  prefs.setString("soh", value);
}
String? getSOH() {
  return prefs.getString("soh");
}

void saveEnergy(String value){
  prefs.setString("energy", value);
}
String? getEnergy() {
  return prefs.getString("energy");
}

void saveChargeDuration(String value){
  prefs.setString("chargeDuration", value);
}
String? getChargeDuration() {
  return prefs.getString("chargeDuration");
}

void saveChargeLockState(String value){
  prefs.setString("chargeLockState", value);
}
String? getChargeLockState() {
  return prefs.getString("chargeLockState");
}

void saveFaultState(String value){
  prefs.setString("faultState", value);
}
String? getFaultState() {
  return prefs.getString("faultState");
}

void saveLoadBalancingState(String value){
  prefs.setString("loadBalancingState", value);
}
String? getLoadBalancingState() {
  return prefs.getString("loadBalancingState");
}

void saveScheduleChargeEnableState(String value){
  prefs.setString("scheduleChargeEnableState", value);
}
String? getScheduleChargeEnableState() {
  return prefs.getString("scheduleChargeEnableState");
}

void saveState(String value){
  prefs.setString("state", value);
}
String? getState() {
  return prefs.getString("state");
}

void saveScheduleStartTimeWeekDays(DateTime value){
  prefs.setString("scheduleStartTimeWeekDays", value.toString());
}
DateTime getScheduleStartTimeWeekDays() {
  return DateTime.parse(prefs.getString("scheduleStartTimeWeekDays") ?? DateTime.now().toIso8601String());
}

void saveScheduleStopTimeWeekEnds(DateTime value){
  prefs.setString("scheduleStopTimeWeekEnds", value.toString());
}
DateTime getScheduleStopTimeWeekEnds() {
  return DateTime.parse(prefs.getString("scheduleStopTimeWeekEnds") ?? DateTime.now().add(const Duration(minutes: 5)).toIso8601String());
}

void saveScheduleStartTimeWeekEnds(DateTime value){
  prefs.setString("scheduleStartTimeWeekEnds", value.toString());
}
DateTime getScheduleStartTimeWeekEnds() {
  return  DateTime.parse(prefs.getString("saveScheduleStartTimeWeekEnds") ?? DateTime.now().toIso8601String());
}

void saveScheduleStopTimeWeekDays(DateTime value){
  prefs.setString("scheduleStopTimeWeekDays", value.toString());
}
DateTime getScheduleStopTimeWeekDays() {
  return DateTime.parse(prefs.getString("scheduleStopTimeWeekDays") ?? DateTime.now().add(const Duration(minutes: 5)).toIso8601String());
}


clearSharedPrefs(){
  prefs.clear();
}