//name constants are started from letter l
//images and icon constants are started from letter m
//color constants are started from letter k

const String lAppName = "ChargeNet Home";
const String lSkip = "Skip";
const String lOnBoardingTopic1 = "OnBoarding 1";
const String lOnBoardingTopic2 = "OnBoarding 2";
const String lOnBoardingTopic3 = "OnBoarding 3";
const String lOnBoardingPara1 = "App onboarding is key for one core reason: if users don't understand your app,"
    " ... Avoid text-heavy explanations and opt instead for app screenshots.";
const String lOnBoardingPara2 = "A review of 3 mobile apps: Slack, Trip and Feedly in terms of the way that"
    " they have implemented inspirational user onboarding techniques.";
const String lOnBoardingPara3 = "Welcome messages: The definitive first impression for your mobile app. "
    " impressions count, both for people and products. Apps have one.";
const String lForgotPassword = "Forgot Password?";
const String lNewToVegaPower = "New to Vega Power? ";
const String lAgreeTo = "By signInUp you're agree to our ";
const String lTerms = "Terms & Conditions and Privacy Policy";
const String lJoinedUsBefore = "Joined us before? ";
const String lPairDevice = "Pair Device";
const String lRegisterDevice = "Register Device";
const String lChooseLocation = "Choose your Device Location";
const String lAddLocation = "Add your Device Location";
const String lLogin = "LogIn";
const String lSignUp = "Sign Up";
const String lRegister = "Register";
const String lEnterPin = "Enter your device PIN";
const String lScanQR = "Scan QR";
const String lEnterSerial = "Enter the Serial number";
const String lOr = "OR";
const String lUserName = "User Name";
const String lPassword = "Password";
const String lRePassword = "Confirm Password";
const String lEmail = "Email";
const String lPowerCell = "Power Cell";
const String lGo = "Go";
const String lHistory = "History";
const String lChooseHistoryType = "Select a day";
const String lSettings = "Settings";
const String lDevices = "Devices";
const String lApply = "Apply";
const String lSetAsDefault = "Set as current Location";
const String lProfile = "Profile";
const String lFirstName = "First Name";
const String lLastName = "Last Name";
const String lPhone = "Mobile Number";
const String lUpdate = "Update";
const String lResetPassword = "Reset Password";
const String lUpdatePassword = "Change Password";
const String lCurrentPassword = "Current Password";
const String lSendInstructions = "Send Instructions";
const String noInternet = 'Connection to API server failed due to internet connection';

//sharedPrefs keys
const String lToken = 'accessToken';
const String lUserLoginId = 'loginId';
const String lUserId = 'userId';
const String lDeviceRegistered = 'registered';
const String lUserLocationId = 'locationId';
const String lBatteryId = 'batteryId';
const String lIsPowerCellPaired = 'isPowerCellPaired';
const String lPowerCellSerialNumber = 'powerCellSerialNumber';
const String lLocationId = 'locationId';
const String lLocationName = 'locationName';
const String lName = 'UserName';
const String lBatteryIsDeactivated = 'isDeactivated';

//Broker credentials:
const String lIp = "ec2-52-89-241-125.us-west-2.compute.amazonaws.com";
const int lPort = 1884;
// const String lBrokerUserName = "powercell_user";
// const String lBrokerPassword = "GcCkf8CZ6k";