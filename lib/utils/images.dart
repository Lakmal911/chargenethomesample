//name constants are started from letter l
//images and icon constants are started from letter m
//color constants are started from letter k

//images
const String mBackgroundImage = "background.png";
const String mOnBoarding1 = "solar.svg";
const String mOnBoarding2 = "grid.svg";
const String mOnBoarding3 = "powercell.svg";

//icons
const String mAppLogo = "vega_power.png";
const String mAppWhiteLogo = "vega_power_white.png";