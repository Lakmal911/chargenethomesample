class Base {
  static String appVersion = "1.0.0";

  static String baseTestURL = "http://52.33.161.136:11000/";

  static String baseLiveURL = "https:// /";
  static String baseLivePayment = "https:// /";

  static String baseURL = baseTestURL;

  static String login = baseURL + "IdentityController.asmx/Login";
  static String logout = baseURL + "IdentityController.asmx/Logout";

  static String register = baseURL + "UserController.asmx/Register";
  static String getUserById = baseURL + "UserController.asmx/GetUser";
  static String updateUser = baseURL + "UserController.asmx/UpdateUser";
  static String forgotPassword = baseURL + "UserController.asmx/ResetPassword";
  static String updatePassword = baseURL + "UserController.asmx/ChangePassword";

  static String addLocation = baseURL + "LocationController.asmx/AddLocation";
  static String getLocationById = baseURL + "LocationController.asmx/GetLocation";
  static String getLocationByUser = baseURL + "LocationController.asmx/GetLocationsByUser";
  static String setDefaultLocation = baseURL + "LocationController.asmx/SetDefaultLocation";

  static String addDeviceType = baseURL + "DeviceController.asmx/AddDeviceType";
  static String getAllDeviceTypes = baseURL + "DeviceController.asmx/GetAllDeviceTypes";
  static String addDevice = baseURL + "DeviceController.asmx/AddDevice";
  static String getDeviceById = baseURL + "DeviceController.asmx/GetDevice";
  static String getDevicesByDeviceType = baseURL + "DeviceController.asmx/GetDevicesByType";
  static String getDevicesByUser = baseURL + "DeviceController.asmx/GetDevicesByUser";
  static String assignDeviceToUser = baseURL + "DeviceController.asmx/AssignDeviceToUser";
  static String getDevicesByUserLocation = baseURL + "DeviceController.asmx/GetDevicesByUserLocation";
  static String getAllInverterTypes = baseURL + "DeviceController.asmx/GetAllInverterTypes";
  static String getDeletedDevices = baseURL + "DeviceController.asmx/GetDeletedDevices";
  static String changeDeleteState = baseURL + "DeviceController.asmx/ChangeDeleteState";

  static String getDeviceStatus = baseURL + "HomeController.asmx/GetDeviceStatus";

  static String getBatteryCellStatus = baseURL + "BatteryCellController.asmx/GetBatteryCellStatus";
  static String getSocHistory = baseURL + "BatteryCellController.asmx/GetSocHistory";
  static String getChargeLevels = baseURL + "BatteryCellController.asmx/GetChargeLevels";
  static String getBackupTimeSlots = baseURL + "BatteryCellController.asmx/GetBackupTiming";
  static String updateBackupTiming = baseURL + "BatteryCellController.asmx/UpdateBackupTiming";
  static String updateChargeLevels = baseURL + "BatteryCellController.asmx/UpdateChargeLevels";

}
