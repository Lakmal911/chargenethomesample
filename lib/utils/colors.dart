import 'package:flutter/material.dart';

//name constants are started from letter l
//images and icon constants are started from letter m
//color constants are started from letter k

const kPrimaryColor = Colors.white;
// const kPrimaryColor = Color(0xff16192C);
const kSecondaryColor = Color(0xff6E717A);
// const kSecondaryColor = Color(0xff0DDCE4);
const kSecondaryAccentColor = Color(0xff01FF86);
const kAccentColor = Color(0xff6E717A);
const kScaffoldBackgroundColor = Color(0xffEEEEEE);
const kColorWhite = Colors.white;
const kColorBlack = Colors.black;