import 'package:flutter/material.dart';
import 'package:home/utils/colors.dart';

const gilroyRegularTextStyle = TextStyle(
    color: kColorBlack,
    fontWeight: FontWeight.normal,
    // letterSpacing: 2
);

const gilroyBoldTextStyle = TextStyle(
    color: kColorBlack,
    fontWeight: FontWeight.bold,
    // letterSpacing: 1.5
);

