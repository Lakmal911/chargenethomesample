import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';

class NetworkHelper {

  static Future<bool> checkNetwork() async{
    bool networkResults = true;

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        ////print(' Network changes connected');
        networkResults = true;
      }
    } catch (e) {
      ////print(' Network changes not connected');
      networkResults = false;
    }
    return networkResults;
  }
}
