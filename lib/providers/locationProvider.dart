import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:home/data/base/api_response.dart';
import 'package:home/data/base/error_response.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/models/addLocation.dart';
import 'package:home/models/commonSuccessResponse.dart';
import 'package:home/models/getLocationByUser.dart';
import 'package:home/models/getRegisteredLocation.dart';
import 'package:home/services/locationService.dart';

class LocationProvider with ChangeNotifier {

  final LocationService locationService;

  LocationProvider({required this.locationService});

  bool _isLoading = false;
  String _errorMessage = '';
  bool get isLoading => _isLoading;
  String get errorMessage => _errorMessage;
  GetLocationByUserResponse? _getLocationByUserResponse;
  List<LocationList> _locationList = [];
  List<DeviceLocationList> _tempLocationList = [];
  AddLocationResponse? _addLocationResponse;
  late bool _isLocationFilled = true;
  bool get isLocationFilled => _isLocationFilled;
  LocationList? _selectedLocation;
  DeviceLocationList? _tempSelectedLocation;

  AddLocationResponse? get getAddLocationResponse => _addLocationResponse;

  LocationList? get getSelectedLocation => _selectedLocation;
  DeviceLocationList? get getTempSelectedLocation => _tempSelectedLocation;

  CommonSuccessResponse? _defaultLocationResponse;
  CommonSuccessResponse? get getDefaultLocationResponse => _defaultLocationResponse;

  List<String> _locationNameList = [];
  GetLocationByUserResponse? get getLocationByUserResponse => _getLocationByUserResponse;
  List<String> get locationNameList => _locationNameList;
  List<DeviceLocationList> get getTempLocationList => _tempLocationList;
  List<LocationList> get getLocationList => _locationList;

  addLocationNameList(String value) {
    _locationNameList.add(value);
    notifyListeners();
  }

  clearLocationNameList(){
    _locationNameList.clear();
    notifyListeners();
  }

  set setDefaultLocationResponse(CommonSuccessResponse value) {
    _defaultLocationResponse = value;
    notifyListeners();
  }

  set setSelectedLocation(LocationList value) {
    _selectedLocation = value;
    notifyListeners();
  }

  set setTempSelectedLocation(DeviceLocationList value) {
    _tempSelectedLocation = value;
    notifyListeners();
  }

  set setTempLocationList(List<DeviceLocationList> value) {
    _tempLocationList = value;
    notifyListeners();
  }

  set setIsLocationFilled(bool value) {
    _isLocationFilled = value;
    notifyListeners();
  }

  set setAddLocationResponse(AddLocationResponse value) {
    _addLocationResponse = value;
    notifyListeners();
  }

  set setLocationList(List<LocationList> value) {
    _locationList = value;
    notifyListeners();
  }

  addLocation(DeviceLocationList value){
    _tempLocationList.add(value);
    notifyListeners();
  }

  clearTempLocationList(){
    _tempLocationList.clear();
    notifyListeners();
  }

  clearTempSelectedLocationList(){
    _tempSelectedLocation = DeviceLocationList(locationId: -1, locationName: "");
    notifyListeners();
  }

  set setIsLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  set setLocationByUserResponse(GetLocationByUserResponse value) {
    _getLocationByUserResponse = value;
    notifyListeners();
  }

  set setErrorMessage(String value) {
    _errorMessage = value;
    notifyListeners();
  }

  getLocationByUser() async {
    ApiResponse apiResponse = await locationService.getLocationByUser();
    if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
      var response1 = GetLocationByUserResponse.fromJson(json);
      var response2 = GetRegisteredLocation.fromJson(json);
      if(response1.isSuccess?? false){
        setLocationByUserResponse = response1;
        setLocationList = response1.payload?.locationList?? [];
        setTempLocationList = response2.payload?.locationList?? [];
        if(getLocationList.isNotEmpty){
          for(int i=0; i< getLocationList.length; i++){
            if(getLocationList[i].locationId == getLocationId()){
              setSelectedLocation = LocationList(locationName: getLocationName(), locationId: getLocationId());
              setTempSelectedLocation = DeviceLocationList(locationName: getLocationName(), locationId: getLocationId());
            }else{
              setSelectedLocation = getLocationList[0];
              setTempSelectedLocation = getTempLocationList[0];
            }
          }
        }
        return response1;
      }else{
        setErrorMessage = response1.message?? "Something Went Wrong, Please contact an admin!";
        setLocationList = [];
        setTempLocationList = [];
      }
    } else {
      setLocationList = [];
      setTempLocationList = [];
      handleError(apiResponse);
    }
    notifyListeners();
  }

  // addUserLocation({required String locationName}) async {
  //   try{
  //     ApiResponse apiResponse = await locationService.addUserLocation(locationName: locationName);
  //     if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
  //       Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
  //       var response = AddLocationResponse.fromJson(json);
  //       if(response.isSuccess?? false){
  //         setAddLocationResponse = response;
  //         return response;
  //       }else{
  //         setAddLocationResponse = AddLocationResponse();
  //         setErrorMessage = response.message?? "Something Went Wrong, Please contact an admin!";
  //         setLocationList = [];
  //       }
  //     } else {
  //       setAddLocationResponse = AddLocationResponse();
  //       handleError(apiResponse);
  //     }
  //
  //   }catch(e){
  //     setErrorMessage = "Something Went Wrong, Please contact an admin!";
  //     setAddLocationResponse = AddLocationResponse();
  //   }
  //   notifyListeners();
  // }

  setDefaultLocation({required int locationId}) async {
    setIsLoading = true;
    ApiResponse apiResponse = await locationService.setDefaultLocation(locationId: locationId);
    setIsLoading = false;
    if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
      var response = CommonSuccessResponse.fromJson(json);
      if(response.isSuccess?? false){
        setDefaultLocationResponse = response;
        return response;
      }else{
        setDefaultLocationResponse = CommonSuccessResponse();
        setErrorMessage = response.message?? "Something Went Wrong, Please contact an admin!";
      }
    } else {
      setDefaultLocationResponse = CommonSuccessResponse();
      handleError(apiResponse);
    }
    notifyListeners();
  }

  handleError(ApiResponse apiResponse) {
    String? errorMessage;
    if (apiResponse.error is String) {
      errorMessage = apiResponse.error.toString();
    } else {
      ErrorResponse errorResponse = apiResponse.error;
      errorMessage = errorResponse.errors![0].message;
    }
    setErrorMessage = errorMessage ?? "Something went wrong!";
  }
}