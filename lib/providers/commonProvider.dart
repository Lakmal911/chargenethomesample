import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class CommonProvider with ChangeNotifier {

  late int _onBoardingCurrentPage = 0;
  late int _bottomNavCurrentIndex = 0;
  late bool _obscureTextPassword = true; //password show/ not show
  late bool _obscureTextRePassword = true; //password show/ not show
  late double _powerCellPosition = 0.0;
  Barcode? _result;

  int get getOnBoardingCurrentPage => _onBoardingCurrentPage;
  int get getBottomNavCurrentIndex => _bottomNavCurrentIndex;
  bool get getIsObscureTextPassword => _obscureTextPassword;
  bool get getIsObscureTextRePassword => _obscureTextRePassword;
  double get getPowerCellPosition => _powerCellPosition;
  Barcode? get getBarcodeResult => _result;

  set isObscureTextPassword(bool value) {
    _obscureTextPassword = value;
    notifyListeners();
  }

  set isObscureTextRePassword(bool value) {
    _obscureTextRePassword = value;
    notifyListeners();
  }

  set bottomNavCurrentIndexOnChanged(int value) {
    _bottomNavCurrentIndex = value;
    notifyListeners();
  }

  onBoardingCurrentPageOnChanged(int index) {
    _onBoardingCurrentPage = index;
    notifyListeners();
  }

  set powerCellArrowPosition(double val){
    _powerCellPosition = val;
    notifyListeners();
  }

  set barcodeResult(Barcode val){
    _result = val;
    notifyListeners();
  }

}