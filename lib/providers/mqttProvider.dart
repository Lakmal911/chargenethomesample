import 'dart:convert';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/reusableWidgets/customNoInternetAlert.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/network.dart';
import 'package:uuid/uuid.dart';

import '../models/command.dart';
import '../models/mqtt.dart';

class MqttProvider with ChangeNotifier {
  Mqtt? _mqttResponse;
  String _powerFlowStatus = getCommandStatus() ?? "N/A";
  String _state = getState() ?? "0.0";
  String _current = getCurrent() ?? "0.0";
  String _voltage = getVoltage() ?? "0.0";
  String _power = getPower() ?? "0.0";
  String _energy = getEnergy() ?? "0.0";
  String _chargeDuration = getChargeDuration() ?? "0.0";
  String _chargeLockState = getChargeLockState() ?? "0.0";
  String _faultState = getFaultState() ?? "0.0";
  String _loadBalancingState = getLoadBalancingState() ?? "0.0";
  String _scheduleChargeEnableState = getScheduleChargeEnableState() ?? "0.0";
  String _runningAppVersion = "0";
  bool _isMqttLoading = false;
  DateTime _scheduleStartTimeWeekDays = getScheduleStartTimeWeekDays();
  DateTime _scheduleStopTimeWeekDays = getScheduleStopTimeWeekDays();
  DateTime _scheduleStartTimeWeekEnds = getScheduleStartTimeWeekEnds();
  DateTime _scheduleStopTimeWeekEnds = getScheduleStopTimeWeekEnds();
  String _givenCommand = "";
  bool _isScheduling = false;
  bool _isWifiConfig = false;

  String get state => _state;
  String get voltage => _voltage;
  String get powerFlowStatus => _powerFlowStatus;
  String get chargeDuration => _chargeDuration;
  String get chargeLockState => _chargeLockState;
  String get faultState => _faultState;
  String get loadBalancingState => _loadBalancingState;
  String get scheduleChargeEnableState => _scheduleChargeEnableState;
  String get runningAppVersion => _runningAppVersion;
  bool get isMqttLoading => _isMqttLoading;
  DateTime get scheduleStartTimeWeekDays => _scheduleStartTimeWeekDays;
  DateTime get scheduleStopTimeWeekDays => _scheduleStopTimeWeekDays;
  DateTime get scheduleStartTimeWeekEnds => _scheduleStartTimeWeekEnds;
  DateTime get scheduleStopTimeWeekEnds => _scheduleStopTimeWeekEnds;
  String get givenCommand => _givenCommand;
  bool get isScheduling => _isScheduling;
  bool get isWifiConfig => _isWifiConfig;

  String _myTopic = "/C0090";
  String get myTopic => _myTopic;

  set setIsWifiConfig(bool value) {
    _isWifiConfig = value;
    notifyListeners();
  }

  set setIsScheduling(bool value) {
    _isScheduling = value;
    notifyListeners();
  }

  set setMyTopic(String value) {
    _myTopic = value;
    notifyListeners();
  }

  set givenCommand(String value) {
    _givenCommand = value;
    notifyListeners();
  }

  set scheduleStartTimeWeekDays(DateTime value) {
    _scheduleStartTimeWeekDays = value;
    notifyListeners();
  }

  set scheduleStopTimeWeekDays(DateTime value) {
    _scheduleStopTimeWeekDays = value;
    notifyListeners();
  }

  set scheduleStartTimeWeekEnds(DateTime value) {
    _scheduleStartTimeWeekEnds = value;
    notifyListeners();
  }

  set scheduleStopTimeWeekEnds(DateTime value) {
    _scheduleStopTimeWeekEnds = value;
    notifyListeners();
  }

  set mqttLoading(bool value) {
    _isMqttLoading = value;
    notifyListeners();
  }

  set chargeDuration(String value) {
    _chargeDuration = value;
    notifyListeners();
  }

  set voltage(String value) {
    _voltage = value;
    notifyListeners();
  }

  set runningAppVersion(String value) {
    _runningAppVersion = value;
    notifyListeners();
  }

  set state(String value) {
    _state = value;
    notifyListeners();
  }

  set chargeLockState(String value) {
    _chargeLockState = value;
    notifyListeners();
  }

  set faultState(String value) {
    _faultState = value;
    notifyListeners();
  }

  set powerFlowStatus(String value) {
    _powerFlowStatus = value;
    notifyListeners();
  }

  set loadBalancingState(String value) {
    _loadBalancingState = value;
    notifyListeners();
  }

  set scheduleChargeEnableState(String value) {
    _scheduleChargeEnableState = value;
    notifyListeners();
  }

  String get current => _current;

  set current(String value) {
    _current = value;
    notifyListeners();
  }

  String get power => _power;

  set power(String value) {
    _power = value;
    notifyListeners();
  }

  String get energy => _energy;

  set energy(String value) {
    _energy = value;
    notifyListeners();
  }

  Mqtt? get mqttResponse => _mqttResponse;

  set setMqttResponse(Mqtt value) {
    _mqttResponse = value;
    notifyListeners();
  }

  clearMqttProviderData() {
    _mqttResponse = Mqtt();
    voltage = "0.0";
    current = "0.0";
    powerFlowStatus = "N/A";
    notifyListeners();
  }

  final client = MqttServerClient.withPort(lIp, '1234', lPort);

  var pongCount = 0; // Pong counter

  Future<int> main(BuildContext context) async {
    /// Set logging on if needed, defaults to off
    client.logging(on: false);

    /// Set the correct MQTT protocol for mosquito
    client.setProtocolV311();

    /// If you intend to use a keep alive you must set it here otherwise keep alive will be disabled.
    client.keepAlivePeriod = 20;

    /// Add the unsolicited disconnection callback
    client.onDisconnected = onDisconnected(context);

    /// Add the successful connection callback
    client.onConnected = onConnected;

    /// Add a subscribed callback, there is also an unsubscribed callback if you need it.
    /// You can add these before connection or change them dynamically after connection if
    /// you wish. There is also an onSubscribeFail callback for failed subscriptions, these
    /// can fail either because you have tried to subscribe to an invalid topic or the broker
    /// rejects the subscribe request.
    client.onSubscribed = onSubscribed;

    /// Set a ping received callback if needed, called whenever a ping response(pong) is received
    /// from the broker.
    client.pongCallback = pong;

    /// Create a connection message to use or use the default one. The default one sets the
    /// client identifier, any supplied username/password and clean session,
    /// an example of a specific one below.
    final connMess = MqttConnectMessage()
        // .authenticateAs(lBrokerUserName, lBrokerPassword)
        .withClientIdentifier(const Uuid().v1().toString())
        // .withClientIdentifier('${getUserToken()}')
        .withWillTopic('willtopic') // If you set this you must set a will message
        .withWillMessage('My Will message')
        .startClean() // Non persistent session for testing
        .withWillQos(MqttQos.atLeastOnce);
    print('EXAMPLE::Mosquitto client connecting....');
    client.connectionMessage = connMess;

    /// Connect the client, any errors here are communicated by raising of the appropriate exception. Note
    /// in some circumstances the broker will just disconnect us, see the spec about this, we however will
    /// never send malformed messages.
    try {
      await client.connect();
    } on NoConnectionException catch (e) {
      // Raised by the client when connection fails.
      print('EXAMPLE::client exception - $e');
      client.disconnect();
    } on SocketException catch (e) {
      // Raised by the socket layer
      print('EXAMPLE::socket exception - $e');
      client.disconnect();
    }

    /// Check we are connected
    if (client.connectionStatus!.state == MqttConnectionState.connected) {
      print('EXAMPLE::Mosquitto client connected');
    } else {
      /// Use status here rather than state if you also want the broker return code.
      print('EXAMPLE::ERROR Mosquitto client connection failed - disconnecting, status is ${client.connectionStatus}');
      client.disconnect();
      customNoInternetAlert(
        context: context,
        onYes: () async {
          bool networkResults = await NetworkHelper.checkNetwork();
          if (networkResults) {
            Navigator.pop(context);
            main(context);
          }
        },
      );
    }

    client.subscribe('/C0090', MqttQos.atMostOnce);
    client.subscribe('C0090', MqttQos.atMostOnce);

    client.updates!.listen((List<MqttReceivedMessage<MqttMessage?>>? c) {
      final recMess = c![0].payload as MqttPublishMessage;
      String pt = MqttPublishPayload.bytesToStringAsString(recMess.payload.message);

      /// The above may seem a little convoluted for users only interested in the
      /// payload, some users however may be interested in the received publish message,
      /// lets not constrain ourselves yet until the package has been in the wild
      /// for a while.
      /// The payload is a byte buffer, this will be specific to the topic
      print('EXAMPLE::Change notification:: topic is <${c[0].topic}>, payload is <-- $pt -->');
      print('');

      mqttLoading = false;
      if (pt.isNotEmpty) {
        try {
          Map<String, dynamic> map = json.decode(pt);

          var response = Mqtt.fromJson(map);
          setMqttResponse = response;

          if (response.command == 'heartBeat') {
            //set Mqtt values
            powerFlowStatus = response.command ?? "N/A";
            state = response.state ?? "0.0";
            current = response.current ?? "0.0";
            voltage = response.voltage ?? "0.0";
            power = response.power ?? "0.0";
            energy = response.energy ?? "0.0";
            chargeDuration = response.chargeDuration ?? "0.0";
            chargeLockState = response.chargerLockState ?? "0.0";
            faultState = response.faultState ?? "0.0";
            loadBalancingState = response.loadBalancingState ?? "0.0";
            scheduleChargeEnableState = response.scheduleChargeEnableState ?? "0.0";
            runningAppVersion = response.runningAppVersion ?? "0.0";

            //save to shared to cache data
            saveCommandStatus(powerFlowStatus);
            saveState(state);
            saveCurrent(current);
            saveVoltage(voltage);
            savePower(power);
            saveEnergy(energy);
            saveChargeDuration(chargeDuration);
            saveChargeLockState(chargeLockState);
            saveFaultState(faultState);
            saveLoadBalancingState(loadBalancingState);
            saveScheduleChargeEnableState(scheduleChargeEnableState);

            if(!isWifiConfig){
              setIsWifiConfig = true;
              publishMyMsgFromAppToDevice('wifi_config_state_off', false);
            }
          }
        } catch (e) {
          print("exception $e");
        }
      } else {}
    });

    return 0;
  }

  publishMyMsgFromAppToDevice(String msg, bool isSchedule,) {
    final builder = MqttClientPayloadBuilder();
    if(isSchedule){
      builder.addString(msg);
    }else{
      builder.addString(jsonEncode(Command(command: msg)));
    }
    client.publishMessage('C0090', MqttQos.atMostOnce, builder.payload!);
  }

  /// The subscribed callback
  void onSubscribed(String topic) {
    print('EXAMPLE::Subscription confirmed for topic $topic');
  }

  /// The unsolicited disconnect callback
  onDisconnected(BuildContext context) {
    print('EXAMPLE::OnDisconnected client callback - Client disconnection');
    if (client.connectionStatus!.disconnectionOrigin == MqttDisconnectionOrigin.solicited) {
      print('EXAMPLE::OnDisconnected callback is solicited, this is correct');
    } else {
      print('EXAMPLE::OnDisconnected callback is unsolicited or none, this is incorrect - exiting');
    }
    if (pongCount == 3) {
      print('EXAMPLE:: Pong count is correct');
    } else {
      print('EXAMPLE:: Pong count is incorrect, expected 3. actual $pongCount');
    }
  }

  /// The successful connect callback
  void onConnected() {
    print('EXAMPLE::OnConnected client callback - Client connection was successful');
  }

  /// Pong callback
  void pong() {
    print('EXAMPLE::Ping response client callback invoked');
    // if(powerFlowStatus != "N/A"){
    //   setIsBatteryDataAvailable = true;
    // }else{
    //   setIsBatteryDataAvailable = false;
    // }
    pongCount++;
  }
}
