import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:home/data/base/api_response.dart';
import 'package:home/data/base/error_response.dart';
import 'package:home/models/commonSuccessResponse.dart';
import 'package:home/models/getUser.dart';
import 'package:home/models/login.dart';
import 'package:home/models/logout.dart';
import 'package:home/models/regsiter.dart';
import 'package:home/services/userService.dart';

class UserProvider with ChangeNotifier {

  final UserService userService;

  UserProvider({required this.userService});

  bool _isLoading = false;
  bool _isEditPressed = false;
  String _errorMessage = '';
  LoginResponse? _loginResponse;
  LogoutResponse? _logoutResponse;
  RegisterResponse? _registerResponse;
  GetUserByIdResponse? _getUserByIdResponse;
  CommonSuccessResponse? _commonSuccessResponse;

  bool get isLoading => _isLoading;
  bool get isEditPressed => _isEditPressed;
  String get errorMessage => _errorMessage;
  LogoutResponse? get logoutResponse => _logoutResponse;
  GetUserByIdResponse? get getUserByIdResponse => _getUserByIdResponse;
  CommonSuccessResponse? get commonSuccessResponse => _commonSuccessResponse;
  RegisterResponse? get registerResponse => _registerResponse;
  LoginResponse? get loginResponse => _loginResponse;

  set setIsEditPressed(bool value) {
    _isEditPressed = value;
    notifyListeners();
  }

  set setCommonSuccessResponse(CommonSuccessResponse value) {
    _commonSuccessResponse = value;
    notifyListeners();
  }

  set setUserByIdResponse(GetUserByIdResponse value) {
    _getUserByIdResponse = value;
    notifyListeners();
  }

  set setLogoutResponse(LogoutResponse value) {
    _logoutResponse = value;
    notifyListeners();
  }

  set setRegisterResponse(RegisterResponse value) {
    _registerResponse = value;
    notifyListeners();
  }

  set setIsLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  set setLoginResponse(LoginResponse value) {
    _loginResponse = value;
    notifyListeners();
  }

  set setErrorMessage(String value) {
    _errorMessage = value;
    notifyListeners();
  }

  getLoginResponse({required String userName, required String password}) async {
    try{
        ApiResponse apiResponse = await userService.userLogin(
            userName: userName,
            password: password,
            loginMode: "App",
        );
        if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
          Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
          var response = LoginResponse.fromJson(json);
          if(response.isSuccess?? false){
            setLoginResponse = response;
            return response;
          }else{
            setLoginResponse = LoginResponse();
            setErrorMessage = response.message?? "Something Went Wrong, Please contact an admin!";
          }
        } else {
          handleError(apiResponse);
        }

    }catch(e){
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
      // print(e.toString());
    }
    notifyListeners();
  }

  getRegisterResponse({
    required String username, required String password, required String email
  }) async {
    try{
      ApiResponse apiResponse = await userService.userRegister(
        username: username,
        email: email,
        password: password,
      );
      if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
        var response = RegisterResponse.fromJson(json);
        if(response.isSuccess?? false){
          setRegisterResponse = response;
          return response;
        }else{
          setRegisterResponse = RegisterResponse();
          setErrorMessage = response.message?? "Something Went Wrong";
        }
      } else {
        setRegisterResponse = RegisterResponse();
        handleError(apiResponse);
      }

    }catch(e){
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
      setRegisterResponse = RegisterResponse();
    }
    notifyListeners();
  }

  getLogoutResponse() async {
    try{
      ApiResponse apiResponse = await userService.userLogout();
      if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
        var response = LogoutResponse.fromJson(json);
        if(response.isSuccess?? false){
          setLogoutResponse = response;
          return response;
        }else{
          setLogoutResponse = LogoutResponse();
          setErrorMessage = response.message?? "Something Went Wrong";
        }
      } else {
        setLogoutResponse = LogoutResponse();
        handleError(apiResponse);
      }

    }catch(e){
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
      setLogoutResponse = LogoutResponse();
    }
    notifyListeners();
  }

  getUserById() async {
    ApiResponse apiResponse = await userService.getUser();
    if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
      var response = GetUserByIdResponse.fromJson(json);
      if(response.isSuccess?? false){
        setUserByIdResponse = response;
        return response;
      }else{
        setUserByIdResponse = GetUserByIdResponse();
        setErrorMessage = response.message?? "Something Went Wrong";
      }
    } else {
      setUserByIdResponse = GetUserByIdResponse();
      handleError(apiResponse);
    }
    notifyListeners();
  }

  updateUser({required String firstName, required String lastName, required String email, required String contactNo}) async {
   setIsLoading = true;
   ApiResponse apiResponse = await userService.userUpdate(
       firstName: firstName,
       lastName: lastName,
       email: email,
       contactNo: contactNo
   );
   if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
     Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
     var response = CommonSuccessResponse.fromJson(json);
     if(response.isSuccess?? false){
       setCommonSuccessResponse = response;
       return response;
     }else{
       setCommonSuccessResponse = CommonSuccessResponse();
       setErrorMessage = response.message?? "Something Went Wrong";
     }
   } else {
     setCommonSuccessResponse = CommonSuccessResponse();
     handleError(apiResponse);
   }
   setIsLoading = false;
    notifyListeners();
  }

  userResetPassword({required String username, required String email}) async {
    try{
      ApiResponse apiResponse = await userService.userResetPassword(
         username: username,
         email: email,
      );
      if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
        var response = CommonSuccessResponse.fromJson(json);
        if(response.isSuccess?? false){
          setCommonSuccessResponse = response;
          return response;
        }else{
          setCommonSuccessResponse = CommonSuccessResponse();
          setErrorMessage = response.message?? "Something Went Wrong";
        }
      } else {
        setCommonSuccessResponse = CommonSuccessResponse();
        handleError(apiResponse);
      }

    }catch(e){
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
      setCommonSuccessResponse = CommonSuccessResponse();
    }
    notifyListeners();
  }

  userUpdatePassword({required String newPassword, required String oldPassword}) async {
    try{
      ApiResponse apiResponse = await userService.userUpdatePassword(oldPassword: oldPassword, newPassword: newPassword);
      if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
        var response = CommonSuccessResponse.fromJson(json);
        if(response.isSuccess?? false){
          setCommonSuccessResponse = response;
          return response;
        }else{
          setCommonSuccessResponse = CommonSuccessResponse();
          setErrorMessage = response.message?? "Something Went Wrong";
        }
      } else {
        setCommonSuccessResponse = CommonSuccessResponse();
        handleError(apiResponse);
      }

    }catch(e){
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
      setCommonSuccessResponse = CommonSuccessResponse();
    }
    notifyListeners();
  }

  handleError(ApiResponse apiResponse) {
    String? errorMessage;
    if (apiResponse.error is String) {
      errorMessage = apiResponse.error.toString();
    } else {
      ErrorResponse errorResponse = apiResponse.error;
      errorMessage = errorResponse.errors![0].message;
    }
    setErrorMessage = errorMessage ?? "Something went wrong!";
  }
}