import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:time_range/time_range.dart';
import 'package:home/data/base/api_response.dart';
import 'package:home/data/base/error_response.dart';
import 'package:home/models/commonSuccessResponse.dart';
import 'package:home/models/getBackupTiming.dart';
import 'package:home/models/getChargeLevels.dart';
import 'package:home/models/getSocHistory.dart';
import 'package:home/services/batteryCellService.dart';
import 'package:intl/intl.dart';

class BatteryCellProvider with ChangeNotifier {

  final BatteryCellService batteryCellService;

  BatteryCellProvider({required this.batteryCellService});

  bool _isLoading = true;
  String _errorMessage = '';
  GetSocHistoryResponse? _socHistoryResponse;
  GetChargeLevelsResponse? _getChargeLevelsResponse;
  double _chargeValue = 50.0;
  double _disChargeValue = 1.0;
  String _inverterTypeId = "-1";
  bool _isEnableReverseCapacity = false;
  bool _isWeekEndSelected = false;
  //backup time for weekend and weekdays
  String _weekDayMorningStartTime = "0001-01-01T01:00:00";
  String _weekDayMorningEndTime = "0001-01-01T01:00:00";
  String _weekDayEveningStartTime = "0001-01-01T01:00:00";
  String _weekDayEveningEndTime = "0001-01-01T01:00:00";

  String _weekEndMorningStartTime = "0001-01-01T01:00:00";
  String _weekEndMorningEndTime = "0001-01-01T01:00:00";
  String _weekEndEveningStartTime = "0001-01-01T01:00:00";
  String _weekEndEveningEndTime = "0001-01-01T01:00:00";

  bool _isWeekDaySelected = true;
  CommonSuccessResponse? _commonSuccessResponse;
  GetBackupTimeResponse? _backupTimeSlotsResponse;
  String _selectedHistoryDay = DateFormat('MMM d, yyyy').format(DateTime.now());
  TimeRangeResult? _selectedTimeRange;

  String get inverterTypeId => _inverterTypeId;
  TimeRangeResult? get selectedTimeRange => _selectedTimeRange;
  String get selectedHistoryDay => _selectedHistoryDay;
  bool get isLoading => _isLoading;
  String get errorMessage => _errorMessage;
  GetSocHistoryResponse? get getSocHistoryResponse => _socHistoryResponse;
  GetChargeLevelsResponse? get getChargeLevelsResponse => _getChargeLevelsResponse;
  double get chargeValue => _chargeValue;
  double get disChargeValue => _disChargeValue;
  bool get isEnableReverseCapacity => _isEnableReverseCapacity;
  bool get isWeekEndSelected => _isWeekEndSelected;
  bool get isWeekDaySelected => _isWeekDaySelected;
  CommonSuccessResponse? get commonSuccessResponse => _commonSuccessResponse;
  GetBackupTimeResponse? get getBackupTimeSlotsResponse => _backupTimeSlotsResponse;

  String get weekDayMorningEndTime => _weekDayMorningEndTime;
  String get weekDayMorningStartTime => _weekDayMorningStartTime;
  String get weekDayEveningStartTime => _weekDayEveningStartTime;
  String get weekDayEveningEndTime => _weekDayEveningEndTime;
  String get weekEndMorningStartTime => _weekEndMorningStartTime;
  String get weekEndMorningEndTime => _weekEndMorningEndTime;
  String get weekEndEveningStartTime => _weekEndEveningStartTime;
  String get weekEndEveningEndTime => _weekEndEveningEndTime;

  set setInverterTypeId(String value) {
    _inverterTypeId = value;
    notifyListeners();
  }

  set setSelectedTimeRange(TimeRangeResult? value) {
    _selectedTimeRange = value;
    notifyListeners();
  }

  set setSelectedHistoryDay(String value) {
    _selectedHistoryDay = value;
    notifyListeners();
  }

  set setIsEnableReverseCapacity(bool value) {
    _isEnableReverseCapacity = value;
    notifyListeners();
  }

  set setCommonSuccessResponse(CommonSuccessResponse value) {
    _commonSuccessResponse = value;
    notifyListeners();
  }

  set weekDayMorningEndTime(String value) {
    _weekDayMorningEndTime = value;
    notifyListeners();
  }

  set weekDayEveningStartTime(String value) {
    _weekDayEveningStartTime = value;
    notifyListeners();
  }

  set weekDayEveningEndTime(String value) {
    _weekDayEveningEndTime = value;
    notifyListeners();
  }

  set weekEndMorningStartTime(String value) {
    _weekEndMorningStartTime = value;
    notifyListeners();
  }

  set weekEndMorningEndTime(String value) {
    _weekEndMorningEndTime = value;
    notifyListeners();
  }

  set weekEndEveningStartTime(String value) {
    _weekEndEveningStartTime = value;
    notifyListeners();
  }

  set weekEndEveningEndTime(String value) {
    _weekEndEveningEndTime = value;
    notifyListeners();
  }

  set weekDayMorningStartTime(String value) {
    _weekDayMorningStartTime = value;
    notifyListeners();
  }

  set setIsWeekDaySelected(bool val){
    _isWeekDaySelected = val;
    notifyListeners();
  }

  set setIsWeekEndSelected(bool val){
    _isWeekEndSelected = val;
    notifyListeners();
  }

  set setDisChargeValue(double value) {
    _disChargeValue = value;
    notifyListeners();
  }

  set setChargeValue(double value) {
    _chargeValue = value;
    notifyListeners();
  }

  set setChargeLevelsResponse(GetChargeLevelsResponse value) {
    _getChargeLevelsResponse = value;
    notifyListeners();
  }

  set setBackupTimeSlotsResponse(GetBackupTimeResponse value) {
    _backupTimeSlotsResponse = value;
    notifyListeners();
  }

  set setSocHistoryResponse(GetSocHistoryResponse value) {
    _socHistoryResponse = value;
    notifyListeners();
  }

  set setIsLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  set setErrorMessage(String value) {
    _errorMessage = value;
    notifyListeners();
  }

  validateMyBackupTime(bool isMorning){
    if(isWeekDaySelected){
      if(isMorning){
        weekDayMorningStartTime = formatDateTime(selectedTimeRange!.start);
        weekDayMorningEndTime = formatDateTime(selectedTimeRange!.end);
      }else{
        weekDayEveningStartTime = formatDateTime(selectedTimeRange!.start);
        weekDayEveningEndTime = formatDateTime(selectedTimeRange!.end);
      }
    }else{
      if(isMorning){
        weekEndMorningStartTime = formatDateTime(selectedTimeRange!.start);
        weekEndMorningEndTime = formatDateTime(selectedTimeRange!.end);
      }else{
        weekEndEveningStartTime = formatDateTime(selectedTimeRange!.start);
        weekEndEveningEndTime = formatDateTime(selectedTimeRange!.end);
      }
    }
    notifyListeners();
  }

  String formatDateTime(TimeOfDay time){
    return DateFormat('hh:mm:ss').format(DateTime(2022, 2, 1, time.hour, time.minute));
  }

  String formatDateToTime(String day){
    return DateFormat("hh:mm:ss").format(DateTime.parse(day));
  }

  clearTimeRange(){
    setSelectedTimeRange = TimeRangeResult(const TimeOfDay(hour: 55, minute: 55), const TimeOfDay(hour: 55, minute: 55),);
    notifyListeners();
    print(selectedTimeRange?.start);
  }

  getSocHistory({required DateTime from, required DateTime to}) async {
    try{
      ApiResponse apiResponse = await batteryCellService.getSocHistory(
          from: from,
          to: to
      );
      if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
        var response = GetSocHistoryResponse.fromJson(json);
        if(response.isSuccess?? false){
          setSocHistoryResponse = response;
          return response;
        }else{
          setSocHistoryResponse = GetSocHistoryResponse();
          setErrorMessage = response.message?? "Something Went Wrong, Please contact an admin!";
        }
      } else {
        setSocHistoryResponse = GetSocHistoryResponse();
        handleError(apiResponse);
      }

    }catch(e){
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
      setSocHistoryResponse = GetSocHistoryResponse();
    }
    notifyListeners();
  }

  getChargeLevels() async {
    try{
      ApiResponse apiResponse = await batteryCellService.getChargeLevels();
      if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
        var response = GetChargeLevelsResponse.fromJson(json);
        if(response.isSuccess?? false){
          setChargeLevelsResponse = response;
          setChargeValue = response.payload!.chargeLevels!.chargeLimit!;
          setDisChargeValue = response.payload!.chargeLevels!.dischargeLimit!;
          setIsEnableReverseCapacity = response.payload!.chargeLevels!.useReverseCapacity!;
          setInverterTypeId = response.payload!.chargeLevels!.inverterTypeId!.toString();
          return response;
        }else{
          setChargeLevelsResponse = GetChargeLevelsResponse();
          setErrorMessage = response.message?? "Something Went Wrong, Please contact an admin!";
        }
      } else {
        setChargeLevelsResponse = GetChargeLevelsResponse();
        handleError(apiResponse);
      }
    }catch(e){
      setChargeLevelsResponse = GetChargeLevelsResponse();
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
    }
    notifyListeners();
  }

  getBackupTimeSlots()async {
    try{
      ApiResponse apiResponse = await batteryCellService.getBackupTimeSlots();
      if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
        var response = GetBackupTimeResponse.fromJson(json);
        if(response.isSuccess?? false){
          setBackupTimeSlotsResponse = response;
          try{
            var _weekDay = response.payload?.backupTime?.weekday!;
            var _weekEnd = response.payload?.backupTime?.weekend!;

            weekDayMorningStartTime = formatDateToTime(_weekDay!.morning!.startAt!);
            weekDayMorningEndTime = formatDateToTime(_weekDay.morning!.endAt!);
            weekDayEveningStartTime = formatDateToTime(_weekDay.evening!.startAt!);
            weekDayEveningEndTime = formatDateToTime(_weekDay.evening!.endAt!);

            weekEndMorningStartTime = formatDateToTime(_weekEnd!.morning!.startAt!);
            weekEndMorningEndTime = formatDateToTime(_weekEnd.morning!.endAt!);
            weekEndEveningStartTime = formatDateToTime(_weekEnd.evening!.startAt!);
            weekEndEveningEndTime = formatDateToTime(_weekEnd.evening!.endAt!);
          }catch(e){
            print(e);
          }

          return response;
        }else{
          setBackupTimeSlotsResponse = GetBackupTimeResponse();
          setErrorMessage = response.message?? "Something Went Wrong, Please contact an admin!";
        }
      } else {
        setBackupTimeSlotsResponse = GetBackupTimeResponse();
        handleError(apiResponse);
      }

    }catch(e){
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
      setBackupTimeSlotsResponse = GetBackupTimeResponse();
    }
    notifyListeners();
  }

  updateBackupTimes({
    required String startTime, required String endTime,
    required int dateGroup, required int periodOfDay
  }) async {
    try{
      ApiResponse apiResponse = await batteryCellService.updateBackupTiming(
          startTime: startTime, endTime: endTime, dateGroup: dateGroup, periodOfDay: periodOfDay
      );
      if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
        var response = CommonSuccessResponse.fromJson(json);
        if(response.isSuccess?? false){
          setCommonSuccessResponse = response;
          return response;
        }else{
          setCommonSuccessResponse = CommonSuccessResponse();
          setErrorMessage = response.message?? "Something Went Wrong, Please contact an admin!";
        }
      } else {
        setCommonSuccessResponse = CommonSuccessResponse();
        handleError(apiResponse);
      }

    }catch(e){
      setCommonSuccessResponse = CommonSuccessResponse();
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
      // print(e.toString());
    }
    notifyListeners();
  }

  updateChargeLevels({required String inverterTypeId}) async {
    ApiResponse apiResponse = await batteryCellService.updateChargeLevels(
        chargeLimit: chargeValue,
        dischargeLimit: disChargeValue,
        enableReverseCapacity: isEnableReverseCapacity,
        inverterTypeId: inverterTypeId
    );
    if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
      var response = CommonSuccessResponse.fromJson(json);
      if(response.isSuccess?? false){
        setCommonSuccessResponse = response;
        return response;
      }else{
        setCommonSuccessResponse = CommonSuccessResponse();
        setErrorMessage = response.message?? "Something Went Wrong, Please contact an admin!";
      }
    } else {
      setCommonSuccessResponse = CommonSuccessResponse();
      handleError(apiResponse);
    }
    notifyListeners();
  }


  handleError(ApiResponse apiResponse) {
    String? errorMessage;
    if (apiResponse.error is String) {
      errorMessage = apiResponse.error.toString();
    } else {
      ErrorResponse errorResponse = apiResponse.error;
      errorMessage = errorResponse.errors![0].message;
    }
    setErrorMessage = errorMessage ?? "Something went wrong!";
  }
}