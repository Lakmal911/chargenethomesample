import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:home/data/base/api_response.dart';
import 'package:home/data/base/error_response.dart';
import 'package:home/models/addDeviceType.dart';
import 'package:home/models/assignDeviceToUserResponse.dart';
import 'package:home/models/chargeLevelTypesResponse.dart';
import 'package:home/models/deletedDevice.dart';
import 'package:home/models/getAllDeviceTypes.dart';
import 'package:home/models/getChargeLevels.dart';
import 'package:home/models/getDevicesByUserLocation.dart';
import 'package:home/models/commonSuccessResponse.dart';
import 'package:home/services/deviceService.dart';

class DeviceProvider with ChangeNotifier {
  final DeviceService deviceService;

  DeviceProvider({required this.deviceService});

  bool _isLoading = false;
  String _errorMessage = '';
  AddDeviceResponse? _addDeviceResponse;
  AssignDeviceToUserResponse? _assignDeviceToUserResponse;
  ChargeLevelTypesResponse? _chargeLevelTypesResponse;
  ChargeLevelTypes? _selectedChargeLevelType;
  String _inverterTypeId = "-1";
  List<DeviceList>? _deviceList;
  GetDevicesByUserLocationResponse? _devicesByUserLocationResponse;
  CommonSuccessResponse? _commonSuccessResponse;
  DeletedDevice? _deletedDeviceResponse;

  bool get isLoading => _isLoading;
  String get errorMessage => _errorMessage;
  String get inverterTypeId => _inverterTypeId;
  ChargeLevelTypes? get getSelectedChargeLevelType => _selectedChargeLevelType;
  AddDeviceResponse? get getAddDeviceResponse => _addDeviceResponse;
  ChargeLevelTypesResponse? get getChargeLevelTypesResponse => _chargeLevelTypesResponse;
  AssignDeviceToUserResponse? get getAssignDeviceToUserResponse => _assignDeviceToUserResponse;
  GetDevicesByUserLocationResponse? get getDevicesByUserLocationResponse => _devicesByUserLocationResponse;
  List<DeviceList>? get getDeviceList => _deviceList;
  CommonSuccessResponse? get commonSuccessResponse => _commonSuccessResponse;
  DeletedDevice? get deletedDeviceResponse => _deletedDeviceResponse;

  set setCommonSuccessResponse(CommonSuccessResponse value) {
    _commonSuccessResponse = value;
    notifyListeners();
  }

  set setDeletedDeviceResponse(DeletedDevice value) {
    _deletedDeviceResponse = value;
    notifyListeners();
  }

  set setInverterTypeId(String value) {
    _inverterTypeId = value;
    notifyListeners();
  }

  set setDeviceList(List<DeviceList> value) {
    _deviceList = value;
    notifyListeners();
  }

  set setSelectedChargeLevelType(ChargeLevelTypes value) {
    _selectedChargeLevelType = value;
    notifyListeners();
  }

  set setChargeLevelTypesResponse(ChargeLevelTypesResponse value) {
    _chargeLevelTypesResponse = value;
    notifyListeners();
  }

  set setDevicesByUserLocation(GetDevicesByUserLocationResponse value) {
    _devicesByUserLocationResponse = value;
    notifyListeners();
  }

  set setAddDeviceResponse(AddDeviceResponse value) {
    _addDeviceResponse = value;
    notifyListeners();
  }

  set setAssignDeviceToUserResponse(AssignDeviceToUserResponse value) {
    _assignDeviceToUserResponse = value;
    notifyListeners();
  }

  set setIsLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  set setErrorMessage(String value) {
    _errorMessage = value;
    notifyListeners();
  }

  assignDeviceToUser(
      {required String serialNumber, required int locationId, String? locationName, required String pin}) async {
    try {
      ApiResponse apiResponse = await deviceService.assignDeviceToUser(
          serialNumber: serialNumber, locationId: locationId, locationName: locationName, pin: pin);
      if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
        var response = AssignDeviceToUserResponse.fromJson(json);
        if (response.isSuccess ?? false) {
          setAssignDeviceToUserResponse = response;
          return response;
        } else {
          setAssignDeviceToUserResponse = AssignDeviceToUserResponse();
          setErrorMessage = response.message ?? "Something Went Wrong, Please contact an admin!";
        }
      } else {
        setAssignDeviceToUserResponse = AssignDeviceToUserResponse();
        handleError(apiResponse);
      }
    } catch (e) {
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
      setAssignDeviceToUserResponse = AssignDeviceToUserResponse();
    }
    notifyListeners();
  }

  getDevicesByUserLocation({required int locationId}) async {
    setIsLoading = true;
    ApiResponse apiResponse = await deviceService.getDevicesByUserLocation(locationId: locationId);
    setIsLoading = false;
    if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
      var response = GetDevicesByUserLocationResponse.fromJson(json);
      if (response.isSuccess ?? false) {
        setDevicesByUserLocation = response;
        setDeviceList = response.payload?.deviceList ?? [];
        return response;
      } else {
        setDevicesByUserLocation = GetDevicesByUserLocationResponse();
        setErrorMessage = response.message ?? "Something Went Wrong, Please contact an admin!";
      }
    } else {
      setDevicesByUserLocation = GetDevicesByUserLocationResponse();
      handleError(apiResponse);
    }
    notifyListeners();
  }

  getAllChargeLevelTypes() async {
    try {
      ApiResponse apiResponse = await deviceService.getAllChargeLevelTypes();
      if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
        Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
        var response = ChargeLevelTypesResponse.fromJson(json);
        if (response.isSuccess ?? false) {
          setChargeLevelTypesResponse = response;
          return response;
        } else {
          setChargeLevelTypesResponse = ChargeLevelTypesResponse();
          setErrorMessage = response.message ?? "Something Went Wrong, Please contact an admin!";
        }
      } else {
        setChargeLevelTypesResponse = ChargeLevelTypesResponse();
        handleError(apiResponse);
      }
    } catch (e) {
      setErrorMessage = "Something Went Wrong, Please contact an admin!";
    }
    notifyListeners();
  }

  getDeletedDevices(int locationId) async {
    ApiResponse apiResponse = await deviceService.getDeletedDevices(locationId);
    if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
      var response = DeletedDevice.fromJson(json);
      if (response.isSuccess ?? false) {
        setDeletedDeviceResponse = response;
        return response;
      } else {
        setDeletedDeviceResponse = DeletedDevice();
        setErrorMessage = response.message ?? "Something Went Wrong, Please contact an admin!";
      }
    } else {
      setDeletedDeviceResponse = DeletedDevice();
      handleError(apiResponse);
    }
    notifyListeners();
  }

  updateDeviceDeleteStatus(int deviceId, bool isDelete) async {
    ApiResponse apiResponse = await deviceService.updateDeviceDeleteStatus(deviceId, isDelete);
    if (apiResponse.response != null && apiResponse.response!.statusCode == 200) {
      Map<String, dynamic> json = jsonDecode(apiResponse.response!.data);
      var response = CommonSuccessResponse.fromJson(json);
      if (response.isSuccess ?? false) {
        setCommonSuccessResponse = response;
        return response;
      } else {
        setCommonSuccessResponse = CommonSuccessResponse();
        setErrorMessage = response.message ?? "Something Went Wrong, Please contact an admin!";
      }
    } else {
      setCommonSuccessResponse = CommonSuccessResponse();
      handleError(apiResponse);
    }
    notifyListeners();
  }

  handleError(ApiResponse apiResponse) {
    String? errorMessage;
    if (apiResponse.error is String) {
      errorMessage = apiResponse.error.toString();
    } else {
      ErrorResponse errorResponse = apiResponse.error;
      errorMessage = errorResponse.errors![0].message;
    }
    setErrorMessage = errorMessage ?? "Something went wrong!";
  }
}
