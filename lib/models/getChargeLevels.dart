class GetChargeLevelsResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  GetChargeLevelsResponse(
      {this.isSuccess, this.errorCode, this.message, this.payload});

  GetChargeLevelsResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  int? batteryId;
  String? serialNumber;
  ChargeLevels? chargeLevels;

  Payload({this.batteryId, this.serialNumber, this.chargeLevels});

  Payload.fromJson(Map<String, dynamic> json) {
    batteryId = json['batteryId'];
    serialNumber = json['serialNumber'];
    chargeLevels = json['chargeLevels'] != null
        ? ChargeLevels.fromJson(json['chargeLevels'])
        : null;
  }
}

class ChargeLevels {
  double? chargeLimit;
  double? dischargeLimit;
  bool? useReverseCapacity;
  int? inverterTypeId;

  ChargeLevels(
      {this.chargeLimit, this.dischargeLimit, this.useReverseCapacity, this.inverterTypeId});

  ChargeLevels.fromJson(Map<String, dynamic> json) {
    chargeLimit = json['chargeLimit'];
    dischargeLimit = json['dischargeLimit'];
    useReverseCapacity = json['useReverseCapacity'];
    inverterTypeId = json['inverterTypeId'];
  }
}
