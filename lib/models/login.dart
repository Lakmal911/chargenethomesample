import 'package:home/models/getLocationByUser.dart';

class LoginResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  LoginResponse({this.isSuccess, this.errorCode, this.message, this.payload});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  int? loginId;
  int? userId;
  String? username;
  String? firstName;
  String? lastName;
  int? roleId;
  String? roleName;
  String? token;
  List<LocationList>? locationList;
  LocationList? defaultLocation;

  Payload(
      {this.loginId,
        this.userId,
        this.username,
        this.firstName,
        this.lastName,
        this.roleId,
        this.roleName,
        this.token,
        this.locationList,
        this.defaultLocation});

  Payload.fromJson(Map<String, dynamic> json) {
    loginId = json['loginId'];
    userId = json['userId'];
    username = json['username'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    roleId = json['roleId'];
    roleName = json['roleName'];
    token = json['token'];
    if (json['locationList'] != null) {
      locationList = <LocationList>[];
      json['locationList'].forEach((v) {
        locationList!.add(LocationList.fromJson(v));
      });
    }
    defaultLocation = json['defaultLocation'] != null
        ? LocationList.fromJson(json['defaultLocation'])
        : null;
  }
}

