class GetRegisteredLocation {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  GetRegisteredLocation(
      {this.isSuccess, this.errorCode, this.message, this.payload});

  GetRegisteredLocation.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  List<DeviceLocationList>? locationList;

  Payload({this.locationList});

  Payload.fromJson(Map<String, dynamic> json) {
    if (json['locationList'] != null) {
      locationList = <DeviceLocationList>[];
      json['locationList'].forEach((v) {
        locationList!.add(DeviceLocationList.fromJson(v));
      });
    }
  }
}

class DeviceLocationList {
  int? locationId;
  String? locationName;

  DeviceLocationList({this.locationId, this.locationName});

  DeviceLocationList.fromJson(Map<String, dynamic> json) {
    locationId = json['locationId'];
    locationName = json['locationName'];
  }
  @override
  String toString() => locationName!;
}
