class GetDevicesByUserLocationResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  GetDevicesByUserLocationResponse(
      {this.isSuccess, this.errorCode, this.message, this.payload});

  GetDevicesByUserLocationResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  List<DeviceList>? deviceList;

  Payload({this.deviceList});

  Payload.fromJson(Map<String, dynamic> json) {
    if (json['deviceList'] != null) {
      deviceList = <DeviceList>[];
      json['deviceList'].forEach((v) {
        deviceList!.add( DeviceList.fromJson(v));
      });
    }
  }
}

class DeviceList {
  int? deviceId;
  int? componentId;
  String? serialNumber;
  DeviceTypeInfo? deviceTypeInfo;
  bool? isConnected;
  bool? isActive;
  bool? isDeleted;

  DeviceList(
      {this.deviceId,
        this.componentId,
        this.serialNumber,
        this.deviceTypeInfo,
        this.isConnected,
        this.isActive,
        this.isDeleted});

  DeviceList.fromJson(Map<String, dynamic> json) {
    deviceId = json['deviceId'];
    componentId = json['componentId'];
    serialNumber = json['serialNumber'];
    deviceTypeInfo = json['deviceTypeInfo'] != null
        ? DeviceTypeInfo.fromJson(json['deviceTypeInfo'])
        : null;
    isConnected = json['isConnected'];
    isActive = json['isActive'];
    isDeleted = json['isDeleted'];
  }
}

class DeviceTypeInfo {
  int? deviceTypeId;
  String? deviceTypeName;

  DeviceTypeInfo({this.deviceTypeId, this.deviceTypeName});

  DeviceTypeInfo.fromJson(Map<String, dynamic> json) {
    deviceTypeId = json['deviceTypeId'];
    deviceTypeName = json['deviceTypeName'];
  }
}
