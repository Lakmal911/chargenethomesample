class GetLocationByIdResponse {
  bool? isSuccess;
  int? errorCode;
  Message? message;

  GetLocationByIdResponse({this.isSuccess, this.errorCode, this.message});

  GetLocationByIdResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message =
    json['message'] != null ? Message.fromJson(json['message']) : null;
  }
}

class Message {
  Location? location;

  Message({this.location});

  Message.fromJson(Map<String, dynamic> json) {
    location = json['location'] != null
        ? Location.fromJson(json['location'])
        : null;
  }
}

class Location {
  int? locationId;
  String? locationName;

  Location({this.locationId, this.locationName});

  Location.fromJson(Map<String, dynamic> json) {
    locationId = json['locationId'];
    locationName = json['locationName'];
  }
}
