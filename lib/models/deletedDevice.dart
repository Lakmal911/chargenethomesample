class DeletedDevice {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  DeletedDevice({this.isSuccess, this.errorCode, this.message, this.payload});

  DeletedDevice.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  List<DeletedDeviceList>? deletedDeviceList;

  Payload({this.deletedDeviceList});

  Payload.fromJson(Map<String, dynamic> json) {
    if (json['deletedDeviceList'] != null) {
      deletedDeviceList = <DeletedDeviceList>[];
      json['deletedDeviceList'].forEach((v) {
        deletedDeviceList!.add(DeletedDeviceList.fromJson(v));
      });
    }
  }
}

class DeletedDeviceList {
  int? deviceId;
  String? serialNumber;
  int? componentId;
  DeviceTypeInfo? deviceTypeInfo;
  LocationInfo? locationInfo;
  bool? isActive;
  bool? isDeleted;

  DeletedDeviceList(
      {this.deviceId,
        this.serialNumber,
        this.componentId,
        this.deviceTypeInfo,
        this.locationInfo,
        this.isActive,
        this.isDeleted});

  DeletedDeviceList.fromJson(Map<String, dynamic> json) {
    deviceId = json['deviceId'];
    serialNumber = json['serialNumber'];
    componentId = json['componentId'];
    deviceTypeInfo = json['deviceTypeInfo'] != null
        ? DeviceTypeInfo.fromJson(json['deviceTypeInfo'])
        : null;
    locationInfo = json['locationInfo'] != null
        ? LocationInfo.fromJson(json['locationInfo'])
        : null;
    isActive = json['isActive'];
    isDeleted = json['isDeleted'];
  }
}

class DeviceTypeInfo {
  int? deviceTypeId;
  String? deviceTypeName;

  DeviceTypeInfo({this.deviceTypeId, this.deviceTypeName});

  DeviceTypeInfo.fromJson(Map<String, dynamic> json) {
    deviceTypeId = json['deviceTypeId'];
    deviceTypeName = json['deviceTypeName'];
  }
}

class LocationInfo {
  int? locationId;
  String? locationName;

  LocationInfo({this.locationId, this.locationName});

  LocationInfo.fromJson(Map<String, dynamic> json) {
    locationId = json['locationId'];
    locationName = json['locationName'];
  }
}
