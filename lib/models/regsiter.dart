class RegisterResponse{
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  RegisterResponse({this.isSuccess, this.errorCode, this.message, this.payload});

  RegisterResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  String? result;
  int? userId;
  String? username;

  Payload({this.result, this.userId, this.username});

  Payload.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    userId = json['userId'];
    username = json['username'];
  }
}