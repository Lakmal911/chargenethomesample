class Command {
  String? command;

  Command({this.command});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['command'] = command;
    return data;
  }
}
