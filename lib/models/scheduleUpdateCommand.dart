class ScheduleUpdateCommand {
  String? command;
  String? scheduleStartTimeWeekDays;
  String? scheduleStopTimeWeekDays;
  String? scheduleStartTimeWeekEnds;
  String? scheduleStopTimeWeekEnds;

  ScheduleUpdateCommand(
      {this.command,
        this.scheduleStartTimeWeekDays,
        this.scheduleStopTimeWeekDays,
        this.scheduleStartTimeWeekEnds,
        this.scheduleStopTimeWeekEnds});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['command'] = command;
    data['scheduleStartTimeWeekDays'] = scheduleStartTimeWeekDays;
    data['scheduleStopTimeWeekDays'] = scheduleStopTimeWeekDays;
    data['scheduleStartTimeWeekEnds'] = scheduleStartTimeWeekEnds;
    data['scheduleStopTimeWeekEnds'] = scheduleStopTimeWeekEnds;
    return data;
  }
}
