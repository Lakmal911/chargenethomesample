class CommonSuccessResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;

  CommonSuccessResponse({this.isSuccess, this.errorCode, this.message});

  CommonSuccessResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
  }
}
