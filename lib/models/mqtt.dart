class Mqtt {
  String? command;
  String? state;
  String? current;
  String? voltage;
  String? power;
  String? chargeDuration;
  String? energy;
  String? chargerLockState;
  String? faultState;
  String? loadBalancingState;
  String? scheduleChargeEnableState;
  String? runningAppVersion;
  String? scheduleStartTimeWeekDays;
  String? scheduleStopTimeWeekDays;
  String? scheduleStartTimeWeekEnds;
  String? scheduleStopTimeWeekEnds;

  Mqtt(
      {this.command,
      this.state,
      this.current,
      this.voltage,
      this.power,
      this.chargeDuration,
      this.energy,
      this.chargerLockState,
      this.faultState,
      this.loadBalancingState,
      this.scheduleChargeEnableState,
      this.runningAppVersion,
      this.scheduleStartTimeWeekDays,
      this.scheduleStopTimeWeekDays,
      this.scheduleStartTimeWeekEnds,
      this.scheduleStopTimeWeekEnds});

  Mqtt.fromJson(Map<String, dynamic> json) {
    command = json['command'];
    state = json['state'];
    current = json['current'];
    voltage = json['voltage'];
    power = json['power'];
    chargeDuration = json['chargeDuration'];
    energy = json['energy'];
    chargerLockState = json['chargerLockState'];
    faultState = json['faultState'];
    loadBalancingState = json['loadBalancingEnableState'];
    scheduleChargeEnableState = json['scheduleChargeEnableState'];
    runningAppVersion = json['runningAppVersion'];
    scheduleStartTimeWeekDays = json['scheduleStartTimeWeekDays'];
    scheduleStopTimeWeekDays = json['scheduleStopTimeWeekDays'];
    scheduleStartTimeWeekEnds = json['scheduleStartTimeWeekEnds'];
    scheduleStopTimeWeekEnds = json['scheduleStopTimeWeekEnds'];
  }
}
