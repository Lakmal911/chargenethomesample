class GetAllDevicesTypesResponse {
  bool? isSuccess;
  int? errorCode;
  Message? message;

  GetAllDevicesTypesResponse({this.isSuccess, this.errorCode, this.message});

  GetAllDevicesTypesResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message =
    json['message'] != null ? Message.fromJson(json['message']) : null;
  }
}

class Message {
  List<DeviceTypeList>? deviceTypeList;

  Message({this.deviceTypeList});

  Message.fromJson(Map<String, dynamic> json) {
    if (json['deviceTypeList'] != null) {
      deviceTypeList = <DeviceTypeList>[];
      json['deviceTypeList'].forEach((v) {
        deviceTypeList!.add(DeviceTypeList.fromJson(v));
      });
    }
  }
}

class DeviceTypeList {
  int? deviceTypeId;
  String? deviceTypeName;

  DeviceTypeList({this.deviceTypeId, this.deviceTypeName});

  DeviceTypeList.fromJson(Map<String, dynamic> json) {
    deviceTypeId = json['deviceTypeId'];
    deviceTypeName = json['deviceTypeName'];
  }
}
