class GetUserByIdResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  GetUserByIdResponse(
      {this.isSuccess, this.errorCode, this.message, this.payload});

  GetUserByIdResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  User? user;

  Payload({this.user});

  Payload.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }
}

class User {
  int? userId;
  String? firstName;
  String? lastName;
  String? username;
  String? email;
  String? contactNumber;
  Role? role;
  bool? isActive;

  User(
      {this.userId,
        this.firstName,
        this.lastName,
        this.username,
        this.email,
        this.contactNumber,
        this.role,
        this.isActive});

  User.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    username = json['username'];
    email = json['email'];
    contactNumber = json['contactNumber'];
    role = json['role'] != null ? Role.fromJson(json['role']) : null;
    isActive = json['isActive'];
  }
}

class Role {
  int? roleId;
  String? roleName;

  Role({this.roleId, this.roleName});

  Role.fromJson(Map<String, dynamic> json) {
    roleId = json['roleId'];
    roleName = json['roleName'];
  }
}
