class GetDevicesStatusResponse {
  bool? isSuccess;
  int? errorCode;
  Message? message;

  GetDevicesStatusResponse({this.isSuccess, this.errorCode, this.message});

  GetDevicesStatusResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message =
    json['message'] != null ?  Message.fromJson(json['message']) : null;
  }
}

class Message {
  BatteryCell? batteryCell;

  Message({this.batteryCell});

  Message.fromJson(Map<String, dynamic> json) {
    batteryCell = json['batteryCell'] != null
        ? BatteryCell.fromJson(json['batteryCell'])
        : null;
  }
}

class BatteryCell {
  int? batteryId;
  String? serialNumber;
  String? powerFlowStatus;
  String? batteryStatus;

  BatteryCell(
      {this.batteryId,
        this.serialNumber,
        this.powerFlowStatus,
        this.batteryStatus});

  BatteryCell.fromJson(Map<String, dynamic> json) {
    batteryId = json['batteryId'];
    serialNumber = json['serialNumber'];
    powerFlowStatus = json['powerFlowStatus'];
    batteryStatus = json['batteryStatus'];
  }
}
