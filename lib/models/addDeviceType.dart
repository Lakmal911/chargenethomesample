class AddDeviceResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  AddDeviceResponse(
      {this.isSuccess, this.errorCode, this.message, this.payload});

  AddDeviceResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  String? result;
  int? deviceId;

  Payload({this.result, this.deviceId});

  Payload.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    deviceId = json['deviceId'];
  }
}
