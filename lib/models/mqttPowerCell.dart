class MqttPowerCellResponse {
  String? deviceType;
  String? serialNumber;
  Data? data;

  MqttPowerCellResponse({this.deviceType, this.serialNumber, this.data});

  MqttPowerCellResponse.fromJson(Map<String, dynamic> json) {
    deviceType = json['deviceType'];
    serialNumber = json['serialNumber'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
}

class Data {
  String? powerFlowStatus;
  String? current;
  String? voltage;
  String? temperature;
  String? soc;
  String? soh;
  String? totalEnergy;

  Data(
      {this.powerFlowStatus,
        this.current,
        this.voltage,
        this.temperature,
        this.soc,
        this.soh,
        this.totalEnergy});

  Data.fromJson(Map<String, dynamic> json) {
    powerFlowStatus = json['powerFlowStatus'];
    current = json['current'];
    voltage = json['voltage'];
    temperature = json['temperature'];
    soc = json['soc'];
    soh = json['soh'];
    totalEnergy = json['totalEnergy'];
  }
}
