class ChargeLevelTypesResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  List<ChargeLevelTypes>? chargeLevelTypes;

  ChargeLevelTypesResponse(
      {this.isSuccess, this.errorCode, this.message, this.chargeLevelTypes});

  ChargeLevelTypesResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    if (json['payload'] != null) {
      chargeLevelTypes = <ChargeLevelTypes>[];
      json['payload'].forEach((v) {
        chargeLevelTypes!.add(ChargeLevelTypes.fromJson(v));
      });
    }
  }
}

class ChargeLevelTypes {
  String? idInverterType;
  String? type;

  ChargeLevelTypes({this.idInverterType, this.type});

  ChargeLevelTypes.fromJson(Map<String, dynamic> json) {
    idInverterType = json['id_inverter_type'];
    type = json['type'];
  }

  @override
  String toString() => type!;
}
