class LogoutResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;

  LogoutResponse({this.isSuccess, this.errorCode, this.message});

  LogoutResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
  }
}
