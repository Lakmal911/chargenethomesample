class GetBackupTimeResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  GetBackupTimeResponse(
      {this.isSuccess, this.errorCode, this.message, this.payload});

  GetBackupTimeResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  int? batteryId;
  String? serialNumber;
  BackupTime? backupTime;

  Payload({this.batteryId, this.serialNumber, this.backupTime});

  Payload.fromJson(Map<String, dynamic> json) {
    batteryId = json['batteryId'];
    serialNumber = json['serialNumber'];
    backupTime = json['backupTime'] != null
        ? BackupTime.fromJson(json['backupTime'])
        : null;
  }
}

class BackupTime {
  Weekday? weekday;
  Weekday? weekend;

  BackupTime({this.weekday, this.weekend});

  BackupTime.fromJson(Map<String, dynamic> json) {
    weekday =
    json['weekday'] != null ? Weekday.fromJson(json['weekday']) : null;
    weekend =
    json['weekend'] != null ? Weekday.fromJson(json['weekend']) : null;
  }
}

class Weekday {
  Morning? morning;
  Morning? evening;

  Weekday({this.morning, this.evening});

  Weekday.fromJson(Map<String, dynamic> json) {
    morning =
    json['morning'] != null ? Morning.fromJson(json['morning']) : null;
    evening =
    json['evening'] != null ? Morning.fromJson(json['evening']) : null;
  }
}

class Morning {
  String? startAt;
  String? endAt;

  Morning({this.startAt, this.endAt});

  Morning.fromJson(Map<String, dynamic> json) {
    startAt = json['startAt'];
    endAt = json['endAt'];
  }
}
