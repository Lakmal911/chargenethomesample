class GetDeviceByIdResponse {
  bool? isSuccess;
  int? errorCode;
  Message? message;

  GetDeviceByIdResponse({this.isSuccess, this.errorCode, this.message});

  GetDeviceByIdResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message =
    json['message'] != null ? Message.fromJson(json['message']) : null;
  }
}

class Message {
  Device? device;

  Message({this.device});

  Message.fromJson(Map<String, dynamic> json) {
    device =
    json['device'] != null ? Device.fromJson(json['device']) : null;
  }
}

class Device {
  int? deviceId;
  DeviceType? deviceType;
  String? serialNumber;
  bool? isActive;
  bool? isDeleted;
  Location? location;

  Device(
      {this.deviceId,
        this.deviceType,
        this.serialNumber,
        this.isActive,
        this.isDeleted,
        this.location});

  Device.fromJson(Map<String, dynamic> json) {
    deviceId = json['deviceId'];
    deviceType = json['deviceType'] != null
        ? DeviceType.fromJson(json['deviceType'])
        : null;
    serialNumber = json['serialNumber'];
    isActive = json['isActive'];
    isDeleted = json['isDeleted'];
    location = json['location'] != null
        ? Location.fromJson(json['location'])
        : null;
  }
}

class DeviceType {
  int? deviceTypeId;
  String? deviceTypeName;

  DeviceType({this.deviceTypeId, this.deviceTypeName});

  DeviceType.fromJson(Map<String, dynamic> json) {
    deviceTypeId = json['deviceTypeId'];
    deviceTypeName = json['deviceTypeName'];
  }
}

class Location {
  int? locationId;
  String? locationName;
  User? user;

  Location({this.locationId, this.locationName, this.user});

  Location.fromJson(Map<String, dynamic> json) {
    locationId = json['locationId'];
    locationName = json['locationName'];
    user = json['user'] != null ? User.fromJson(json['user']) : null;
  }
}

class User {
  int? userId;
  String? firstName;
  String? lastName;
  String? username;
  String? email;
  String? contactNumber;
  Role? role;
  bool? isActive;

  User(
      {this.userId,
        this.firstName,
        this.lastName,
        this.username,
        this.email,
        this.contactNumber,
        this.role,
        this.isActive});

  User.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    username = json['username'];
    email = json['email'];
    contactNumber = json['contactNumber'];
    role = json['role'] != null ? Role.fromJson(json['role']) : null;
    isActive = json['isActive'];
  }
}

class Role {
  int? roleId;
  String? roleName;

  Role({this.roleId, this.roleName});

  Role.fromJson(Map<String, dynamic> json) {
    roleId = json['roleId'];
    roleName = json['roleName'];
  }
}
