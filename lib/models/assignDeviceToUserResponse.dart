class AssignDeviceToUserResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  AssignDeviceToUserResponse(
      {this.isSuccess, this.errorCode, this.message, this.payload});

  AssignDeviceToUserResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  int? mappingId;
  String? serialNumber;
  LocationInfo? locationInfo;
  DeviceTypeInfo? deviceTypeInfo;
  int? deviceId;
  int? componentId;

  Payload(
      {this.mappingId,
        this.serialNumber,
        this.locationInfo,
        this.deviceTypeInfo,
        this.deviceId,
        this.componentId});

  Payload.fromJson(Map<String, dynamic> json) {
    mappingId = json['mappingId'];
    serialNumber = json['serialNumber'];
    locationInfo = json['locationInfo'] != null
        ? LocationInfo.fromJson(json['locationInfo'])
        : null;
    deviceTypeInfo = json['deviceTypeInfo'] != null
        ? DeviceTypeInfo.fromJson(json['deviceTypeInfo'])
        : null;
    deviceId = json['deviceId'];
    componentId = json['componentId'];
  }
}

class LocationInfo {
  bool? isNew;
  int? locationId;
  String? locationName;

  LocationInfo({this.isNew, this.locationId, this.locationName});

  LocationInfo.fromJson(Map<String, dynamic> json) {
    isNew = json['isNew'];
    locationId = json['locationId'];
    locationName = json['locationName'];
  }
}

class DeviceTypeInfo {
  int? typeId;
  String? typeName;

  DeviceTypeInfo({this.typeId, this.typeName});

  DeviceTypeInfo.fromJson(Map<String, dynamic> json) {
    typeId = json['typeId'];
    typeName = json['typeName'];
  }
}
