class GetLocationByUserResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  GetLocationByUserResponse(
      {this.isSuccess, this.errorCode, this.message, this.payload});

  GetLocationByUserResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  List<LocationList>? locationList;

  Payload({this.locationList});

  Payload.fromJson(Map<String, dynamic> json) {
    if (json['locationList'] != null) {
      locationList = <LocationList>[];
      json['locationList'].forEach((v) {
        locationList!.add(LocationList.fromJson(v));
      });
    }
  }
}

class LocationList {
  int? locationId;
  String? locationName;

  LocationList({this.locationId, this.locationName});

  LocationList.fromJson(Map<String, dynamic> json) {
    locationId = json['locationId'];
    locationName = json['locationName'];
  }
  @override
  String toString() => locationName!;
}
