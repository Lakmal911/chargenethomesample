class AddLocationResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  AddLocationResponse(
      {this.isSuccess, this.errorCode, this.message, this.payload});

  AddLocationResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  String? result;
  int? locationId;
  String? locationName;

  Payload({this.result, this.locationId, this.locationName});

  Payload.fromJson(Map<String, dynamic> json) {
    result = json['result'];
    locationId = json['locationId'];
    locationName = json['locationName'];
  }
}
