class GetSocHistoryResponse {
  bool? isSuccess;
  int? errorCode;
  String? message;
  Payload? payload;

  GetSocHistoryResponse(
      {this.isSuccess, this.errorCode, this.message, this.payload});

  GetSocHistoryResponse.fromJson(Map<String, dynamic> json) {
    isSuccess = json['isSuccess'];
    errorCode = json['errorCode'];
    message = json['message'];
    payload =
    json['payload'] != null ? Payload.fromJson(json['payload']) : null;
  }
}

class Payload {
  int? batteryId;
  List<UsageData>? usageData;

  Payload({this.batteryId, this.usageData});

  Payload.fromJson(Map<String, dynamic> json) {
    batteryId = json['batteryId'];
    if (json['usageData'] != null) {
      usageData = <UsageData>[];
      json['usageData'].forEach((v) {
        usageData!.add(UsageData.fromJson(v));
      });
    }
  }
}

class UsageData {
  String? socTime;
  double? socValue;

  UsageData({this.socTime, this.socValue});

  UsageData.fromJson(Map<String, dynamic> json) {
    socTime = json['socTime'];
    socValue = json['socValue'];
  }
}
