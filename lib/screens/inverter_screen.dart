import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/textStyles.dart';

class InverterScreen extends StatefulWidget {
  const InverterScreen({Key? key}) : super(key: key);

  @override
  State<InverterScreen> createState() => _InverterScreenState();
}

class _InverterScreenState extends State<InverterScreen> {

  Widget tabText(String tabName, bool selected){
    return Text(
      tabName,
      style: gilroyBoldTextStyle.copyWith(
          color: selected ? kColorWhite : kColorWhite.withOpacity(0.1),
          fontSize: Dimensions.fontSizeExtraSmall,
          letterSpacing: 2
      ),
    );
  }

  Widget bottomTabComponent(String name, String price, String kwh){
    return Padding(
      padding: const EdgeInsets.only(bottom: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            name,
            style: gilroyBoldTextStyle.copyWith(
                fontSize: Dimensions.fontSizeDefault,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Stack(
                children: [
                  Container(
                    height: 15,
                    width: 45.w,
                    color: kAccentColor,
                  ),
                  Container(
                    width: 45,
                    height: 15,
                    decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: kSecondaryColor,
                      boxShadow: [
                        BoxShadow(
                          color: kSecondaryColor.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: const Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 40,
                width: 40.w,
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0),
                  child: Center(
                    child: Text(
                      "$price RS | $kwh kWh",
                      style: gilroyRegularTextStyle.copyWith(
                        fontWeight: FontWeight.w600,
                        fontSize: Dimensions.fontSizeExtraSmall,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget itemInfoComponent(String imgUrl, String kwh){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "$kwh kWh",
              style: gilroyRegularTextStyle.copyWith(
                  fontWeight: FontWeight.w500,
                  fontSize: Dimensions.fontSizeLarge,
                  letterSpacing: 2
              ),
            ),
          ],
        )
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // child:  Consumer<CommonProvider>(
      //     builder: (context, commonProvider, child) {
      //       return Column(
      //         crossAxisAlignment: CrossAxisAlignment.center,
      //         children: [
      //           Expanded(
      //             flex: 6,
      //             child: Column(
      //               children: [
      //                 //screen title
      //                 CustomAppBar(
      //                   title: inverter,
      //                   isBackButtonExist: false,
      //                   isAction: false,
      //                   iconOnTap: (){
      //
      //                   },
      //                 ),
      //                 Column(
      //                   children: [
      //                     Padding(
      //                       padding: const EdgeInsets.symmetric(horizontal: 55.0, vertical: 8.0),
      //                       child: Row(
      //                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //                         children: [
      //                           GestureDetector(
      //                             onTap: (){
      //                               errorMSGAlert(
      //                                 context: context,
      //                                 title: "title",
      //                                 msg: "jksdkfjsdkjfk jsdkj fksjd f",
      //                                 height: 200.0,
      //                                 onCancel: (){
      //                                   Navigator.pop(context);
      //                                 },
      //                               );
      //                           },
      //                               child: tabText("TODAY", true)
      //                           ),
      //                           tabText("YESTERDAY", false),
      //                           tabText("WEEK", false),
      //                           tabText("MONTH", false),
      //                         ],
      //                       ),
      //                     ),
      //
      //                     //item info component
      //                     Padding(
      //                       padding: const EdgeInsets.fromLTRB(40, 10, 40, 0),
      //                       child: Column(
      //                         crossAxisAlignment: CrossAxisAlignment.center,
      //                         children: [
      //                           Row(
      //                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //                             crossAxisAlignment: CrossAxisAlignment.start,
      //                             children: [
      //                               itemInfoComponent("grid.svg", "20"),
      //                               itemInfoComponent("solar.svg", "30"),
      //                             ],
      //                           ),
      //                           Center(child: itemInfoComponent("powercell.svg", "50") ),
      //                         ],
      //                       ),
      //                     ),
      //
      //                     //
      //                   ],
      //                 ),
      //               ],
      //             ),
      //           ),
      //
      //           //time of use view
      //           Expanded(
      //             flex: 7,
      //             child: Padding(
      //               padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 4),
      //               child: Column(
      //                 crossAxisAlignment: CrossAxisAlignment.start,
      //                 children: [
      //                   //time of use tab view
      //                   Column(
      //                     children: [
      //                       Padding(
      //                         padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 8.0),
      //                         child: SizedBox(
      //                           height: 321,
      //                           child: DefaultTabController(
      //                             length: 2,
      //                             child: Scaffold(
      //                               backgroundColor: Colors.transparent,
      //                               appBar: TabBar(
      //                                   physics: const NeverScrollableScrollPhysics(),
      //                                   indicatorColor: kSecondaryColor,
      //                                   tabs: [
      //                                     Text(
      //                                       "PEAK",
      //                                       style: gilroyBoldTextStyle.copyWith(
      //                                           fontWeight: FontWeight.w600,
      //                                           fontSize: Dimensions.fontSizeExtraLarge,
      //                                           letterSpacing: 2
      //                                       ),
      //                                     ),
      //                                     Text(
      //                                       "OFF PEAK",
      //                                       style: gilroyBoldTextStyle.copyWith(
      //                                         fontWeight: FontWeight.w600,
      //                                         fontSize: Dimensions.fontSizeExtraLarge,
      //                                         letterSpacing: 2
      //                                       ),
      //                                     ),
      //                                   ]
      //                               ),
      //                               body: TabBarView(
      //                                   physics: const NeverScrollableScrollPhysics(),
      //                                   children: [
      //                                     Column(
      //                                       children: [
      //                                         const SizedBox(
      //                                           height: 30,
      //                                         ),
      //                                         bottomTabComponent("Power Cell", "20,000", "450"),
      //                                         bottomTabComponent("Solar", "15,000", "35"),
      //                                         bottomTabComponent("Grid", "4,400", "750"),
      //                                       ],
      //                                     ),
      //                                     Column(
      //                                       children: [
      //                                         const SizedBox(
      //                                           height: 30,
      //                                         ),
      //                                         bottomTabComponent("Power Cell", "20,000", "450"),
      //                                         bottomTabComponent("Solar", "15,000", "35"),
      //                                         bottomTabComponent("Grid", "4,400", "750"),
      //                                       ],
      //                                     )
      //                                   ]
      //                               ),
      //                             ),
      //                           ),
      //                         ),
      //                       ),
      //                     ],
      //                   )
      //                 ],
      //               ),
      //             ),
      //           ),
      //         ],
      //       );
      //     }
      // ),
    );
  }
}

