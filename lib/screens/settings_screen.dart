import 'package:flutter/material.dart';
import 'package:home/screens/routerConnection_screen.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/providers/userProvider.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/customNoInternetAlert.dart';
import 'package:home/reusableWidgets/loadingComponent.dart';
import 'package:home/screens/deviceInfo_screen.dart';
import 'package:home/screens/login_screen.dart';
import 'package:home/screens/profile_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';

import '../reusableWidgets/bottomNavBar.dart';

class SettingsScreen extends StatelessWidget {
  SettingsScreen({Key? key}) : super(key: key);

  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 500), (){
      if(getBatteryIsDeactivated() ?? false){
        mSGAlert(
          context: context,
          title: "No Devices",
          msg: "Please activate your devices or register a device to continue",
          onCancel: () {
            Navigator.pop(context);
          },
          height: 25.h,
        );
      }
    });

    return SafeArea(
      child: Container(
        child: Scaffold(
          key: scaffoldMessengerKey,
          backgroundColor: Colors.transparent,
          appBar: CustomAppBar(
            title: lSettings,
            isBackButtonExist: true,
            isAction: false,
            backOnTap: (){
              if(getBatteryIsDeactivated() ?? false){
                mSGAlert(
                  context: context,
                  title: "No Devices",
                  msg: "Please activate your devices or register a device to continue",
                  onCancel: () {
                    Navigator.pop(context);
                  },
                  height: 25.h,
                );
              }else{
                Navigator.push(context, MaterialPageRoute(builder: (context) => BottomNavBar()));
              }
            },
            iconOnTap: (){},
          ),
          body: Padding(
            padding: const EdgeInsets.fromLTRB(Dimensions.paddingOverLarge, Dimensions.paddingOverLarge, Dimensions.paddingOverLarge, 0),
            child: Consumer<UserProvider>(
                builder: (context, userProvider, child) {
                  return userProvider.isLoading ?
                  const LoadingComponent(loadingMsg: "Logout...")
                      : SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            settingsComponent(
                                icon: Icons.ad_units_rounded,
                                name: "Devices",
                                onTap: (){
                                  // Navigator.push(context, MaterialPageRoute(builder: (context) => DeviceInfoScreen()));
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => const RouterConnectionScreen()),
                                  );
                                }
                            ),
                            // (getIsPowerCellIPaired())
                            //     ?
                            const SizedBox.shrink()
                            //     : const Icon(
                            //   Icons.warning_amber_rounded,
                            //   color: Colors.red,
                            //   size: 25,
                            // ),
                          ],
                        ),
                        settingsComponent(
                            icon: Icons.account_circle_rounded,
                            name: "Profile",
                            onTap: (){
                              final userProvider = Provider.of<UserProvider>(context, listen: false);
                              userProvider.setIsEditPressed = false;
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ProfileScreen()));
                            }
                        ),
                        settingsComponent(
                            icon: Icons.anchor_rounded,
                            name: "Tarrifs",
                            onTap: (){

                            }
                        ),
                        settingsComponent(
                            icon: Icons.assignment,
                            name: "Terms",
                            onTap: (){

                            }
                        ),
                        settingsComponent(
                            icon: Icons.account_circle_rounded,
                            name: "Contact",
                            onTap: ()async{
                              final Uri params = Uri(
                                scheme: 'mailto',
                                path: 'info@vegapower.lk',
                                query: 'subject=VEGA POWER Feedback&body=Hello', //add subject and body here
                              );
                              var url = params.toString();
                              try{
                                await launchUrl(params);
                              } catch (e) {
                                print("my e : $e");
                              }
                            }
                        ),
                        Consumer<UserProvider>(
                            builder: (context, userProvider, child) {
                              return settingsComponent(
                                  icon: Icons.app_settings_alt,
                                  name: "Logout",
                                  onTap: (){
                                    confirmationAlert(
                                        context: context,
                                        title: "Logout",
                                        msg: "Are you sure you want to logout ?",
                                        onCancel: () async{
                                          Navigator.pop(context);
                                        },
                                        onYes: () async{
                                          Navigator.pop(context);
                                          logout(userProvider: userProvider);
                                        },
                                        yesButtonName: "Yes",
                                        noButtonName: "No",
                                        height: 25.h
                                    );
                                  }
                              );
                            }
                        ),

                      ],
                    ),
                  );
                }
            ),
          ),
        ),
      ),
    );
  }

  Widget settingsComponent({required IconData icon, required String name, required Function() onTap}){
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: Dimensions.paddingLarge, horizontal: Dimensions.paddingOverLarge),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          color: Colors.transparent,
          width: 60.w,
          child: Row(
            children: [
              Icon(icon, color: kColorBlack, size: 30),
              Padding(
                padding: const EdgeInsets.only(left: 20),
                child: Text(
                  name,
                  style: gilroyBoldTextStyle.copyWith(
                    fontSize: Dimensions.fontSizeExtraLarge,
                  ),
                  textAlign: TextAlign.start,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  logout({required UserProvider userProvider})async{
    userProvider.setIsLoading = true;
    await userProvider.getLogoutResponse();
    userProvider.setIsLoading = false;
    if(userProvider.logoutResponse?.isSuccess?? false){
      //clear providers and shared prefs then go to login page
      clearSharedPrefs();
      final mqttProvider = Provider.of<MqttProvider>(scaffoldMessengerKey.currentContext!, listen: false);
      mqttProvider.clearMqttProviderData();

      Navigator.pushReplacement(scaffoldMessengerKey.currentContext!, MaterialPageRoute(builder: (context) => LoginScreen()),);
    }else{
      confirmationAlert(
          context: scaffoldMessengerKey.currentContext!,
          title: userProvider.errorMessage == noInternet ? "No Connection" : "Oops",
          msg: userProvider.errorMessage,
          onCancel: () {
            Navigator.pop(scaffoldMessengerKey.currentContext!);
          },
          onYes: () async {
            Navigator.pop(scaffoldMessengerKey.currentContext!);
            logout(userProvider: userProvider);
          },
          yesButtonName: "Retry",
          noButtonName: "No",
          height: userProvider.errorMessage.length > 100 ? 30.h : 25.h
      );
    }
  }

  String? encodeQueryParameters(Map<String, String> params) {
    return params.entries
        .map((e) => '${Uri.encodeComponent(e.key)}=${Uri.encodeComponent(e.value)}')
        .join('&');
  }

  late Uri emailLaunchUri = Uri(
    scheme: 'mailto',
    path: 'info@vegapower.lk',
    query: encodeQueryParameters(<String, String>{
      'subject': 'VEGA POWER',
    }),
  );

}

