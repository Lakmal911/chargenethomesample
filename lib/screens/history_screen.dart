import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:home/models/getSocHistory.dart';
import 'package:home/providers/batteryCellProvider.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/loadingComponent.dart';
import 'package:home/reusableWidgets/sessionExpired.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';
import 'package:intl/intl.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart' as my_date_picker;

class HistoryScreen extends StatefulWidget {
  const HistoryScreen({Key? key}) : super(key: key);

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {

  GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    getSocHistoryData(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: scaffoldMessengerKey,
        backgroundColor: Colors.transparent,
        appBar: CustomAppBar(
          title: lHistory,
          isBackButtonExist: true,
          isAction: false,
          backOnTap: (){
            Navigator.pop(context);
          },
          iconOnTap: (){},
        ),
        body: Consumer<BatteryCellProvider>(
            builder: (context, batteryCellProvider, child) {
            return loadContent(batteryCellProvider);
          }
        ),
      ),
    );
  }

  getSocHistoryData(DateTime dateTime)async{
    final batteryCellProvider = Provider.of<BatteryCellProvider>(context, listen: false);
    getSocHistory(batteryCellProvider, dateTime);
  }

  getSocHistory(BatteryCellProvider batteryCellProvider, DateTime dateTime) async{
    batteryCellProvider.setIsLoading = true;
    await batteryCellProvider.getSocHistory(
      to: dateTime,
      from: dateTime,
    );
    batteryCellProvider.setIsLoading = false;
    if(batteryCellProvider.getSocHistoryResponse?.isSuccess?? false){
    }else if(batteryCellProvider.errorMessage == "Invalid access token"){
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    }else{
      if(batteryCellProvider.errorMessage != "No SoC usage history data found"){
        confirmationAlert(
            context: context,
            title: batteryCellProvider.errorMessage == noInternet ? "No Connection" : "Oops",
            msg: batteryCellProvider.errorMessage,
            onCancel: () {
              Navigator.pop(context);
            },
            onYes: () async {
              Navigator.pop(context);
              getSocHistory(batteryCellProvider, dateTime);
            },
            yesButtonName: "Retry",
            noButtonName: "No",
            height: batteryCellProvider.errorMessage.length > 100 ? 30.h : 25.h
        );
      }
    }
  }

  loadContent(BatteryCellProvider batteryCellProvider){
    if(batteryCellProvider.isLoading){
      return const LoadingComponent(loadingMsg: "Fetching history data...");
    }else{
      return Padding(
        padding: const EdgeInsets.fromLTRB(Dimensions.paddingOverLarge, Dimensions.paddingOverLarge, Dimensions.paddingOverLarge, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: Dimensions.paddingDefault, bottom: 10),
              child: Text(
                lChooseHistoryType,
                style: gilroyRegularTextStyle.copyWith(
                  fontWeight: FontWeight.w600,
                  fontSize: Dimensions.fontSizeDefault,
                  letterSpacing: 1.5,
                ),
                textAlign: TextAlign.start,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: Dimensions.paddingLarge),
              child: Center(
                child: Theme(
                  data: ThemeData(
                    textTheme: TextTheme(
                        subtitle1: gilroyRegularTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault)
                    ),
                  ),
                  child: CustomButton(
                      buttonColor: kPrimaryColor,
                      onTap: (){
                        my_date_picker.DatePicker.showDatePicker(context,
                          showTitleActions: true,
                          minTime: DateTime(2021, 5, 1),
                          maxTime: DateTime.now(),
                          theme: my_date_picker.DatePickerTheme(
                            headerColor: kPrimaryColor,
                            backgroundColor: kPrimaryColor,
                            cancelStyle: gilroyRegularTextStyle,
                            itemStyle: TextStyle(
                                color: kColorWhite,
                                fontWeight: FontWeight.bold,
                                fontSize: 13.sp),
                            doneStyle: gilroyRegularTextStyle.copyWith(color: kSecondaryColor),
                          ),
                          onChanged: (date) {},
                          onConfirm: (date) {
                            batteryCellProvider.setSelectedHistoryDay = DateFormat('MMM d, yyyy').format(date).toString();
                            print(date);
                            getSocHistoryData(date);
                          },
                          currentTime: DateTime.now(),);
                      },
                      name: batteryCellProvider.selectedHistoryDay,
                      fontColor: kColorWhite,
                      height: 50,
                      width: 70.w
                  ),
                ),
              ),
            ),
            (batteryCellProvider.getSocHistoryResponse?.payload == null)
                ? Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 15.h,
                  ),
                  Icon(Icons.airplay_outlined, color: kColorWhite, size: 75.sp),
                  Padding(
                    padding: const EdgeInsets.only(top: 40),
                    child: Text(
                      "No History Data",
                      style: gilroyRegularTextStyle.copyWith(
                          fontWeight: FontWeight.w600,
                          fontSize: Dimensions.fontSizeDefault,
                          letterSpacing: 1.5
                      ),
                    ),
                  ),
                ],
              ),
            )
                : Padding(
              padding: const EdgeInsets.only(top: Dimensions.paddingOverLarge),
              child: Consumer<BatteryCellProvider>(
                  builder: (context, batteryCellProvider, child) {
                    return SfCartesianChart(
                      // Initialize category axis
                        primaryXAxis: CategoryAxis(),
                        series: <LineSeries<UsageData, String>>[
                          LineSeries<UsageData, String>(
                              color: kSecondaryColor,
                              // Bind data source
                              dataSource:  batteryCellProvider.getSocHistoryResponse!.payload!.usageData!,
                              xValueMapper: (UsageData usageData, _) => DateFormat('hh:mm a').format(DateTime.parse("${usageData.socTime}")),
                              yValueMapper: (UsageData usageData, _) => usageData.socValue
                          )
                        ]
                    );
                  }
              ),
            )
          ],
        ),
      );
    }
  }

}