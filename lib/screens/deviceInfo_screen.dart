import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/models/getLocationByUser.dart';
import 'package:home/providers/deviceProvider.dart';
import 'package:home/providers/locationProvider.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/reusableWidgets/bottomNavBar.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customSnackBar.dart';
import 'package:home/reusableWidgets/loadingComponent.dart';
import 'package:home/reusableWidgets/sessionExpired.dart';
import 'package:home/screens/registerDevice_screen.dart';
import 'package:home/screens/routerConnection_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';
import '../models/deletedDevice.dart';
import '../reusableWidgets/shimmerEffect.dart';

class DeviceInfoScreen extends StatelessWidget {
  DeviceInfoScreen({Key? key}) : super(key: key);

  GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          key: scaffoldMessengerKey,
          backgroundColor: Colors.transparent,
          appBar: CustomAppBar(
            title: lDevices,
            isBackButtonExist: true,
            isAction: true,
            backOnTap: () {
              Navigator.pop(context);
            },
            iconOnTap: () {
              confirmationAlert(
                  context: context,
                  title: "Register Device",
                  msg: "Do you want to register a new device!",
                  onCancel: () {
                    Navigator.pop(context);
                  },
                  onYes: () async {
                    Navigator.pop(context);
                    await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const RegisterDeviceScreen()),
                    );
                  },
                  yesButtonName: "Yes",
                  noButtonName: "No",
                  height: 25.h);
            },
            iconData: Icons.add,
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: Dimensions.paddingOverLarge),
            child: Column(
              children: [
                const SizedBox(
                  height: 15,
                ),
                //Location Dropdown
                Consumer<LocationProvider>(builder: (context, locationProvider, child) {
                  return locationProvider.isLoading
                      ? ShimmerEffect().shimmer(context, 50, 100.w, kAccentColor.withOpacity(0.5), kAccentColor, 5)
                      : Expanded(
                          flex: 1,
                          child: Theme(
                            data: ThemeData(
                              textTheme: TextTheme(
                                  subtitle1: gilroyRegularTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault)),
                            ),
                            child: Consumer<DeviceProvider>(builder: (context, deviceProvider, child) {
                              return DropdownSearch<LocationList>(
                                selectedItem: locationProvider.getSelectedLocation,
                                items: locationProvider.getLocationList,
                                dropdownButtonProps: const DropdownButtonProps(color: kAccentColor),
                                dropdownDecoratorProps: const DropDownDecoratorProps(
                                  dropdownSearchDecoration: InputDecoration(
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: kAccentColor, width: 3),
                                    ),
                                    iconColor: kSecondaryColor,
                                  ),
                                ),
                                popupProps: const PopupProps.menu(
                                  menuProps: MenuProps(backgroundColor: kPrimaryColor),
                                  showSearchBox: false,
                                  fit: FlexFit.tight,
                                  constraints: BoxConstraints(maxHeight: 200),
                                ),
                                onChanged: (location) async {
                                  locationProvider.setSelectedLocation = location ?? LocationList();

                                  deviceProvider.setDeviceList = [];
                                  deviceProvider.setDeletedDeviceResponse = DeletedDevice();

                                  await deviceProvider.getDevicesByUserLocation(locationId: location!.locationId!);
                                  await deviceProvider.getDeletedDevices(location.locationId!);

                                  if (locationProvider.getLocationByUserResponse?.isSuccess ?? false) {
                                  } else {
                                    confirmationAlert(
                                        context: context,
                                        title: deviceProvider.errorMessage == noInternet ? "No Connection" : "Oops",
                                        msg: deviceProvider.errorMessage,
                                        onCancel: () {
                                          Navigator.pop(context);
                                        },
                                        onYes: () async {
                                          Navigator.pop(context);
                                          await deviceProvider.getDevicesByUserLocation(
                                              locationId: location.locationId!);
                                        },
                                        yesButtonName: "Retry",
                                        noButtonName: "No",
                                        height: deviceProvider.errorMessage.length > 100 ? 30.h : 25.h);
                                  }
                                },
                                // selectedItem: "Brazil",
                              );
                            }),
                          ),
                        );
                }),
                Expanded(
                  flex: 2,
                  child: TabBar(
                    labelColor: kColorBlack,
                    labelStyle: gilroyBoldTextStyle.copyWith(
                        fontSize: Dimensions.fontSizeDefault, fontWeight: FontWeight.w400),
                    unselectedLabelColor: kColorBlack.withOpacity(0.5),
                    // indicator: const BoxDecoration(),
                    indicatorColor: kSecondaryColor,
                    tabs: const [Text("ACTIVATED"), Text("DEACTIVATED")],
                  ),
                ),
                Expanded(
                  flex: 11,
                  child: TabBarView(
                    children: <Widget>[
                      //Active tab
                      activeDevicesTab(context),

                      //Inactive Tab
                      inActiveDevicesTab(context)
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget activeDevicesTab(BuildContext context) {
    return FutureBuilder(
      future: getAllSavedLocationsData(context),
      builder: (ctx, snapshot) {
        if(snapshot.connectionState == ConnectionState.waiting){
          return const LoadingComponent(loadingMsg: "Getting Devices...");
        }else{
          return Consumer<LocationProvider>(
              builder: (context, locationProvider, child) {
            return Column(
              children: [
                SizedBox(
                  height: 2.h,
                ),
                Expanded(
                  flex: 15,
                  child: Consumer<DeviceProvider>(
                      builder: (context, deviceProvider, child) {
                        if(deviceProvider.isLoading || locationProvider.isLoading){
                          return const LoadingComponent(loadingMsg: "Loading");
                        }else{
                          if((deviceProvider.getDeviceList?.length ?? 0) == 0){
                            return Center(child: Text(
                                "No active devices",
                                style: gilroyRegularTextStyle.copyWith(
                                    fontWeight: FontWeight.w600,
                                    fontSize: Dimensions.fontSizeDefault,
                                    letterSpacing: 1.5
                                ),
                            ),
                            );
                          }
                          return ListView.builder(
                            itemCount: deviceProvider.getDeviceList?.length ?? 0,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: Dimensions.paddingDefault, vertical: Dimensions.paddingDefault),
                                child: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: const BorderRadius.all(Radius.circular(15)),
                                      color: kPrimaryColor,
                                      boxShadow: [
                                        BoxShadow(
                                          color: const Color(0xff019267).withAlpha(70),
                                          blurRadius: 7.0,
                                          spreadRadius: 6.0,
                                          offset: const Offset(0.0, 4.0),
                                        ),
                                      ]),
                                  child: Padding(
                                    padding: const EdgeInsets.all(15),
                                    child: Column(
                                      children: [
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Row(
                                              children: [
                                                const Icon(Icons.ad_units_rounded, color: kColorWhite, size: 30),
                                                Padding(
                                                  padding: const EdgeInsets.only(left: 10),
                                                  child: Text(
                                                    deviceProvider
                                                        .getDeviceList?[index].deviceTypeInfo?.deviceTypeName ??
                                                        "N/A",
                                                    style: gilroyRegularTextStyle.copyWith(
                                                        fontWeight: FontWeight.w600,
                                                        fontSize: Dimensions.fontSizeDefault),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            GestureDetector(
                                              child: Container(
                                                  height: 20.sp,
                                                  width: 20.sp,
                                                  decoration: ShapeDecoration(
                                                    shape: const CircleBorder(),
                                                    color: Colors.red.shade900,
                                                  ),
                                                  child: Center(
                                                      child: Icon(Icons.clear, color: Colors.white, size: 15.sp))),
                                              onTap: () {
                                                if (locationProvider.getSelectedLocation!.locationId == getLocationId()) {
                                                  confirmationAlert(
                                                      context: context,
                                                      title: "Remove Device",
                                                      msg: "Are you sure you want to remove the device!",
                                                      onCancel: () {
                                                        Navigator.pop(context);
                                                      },
                                                      onYes: () async {
                                                        Navigator.pop(context);
                                                        updateDeviceDeleteStatus(
                                                            context, "Deactivated",
                                                            deviceProvider.getDeviceList![index].deviceId!,
                                                            true,
                                                            deviceProvider
                                                        );
                                                      },
                                                      yesButtonName: "Yes",
                                                      noButtonName: "No",
                                                      height: 25.h
                                                  );
                                                } else {
                                                  ScaffoldMessenger.of(context).showSnackBar(customSnackBar(
                                                      context,
                                                      "Please select \"Set as default\" and then remove your device"));
                                                }

                                              },
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          children: [
                                            const Icon(Icons.admin_panel_settings_outlined,
                                                color: kColorWhite, size: 30),
                                            Padding(
                                              padding: const EdgeInsets.only(left: 10),
                                              child: Text(
                                                deviceProvider.getDeviceList?[index].serialNumber ?? "N/A",
                                                style: gilroyRegularTextStyle.copyWith(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: Dimensions.fontSizeDefault),
                                              ),
                                            ),
                                          ],
                                        ),
                                        const SizedBox(
                                          height: 20,
                                        ),
                                        Row(
                                          children: [
                                            const Icon(Icons.whatshot_outlined, color: kColorWhite, size: 30),
                                            Padding(
                                              padding: const EdgeInsets.only(left: 10),
                                              child: Text(
                                                (deviceProvider.getDeviceList?[index].isConnected ?? false)
                                                    ? "ON"
                                                    : "OFF",
                                                style: gilroyRegularTextStyle.copyWith(
                                                    fontWeight: FontWeight.w600,
                                                    fontSize: Dimensions.fontSizeDefault),
                                              ),
                                            ),
                                          ],
                                        ),
                                        ((deviceProvider.getDeviceList?[index].isConnected ?? false))
                                            ? const SizedBox.shrink()
                                            : const SizedBox(
                                          height: 20,
                                        ),
                                        ((deviceProvider.getDeviceList?[index].isConnected ?? false))
                                            ? const SizedBox.shrink()
                                            : Center(
                                          child: CustomButton(
                                              buttonColor: kColorWhite,
                                              onTap: () {
                                                if (locationProvider.getSelectedLocation!.locationId ==
                                                    getLocationId()) {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) => const RouterConnectionScreen()),
                                                  );
                                                } else {
                                                  ScaffoldMessenger.of(context).showSnackBar(customSnackBar(
                                                      context,
                                                      "Please select \"Set as default\" and then pair your device"));
                                                }
                                              },
                                              name: "Pair",
                                              fontColor: kPrimaryColor,
                                              height: 45,
                                              width: 30.w),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
                        }
                  }),
                ),
                ((locationProvider.getSelectedLocation?.locationId ?? getLocationId()) == getLocationId())
                    ? const SizedBox.shrink()
                    : Expanded(
                  flex: 2,
                  child: Center(
                    child: Consumer<DeviceProvider>(
                        builder: (context, deviceProvider, child) {
                          return deviceProvider.isLoading
                              ? const SizedBox.shrink()
                              : Material(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(10),
                            child: Consumer<LocationProvider>(builder: (context, locationProvider, child) {
                              return locationProvider.isLoading
                                  ? const CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor),
                              )
                                  : CustomButton(
                                buttonColor: kPrimaryColor,
                                fontColor: kColorWhite,
                                onTap: () {
                                  updateDefaultLocation(locationProvider, context);
                                },
                                name: lSetAsDefault,
                                height: 50,
                                width: 50.w,
                              );
                            }),
                          );
                        }),
                  ),
                ),
                SizedBox(
                  height: 2.h,
                ),
              ],
            );
          });
        }
      } ,
    );
  }

  Widget inActiveDevicesTab(BuildContext context) {
    return FutureBuilder(
      future: getDeletedDevices(context),
      builder: (ctx, snapshot) => snapshot.connectionState == ConnectionState.waiting
          ? const LoadingComponent(loadingMsg: "Getting Devices...")
          : Consumer<DeviceProvider>(builder: (context, deviceProvider, child) {
              if(deviceProvider.isLoading){
                return const LoadingComponent(loadingMsg: "Loading");
              } else if((deviceProvider.deletedDeviceResponse?.errorCode ?? 511) == 511){
                return Center(child: Text(
                  "No deactivated devices",
                  style: gilroyRegularTextStyle.copyWith(
                      fontWeight: FontWeight.w600,
                      fontSize: Dimensions.fontSizeDefault,
                      letterSpacing: 1.5
                  ),
                ));
              }else{
                return Column(
                  children: [
                    SizedBox(
                      height: 4.h,
                    ),
                    Expanded(
                      flex: 15,
                      child: ListView.builder(
                        itemCount: deviceProvider.deletedDeviceResponse?.payload?.deletedDeviceList?.length ?? 0,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: Dimensions.paddingDefault, vertical: Dimensions.paddingDefault),
                            child: Container(
                              decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                                  color: kPrimaryColor,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.red.withAlpha(70),
                                      blurRadius: 7.0,
                                      spreadRadius: 6.0,
                                      offset: const Offset(0.0, 4.0),
                                    ),
                                  ]),
                              child: Padding(
                                padding: const EdgeInsets.all(15),
                                child: Column(
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            const Icon(Icons.ad_units_rounded, color: kColorWhite, size: 30),
                                            Padding(
                                              padding: const EdgeInsets.only(left: 10),
                                              child: Text(
                                                deviceProvider.deletedDeviceResponse?.payload?.deletedDeviceList?[index].deviceTypeInfo?.deviceTypeName ?? "N/A",
                                                style: gilroyRegularTextStyle.copyWith(
                                                    fontWeight: FontWeight.w600, fontSize: Dimensions.fontSizeDefault),
                                              ),
                                            ),
                                          ],
                                        ),
                                        GestureDetector(
                                          child: Container(
                                              height: 20.sp,
                                              width: 20.sp,
                                              decoration: const ShapeDecoration(
                                                shape: CircleBorder(),
                                                color: Colors.green,
                                              ),
                                              child: Center(child: Icon(Icons.add, color: Colors.white, size: 15.sp))),
                                          onTap: () {
                                            confirmationAlert(
                                                context: context,
                                                title: "Activate Device",
                                                msg: "Are you sure you want to activate this device again ?",
                                                onCancel: () {
                                                  Navigator.pop(context);
                                                },
                                                onYes: () async {
                                                  Navigator.pop(context);
                                                  updateDeviceDeleteStatus(
                                                      context, "Activated",
                                                      deviceProvider.deletedDeviceResponse!.payload!.deletedDeviceList![index].deviceId!,
                                                      false,
                                                      deviceProvider
                                                  );
                                                },
                                                yesButtonName: "Yes",
                                                noButtonName: "No",
                                                height: 25.h);
                                          },
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                      children: [
                                        const Icon(Icons.admin_panel_settings_outlined, color: kColorWhite, size: 30),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 10),
                                          child: Text(
                                            deviceProvider.deletedDeviceResponse?.payload?.deletedDeviceList?[index].serialNumber ?? "N/A",
                                            style: gilroyRegularTextStyle.copyWith(
                                                fontWeight: FontWeight.w600, fontSize: Dimensions.fontSizeDefault),
                                          ),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                      children: [
                                        const Icon(Icons.whatshot_outlined, color: kColorWhite, size: 30),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 10),
                                          child: Text(
                                            "OFF",
                                            style: gilroyRegularTextStyle.copyWith(
                                                fontWeight: FontWeight.w600, fontSize: Dimensions.fontSizeDefault),
                                          ),
                                        ),
                                      ],
                                    ),
                                     const SizedBox(
                                      height: 20,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                );
              }
            }),
    );
  }

  Future<bool> getAllSavedLocationsData(BuildContext context) async {
    WidgetsBinding.instance.addPostFrameCallback((_)async {
      await getAllSavedLocations(context);
    });
    return true;
  }

  updateDefaultLocation(LocationProvider locationProvider, BuildContext context) async {
    final deviceProvider = Provider.of<DeviceProvider>(context, listen: false);

    await locationProvider.setDefaultLocation(locationId: locationProvider.getSelectedLocation!.locationId!);

    if (locationProvider.getDefaultLocationResponse?.isSuccess ?? false) {
      //clear all provider data of previously selected device details.
      final mqttProvider = Provider.of<MqttProvider>(context, listen: false);
      mqttProvider.clearMqttProviderData();

      //save location details, battery id and serial numbers in shared prefs
      saveLocationId(locationProvider.getSelectedLocation!.locationId!);
      saveLocationName(locationProvider.getSelectedLocation!.locationName!);

      if (deviceProvider.getDeviceList != null && deviceProvider.getDeviceList!.isNotEmpty) {
        for (int i = 0; i < deviceProvider.getDeviceList!.length; i++) {
          if (deviceProvider.getDeviceList![i].deviceTypeInfo!.deviceTypeId == 1) {
            //powerCell
            saveBatteryId(deviceProvider.getDevicesByUserLocationResponse!.payload!.deviceList![i].componentId!);
            savePowerCellSerialNumber(deviceProvider.getDevicesByUserLocationResponse!.payload?.deviceList![i].serialNumber ?? "");
            saveBatteryIsDeactivated(false);

            //check device active or not
            if (deviceProvider.getDeviceList![i].isConnected!) {
              saveIsPowerCellIPaired(true);
              mqttProvider.main(context);
            } else {
              saveIsPowerCellIPaired(false);
            }
          } else if (deviceProvider.getDeviceList![i].deviceTypeInfo!.deviceTypeId == 2) {
            //Inverter
          } else {
            //Solar
          }
        }
      } else {
        print('worng!');
      }

      Future.delayed(const Duration(seconds: 1), () {
        if (getLocationId() != -1 && getIsPowerCellIPaired()) {
          //start MQTT
          final mqttProvider = Provider.of<MqttProvider>(context, listen: false);
          mqttProvider.main(context);
        }
      });

      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => BottomNavBar()),
      );
    } else if (locationProvider.errorMessage == "Invalid access token") {
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    } else {
      confirmationAlert(
          context: context,
          title: locationProvider.errorMessage == noInternet ? "No Connection" : "Oops",
          msg: locationProvider.errorMessage,
          onCancel: () {
            Navigator.pop(context);
          },
          onYes: () async {
            Navigator.pop(context);
            updateDefaultLocation(locationProvider, context);
          },
          yesButtonName: "Retry",
          noButtonName: "No",
          height: locationProvider.errorMessage.length > 100 ? 30.h : 25.h);
    }
  }

  Future<bool> getAllSavedLocations(BuildContext context) async {
    final locationProvider = Provider.of<LocationProvider>(context, listen: false);
    locationProvider.setIsLoading = true;
    await locationProvider.getLocationByUser();
    locationProvider.setIsLoading = false;
    if (locationProvider.getLocationByUserResponse?.isSuccess ?? false) {
      await getDevicesByUserLocation(locationProvider, context);
    } else if (locationProvider.errorMessage == "Invalid access token") {
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    } else {
      confirmationAlert(
          context: context,
          title: locationProvider.errorMessage == noInternet ? "No Connection" : "Oops",
          msg: locationProvider.errorMessage,
          onCancel: () {
            Navigator.pop(context);
          },
          onYes: () async {
            Navigator.pop(context);
            getAllSavedLocations(context);
          },
          yesButtonName: "Retry",
          noButtonName: "No",
          height: locationProvider.errorMessage.length > 100 ? 30.h : 25.h);
    }
    return true;
  }

  Future<bool> getDevicesByUserLocation(LocationProvider locationProvider, BuildContext context) async {
    final deviceProvider = Provider.of<DeviceProvider>(context, listen: false);
    deviceProvider.setDeviceList = [];
    await deviceProvider.getDevicesByUserLocation(locationId: getLocationId());

    if (locationProvider.getLocationByUserResponse?.isSuccess ?? false) {
    } else if (locationProvider.errorMessage == "Invalid access token") {
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    } else {
      mSGAlert(
        context: context,
        title: "Oops!",
        msg: deviceProvider.errorMessage,
        onCancel: () {
          Navigator.pop(context);
        },
        height: deviceProvider.errorMessage.length > 100 ? 30.h : 25.h,
      );
    }
    return true;
  }

  Future<bool> updateDeviceDeleteStatus(BuildContext context, String statusName, int deviceId,
      bool isDelete, DeviceProvider deviceProvider) async {
    deviceProvider.setIsLoading = true;
    await deviceProvider.updateDeviceDeleteStatus(deviceId, isDelete);
    if (deviceProvider.commonSuccessResponse?.isSuccess ?? false) {
      if(statusName == "Deactivated"){
        saveBatteryIsDeactivated(true);
        deviceProvider.setDeviceList = [];
        await deviceProvider.getDeletedDevices(getLocationId());
      }else{
        saveBatteryIsDeactivated(false);
        deviceProvider.setDeletedDeviceResponse = DeletedDevice();
        await deviceProvider.getDevicesByUserLocation(locationId: getLocationId());
      }
      deviceProvider.setIsLoading = false;

      mSGAlert(
        context: scaffoldMessengerKey.currentContext!,
        title: "Successful",
        msg: "The device has been $statusName successfully",
        onCancel: () {
          Navigator.pop(scaffoldMessengerKey.currentContext!);
        },
        height: 25.h,
      );
    } else if (deviceProvider.errorMessage == "Invalid access token") {
      deviceProvider.setIsLoading = false;
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    } else {
      deviceProvider.setIsLoading = false;
      confirmationAlert(
          context: context,
          title: deviceProvider.errorMessage == noInternet ? "No Connection" : "Oops",
          msg: deviceProvider.errorMessage,
          onCancel: () {
            Navigator.pop(scaffoldMessengerKey.currentContext!);
          },
          onYes: () async {
            Navigator.pop(scaffoldMessengerKey.currentContext!);
            getAllSavedLocations(context);
          },
          yesButtonName: "Retry",
          noButtonName: "No",
          height: deviceProvider.errorMessage.length > 100 ? 30.h : 25.h);
    }
    return true;
  }

  Future<bool> getDeletedDevices(BuildContext context) async {
    final deviceProvider = Provider.of<DeviceProvider>(context, listen: false);
    await deviceProvider.getDeletedDevices(getLocationId());
    if (deviceProvider.deletedDeviceResponse?.isSuccess ?? false) {
    } else if (deviceProvider.errorMessage == "Invalid access token") {
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    } else {
      if((deviceProvider.deletedDeviceResponse?.errorCode ?? 511) != 511){
        confirmationAlert(
            context: context,
            title: deviceProvider.errorMessage == noInternet ? "No Connection" : "Oops",
            msg: deviceProvider.errorMessage,
            onCancel: () {
              Navigator.pop(context);
            },
            onYes: () async {
              Navigator.pop(context);
              getAllSavedLocations(context);
            },
            yesButtonName: "Retry",
            noButtonName: "No",
            height: deviceProvider.errorMessage.length > 100 ? 30.h : 25.h);
      }
    }
    return true;
  }
}
