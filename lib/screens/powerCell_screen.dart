import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/loadingComponent.dart';
import 'package:home/screens/history_screen.dart';
import 'package:home/screens/powerCustomize_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';

class PowerCellScreen extends StatelessWidget {
  const PowerCellScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: SafeArea(
          child: Container(
              child: Scaffold(
                  backgroundColor: Colors.transparent,
                  appBar: CustomAppBar(
                    title: lAppName,
                    isBackButtonExist: true,
                    isAction: false,
                    isHome: true,
                    backOnTap: (){
                      Navigator.pop(context);
                    },
                    iconOnTap: (){},
                  ),
                body:  Consumer<MqttProvider>(
                    builder: (context, mqttProvider, child) {
                    return Padding(
                      padding: const EdgeInsets.fromLTRB(Dimensions.paddingOverLarge, Dimensions.paddingOverLarge, Dimensions.paddingOverLarge, 0),
                      child: RefreshIndicator(
                        onRefresh: () async {
                          if(getLocationId() != -1 && getIsPowerCellIPaired()) {
                            mqttProvider.main(context);
                          }
                          await Future.delayed(const Duration(seconds: 2));
                        },
                        backgroundColor: kColorWhite,
                        color: kPrimaryColor,
                        displacement: 10,
                        strokeWidth: 3,
                        child: ListView(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                  children: [
                                    Consumer<MqttProvider>(
                                        builder: (context, mqttProvider, child) {
                                          return powerCellComponent(
                                              topic: "STATUS",
                                              value: checkPowerFlowStatus(mqttProvider.powerFlowStatus)
                                          );
                                        }
                                    ),
                                    SizedBox(width: 5.w),

                                    //must consider the power value font size and color
                                    Consumer<MqttProvider>(
                                        builder: (context, mqttProvider, child) {
                                          return powerCellComponent(
                                              topic: "POWER",
                                              //current*voltage/1000
                                              value: "${((double.parse(mqttProvider.current)*double.parse(mqttProvider.voltage))/1000).floor()} kwh"
                                          );
                                        }
                                    ),
                                  ],
                                ),

                                // Padding(
                                //   padding: EdgeInsets.only(top: 4.h, bottom: 4.h),
                                //   child: Center(
                                //     child: Container(
                                //         height: (SizerUtil.deviceType == DeviceType.mobile) ? 175.sp : 150.sp,
                                //         width: (SizerUtil.deviceType == DeviceType.mobile) ? 175.sp : 150.sp,
                                //         decoration: ShapeDecoration(
                                //           shape: const CircleBorder(),
                                //           color: kPrimaryColor,
                                //           shadows: [
                                //             BoxShadow(
                                //               color: kAccentColor.withAlpha(70),
                                //               offset: const Offset(1.0, 1.0),
                                //               blurRadius: 15.0,
                                //             ),
                                //           ],),
                                //         child: Stack(
                                //           children: [
                                //             Center(
                                //               child: Column(
                                //                 mainAxisAlignment: MainAxisAlignment.center,
                                //                 crossAxisAlignment: CrossAxisAlignment.center,
                                //                 children: [
                                //                   Consumer<MqttProvider>(
                                //                       builder: (context, mqttProvider, child) {
                                //                         return Text(
                                //                           "${mqttProvider.voltage} %",
                                //                           style: gilroyRegularTextStyle.copyWith(
                                //                               fontWeight: FontWeight.w600,
                                //                               fontSize: Dimensions.fontSizeOverLarge,
                                //                               letterSpacing: 1.5
                                //                           ),
                                //                         );
                                //                       }
                                //                   ),
                                //                   Padding(
                                //                     padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 1.h),
                                //                     child: Container(
                                //                       color: kColorWhite,
                                //                       height: 2.5,
                                //                     ),
                                //                   ),
                                //                   Padding(
                                //                     padding: EdgeInsets.symmetric(horizontal: 8.w, vertical: 0),
                                //                     child: Consumer<MqttProvider>(
                                //                         builder: (context, mqttProvider, child) {
                                //                           return Text(
                                //                             //((( SOC/100)*(SOH/100)*Total Energy)/Power) hrs
                                //                             "${((( double.parse(mqttProvider.voltage)/100)*(double.parse(mqttProvider.voltage)/100)*double.parse(mqttProvider.energy))/((double.parse(mqttProvider.current)*double.parse(mqttProvider.voltage))/1000)).floorToDouble().isNaN
                                //                                 ? "N/A" : ((( double.parse(mqttProvider.voltage)/100)*(double.parse(mqttProvider.voltage)/100)*double.parse(mqttProvider.energy))/((double.parse(mqttProvider.current)*double.parse(mqttProvider.voltage))/1000)).floorToDouble()} hrs to ${checkPowerFlowStatus(mqttProvider.powerFlowStatus)}",
                                //                             style: gilroyRegularTextStyle.copyWith(
                                //                                 fontWeight: FontWeight.w500,
                                //                                 fontSize: Dimensions.fontSizeDefault,
                                //                                 letterSpacing: 1.5
                                //                             ),
                                //                             textAlign: TextAlign.center,
                                //                           );
                                //                         }
                                //                     ),
                                //                   ),
                                //                 ],
                                //               ),
                                //             ),
                                //             Consumer<MqttProvider>(
                                //                 builder: (context, mqttProvider, child) {
                                //                   return Center(
                                //                     child: CircularPercentIndicator(
                                //                       radius: (SizerUtil.deviceType == DeviceType.mobile) ? 87.sp : 75.sp,
                                //                       lineWidth: 12.sp,
                                //                       percent: double.parse((mqttProvider.voltage))/100.0,
                                //                       animation: true,
                                //                       animationDuration: 1200,
                                //                       circularStrokeCap: CircularStrokeCap.round,
                                //                       progressColor: kSecondaryColor,
                                //                       backgroundColor: kAccentColor.withOpacity(0.3),
                                //                     ),
                                //                   );
                                //                 }
                                //             ),
                                //           ],
                                //         )
                                //     ),
                                //   ),
                                // ),

                                // Container(
                                //   height: 6.h,
                                //   width: 60.w,
                                //   decoration: BoxDecoration(
                                //       color: kPrimaryColor,
                                //       borderRadius: BorderRadius.circular(10),
                                //       boxShadow: [
                                //         BoxShadow(
                                //           color: kAccentColor.withAlpha(70),
                                //           blurRadius: 7.0,
                                //           spreadRadius: 3.0,
                                //           offset: const Offset(0.0, 4.0),
                                //         ),
                                //       ]),
                                //   child: Padding(
                                //     padding: const EdgeInsets.all(Dimensions.paddingSmall),
                                //     child: Row(
                                //       mainAxisAlignment: MainAxisAlignment.spaceAround,
                                //       crossAxisAlignment: CrossAxisAlignment.center,
                                //       children: [
                                //         Text(
                                //           "Temperature : ",
                                //           style: gilroyRegularTextStyle.copyWith(
                                //               fontWeight: FontWeight.w600,
                                //               fontSize: Dimensions.fontSizeDefault,
                                //               letterSpacing: 1.5
                                //           ),
                                //         ),
                                //         const SizedBox(
                                //           height: 10,
                                //         ),
                                //
                                //         //must consider the power value font size and color
                                //         Consumer<MqttProvider>(
                                //             builder: (context, mqttProvider, child) {
                                //             return Text(
                                //               "${mqttProvider.temperature} \u{00B0}c",
                                //               style: gilroyRegularTextStyle.copyWith(
                                //                   fontWeight: FontWeight.w600,
                                //                   color: kSecondaryColor,
                                //                   fontSize: 13.sp,
                                //                   letterSpacing: 1.5
                                //               ),
                                //             );
                                //           }
                                //         ),
                                //       ],
                                //     ),
                                //   ),
                                // ),

                                Padding(
                                  padding: EdgeInsets.only(top: 30.h),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      Expanded(
                                        child: CustomButton(
                                            buttonColor: kPrimaryColor,
                                            onTap: (){
                                              Navigator.push(context, MaterialPageRoute(builder: (context) => PowerCustomizeScreen()));
                                            },
                                            name: "Customize",
                                            fontColor: kColorBlack,
                                            height: 50,
                                            width: 10
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.w,
                                      ),
                                      Expanded(
                                        child: CustomButton(
                                            buttonColor: kPrimaryColor,
                                            onTap: (){
                                              Navigator.push(context, MaterialPageRoute(builder: (context) => const HistoryScreen()));
                                            },
                                            name: "History",
                                            fontColor: kColorBlack,
                                            height: 50,
                                            width: 10
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  }
                ),
              ),
          ),
      ),
    );
  }

  Widget powerCellComponent({required String topic, required String value}){
    return  Expanded(
      child: Container(
        height: 100,
        decoration: BoxDecoration(
            color: kPrimaryColor,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                color: kAccentColor.withAlpha(70),
                blurRadius: 7.0,
                spreadRadius: 3.0,
                offset: const Offset(0.0, 4.0),
              ),
            ]),
        child: Padding(
          padding: const EdgeInsets.all(Dimensions.paddingDefault),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                topic,
                style: gilroyRegularTextStyle.copyWith(
                    fontWeight: FontWeight.w600,
                    fontSize: Dimensions.fontSizeDefault,
                    letterSpacing: 1.5
                ),
              ),
              const SizedBox(
                height: 10,
              ),

              //must consider the power value font size and color
              Text(
                value,
                style: gilroyRegularTextStyle.copyWith(
                    fontWeight: FontWeight.w600,
                    color: kSecondaryColor,
                    fontSize: 12.sp,
                    letterSpacing: 1.5
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String checkPowerFlowStatus(String status){
    if(status == "+"){
      return "CHARGING";
    }else if(status == "-"){
      return "DISCHARGING";
    }else{
      return "N/A";
    }
  }

}
