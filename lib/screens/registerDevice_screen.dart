import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/models/getLocationByUser.dart';
import 'package:home/models/getRegisteredLocation.dart';
import 'package:home/providers/deviceProvider.dart';
import 'package:home/providers/locationProvider.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/reusableWidgets/bottomNavBar.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customSnackBar.dart';
import 'package:home/reusableWidgets/customTextField.dart';
import 'package:home/reusableWidgets/sessionExpired.dart';
import 'package:home/reusableWidgets/shimmerEffect.dart';
import 'package:home/screens/qrView_screen.dart';
import 'package:home/screens/routerConnection_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';

class RegisterDeviceScreen extends StatefulWidget {
  const RegisterDeviceScreen({Key? key}) : super(key: key);

  @override
  State<RegisterDeviceScreen> createState() => _RegisterDeviceScreenState();
}

class _RegisterDeviceScreenState extends State<RegisterDeviceScreen> {
  final locationController = TextEditingController();
  final serialNumberController = TextEditingController();
  final pinController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    getAllSavedLocations();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (getLocationId() != -1) {
          return true;
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => BottomNavBar()),
          );
          return false;
        }
      },
      child: SafeArea(
          child: Container(
        child: Scaffold(
          key: scaffoldMessengerKey,
          backgroundColor: Colors.transparent,
          appBar: CustomAppBar(
            title: lRegisterDevice,
            isBackButtonExist: true,
            isAction: false,
            backOnTap: () {
              if (getLocationId() != -1) {
                Navigator.pop(context);
              } else {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => BottomNavBar()),
                );
              }
            },
            iconOnTap: () {},
          ),
          body: SingleChildScrollView(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: Dimensions.paddingOverLarge),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 40, bottom: 10),
                  child: Consumer<LocationProvider>(builder: (context, locationProvider, child) {
                    return Text(
                      locationProvider.getLocationList.isEmpty ? lAddLocation : lChooseLocation,
                      style: gilroyRegularTextStyle.copyWith(
                        fontWeight: FontWeight.w600,
                        fontSize: Dimensions.fontSizeDefault,
                        letterSpacing: 1.5,
                      ),
                      textAlign: TextAlign.start,
                    );
                  }),
                ),

                //Location Dropdown
                Consumer<LocationProvider>(builder: (context, locationProvider, child) {
                  return locationProvider.isLoading
                      ? ShimmerEffect().shimmer(context, 50, 100.w, kAccentColor.withOpacity(0.5), kAccentColor, 5)
                      : (locationProvider.getTempLocationList.isEmpty
                          ? CustomTextField(
                              controller: locationController,
                              isHint: true,
                              hint: "Ex - Home Colombo 07",
                              textFieldName: "",
                              errorText: locationProvider.isLocationFilled ? null : "Location cannot be empty",
                              onChangedText: (dsf) {
                                WidgetsBinding.instance.addPostFrameCallback((_) {
                                  locationProvider.setIsLocationFilled = true;
                                });
                              },
                              obscureText: false,
                              onSubmit: () {},
                            )
                          : Row(
                              children: [
                                Expanded(
                                  flex: 10,
                                  child: Theme(
                                    data: ThemeData(
                                      textTheme: TextTheme(
                                          subtitle1:
                                              gilroyRegularTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault)),
                                    ),
                                    child: Consumer<LocationProvider>(builder: (context, locationProvider, child) {
                                      return DropdownSearch<DeviceLocationList>(
                                        selectedItem: locationProvider.getTempSelectedLocation,
                                        items: locationProvider.getTempLocationList,
                                        dropdownButtonProps: const DropdownButtonProps(color: kAccentColor),
                                        dropdownDecoratorProps: const DropDownDecoratorProps(
                                          dropdownSearchDecoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(color: kAccentColor, width: 3),
                                            ),
                                            iconColor: kSecondaryColor,
                                          ),
                                        ),
                                        popupProps: const PopupProps.menu(
                                          menuProps: MenuProps(backgroundColor: kPrimaryColor),
                                          showSearchBox: false,
                                          fit: FlexFit.tight,
                                          constraints: BoxConstraints(maxHeight: 200),
                                        ),
                                        onChanged: (location) {
                                          locationProvider.setTempSelectedLocation = location ?? DeviceLocationList();
                                        },
                                        // selectedItem: "Brazil",
                                      );
                                    }),
                                  ),
                                ),
                                SizedBox(
                                  width: 5.w,
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    width: 30.sp,
                                    height: 30.sp,
                                    decoration: const ShapeDecoration(
                                      shape: CircleBorder(),
                                      color: kSecondaryColor,
                                    ),
                                    child: GestureDetector(
                                      onTap: () {
                                        // locationController.clear();
                                        addLocation(locationProvider);
                                      },
                                      child: const Icon(Icons.add, color: kColorBlack),
                                    ),
                                  ),
                                )
                              ],
                            ));
                }),

                //add serial number or scan

                //serial Number
                Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: Row(
                    children: [
                      Expanded(
                        child: CustomTextField(
                          controller: serialNumberController,
                          isHint: true,
                          hint: "g564885525",
                          textFieldName: lEnterSerial,
                          onChangedText: (dsf) {},
                          obscureText: false,
                          onSubmit: () {},
                        ),
                      ),
                    ],
                  ),
                ),

                //OR
                Padding(
                  padding: const EdgeInsets.only(
                    top: Dimensions.paddingOverLarge,
                    bottom: Dimensions.paddingOverLarge,
                  ),
                  child: Center(
                    child: Text(
                      lOr,
                      style: gilroyRegularTextStyle.copyWith(
                        fontWeight: FontWeight.w600,
                        fontSize: Dimensions.fontSizeDefault,
                        letterSpacing: 1.5,
                      ),
                    ),
                  ),
                ),

                //Scan QR
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      lScanQR,
                      style: gilroyRegularTextStyle.copyWith(
                        fontSize: Dimensions.fontSizeSmall,
                        letterSpacing: 1.5,
                      ),
                    ),
                    const SizedBox(width: 20),
                    GestureDetector(
                      onTap: () {
                        //add with await then get the result
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const QrViewScreen()));
                      },
                      child: const Icon(
                        Icons.add_a_photo_outlined,
                        size: 25,
                        color: kColorBlack,
                      ),
                    )
                  ],
                ),

                const SizedBox(height: 40),

                //Enter PIN
                Text(
                  lEnterPin,
                  style: gilroyRegularTextStyle.copyWith(
                      fontWeight: FontWeight.w600, fontSize: Dimensions.fontSizeDefault, letterSpacing: 1.5),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Form(
                    key: formKey,
                    child: PinCodeTextField(
                      appContext: context,
                      autoFocus: false,
                      enablePinAutofill: true,
                      autoDisposeControllers: false,
                      pastedTextStyle: gilroyBoldTextStyle.copyWith(
                        fontSize: Dimensions.fontSizeDefault,
                      ),
                      // length: _otpCodeLength,
                      animationType: AnimationType.fade,
                      textStyle: gilroyBoldTextStyle.copyWith(
                        fontSize: Dimensions.fontSizeLarge,
                      ),
                      hintCharacter: '0',
                      hintStyle: gilroyBoldTextStyle.copyWith(
                        color: kAccentColor,
                        fontSize: Dimensions.fontSizeDefault,
                      ),
                      // cursorColor: kDefaultTextColor,
                      animationDuration: const Duration(milliseconds: 200),
                      enableActiveFill: true,
                      // errorAnimationController: errorController,
                      controller: pinController,
                      keyboardType: const TextInputType.numberWithOptions(signed: true, decimal: true),
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      boxShadows: const [
                        BoxShadow(
                          color: Colors.black12,
                          blurRadius: 1,
                        )
                      ],
                      length: 6,
                      obscureText: false,
                      obscuringCharacter: '*',
                      // obscuringWidget: FlutterLogo(
                      //   size: 24,
                      // ),
                      blinkWhenObscuring: true,
                      validator: (v) {
                        // if (v!.length < 3) {
                        //   return SnackBarReusable().showSnackBar(
                        //     context, "Please enter the code correctly", _scaffoldKey
                        //   );
                        // } else {
                        //   return null;
                        // }
                      },
                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.box,
                        // animationDuration: Duration(milliseconds: 300),
                        borderRadius: const BorderRadius.all(Radius.circular(10)),
                        fieldHeight: 50,
                        fieldWidth: 50,
                        activeFillColor: kPrimaryColor.withOpacity(0.5),

                        // borderWidth: 1,
                        activeColor: kSecondaryColor,
                        selectedColor: kSecondaryColor,
                        inactiveColor: kAccentColor,
                        selectedFillColor: kPrimaryColor,
                        inactiveFillColor: kPrimaryColor,
                      ),
                      onCompleted: (value) {
                        // _onOtpCallBack(value, false);
                      },
                      onChanged: (value) {
                        // _onOtpCallBack(value, false);
                      },
                      beforeTextPaste: (text) {
                        print("here is my Allowing to paste");
                        print("Allowing to paste $text");
                        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                        //but you can show anything you want here, like your pop up saying wrong paste format or etc
                        return true;
                      },
                    ),
                  ),
                ),

                //Register device Button
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 50),
                  child: Center(
                    child: Consumer<DeviceProvider>(builder: (context, deviceProvider, child) {
                      return deviceProvider.isLoading
                          ? const CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor))
                          : Material(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(10),
                              child: CustomButton(
                                buttonColor: kPrimaryColor,
                                fontColor: kColorBlack,
                                onTap: () async {
                                  final locationProvider = Provider.of<LocationProvider>(context, listen: false);

                                  if (locationProvider.getTempLocationList.isEmpty && locationController.text.isEmpty) {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(customSnackBar(context, "Please enter a location!"));
                                  } else if (serialNumberController.text.isEmpty) {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(customSnackBar(context, "Serial number cannot be empty!"));
                                  } else if (pinController.text.isEmpty) {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(customSnackBar(context, "PIN cannot be empty!"));
                                  } else if (pinController.text.length > 7) {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(customSnackBar(context, "Please enter a valid PIN number!"));
                                  } else {
                                    registerDevice(deviceProvider, locationProvider);
                                  }
                                },
                                name: lRegister,
                                height: 50,
                                width: 35.w,
                              ),
                            );
                    }),
                  ),
                ),
              ],
            ),
          )),
        ),
      )),
    );
  }

  getAllSavedLocations() async {
    final locationProvider = Provider.of<LocationProvider>(context, listen: false);
    locationProvider.clearTempSelectedLocationList();
    locationProvider.setIsLoading = true;
    await locationProvider.getLocationByUser();
    locationProvider.setIsLoading = false;
    if (locationProvider.getLocationByUserResponse?.isSuccess ?? false) {
    } else if (locationProvider.errorMessage == "Invalid access token") {
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    } else {
      if(locationProvider.errorMessage != "Location not found"){
        confirmationAlert(
            context: context,
            title: locationProvider.errorMessage == noInternet ? "No Connection" : "Oops",
            msg: locationProvider.errorMessage,
            onCancel: () {
              Navigator.pop(context);
            },
            onYes: () async {
              Navigator.pop(context);
              getAllSavedLocations();
            },
            yesButtonName: "Retry",
            noButtonName: "No",
            height: locationProvider.errorMessage.length > 100 ? 30.h : 25.h
        );
      }
    }
  }

  registerDevice(DeviceProvider deviceProvider, LocationProvider locationProvider) async {

    deviceProvider.setIsLoading = true;
    await deviceProvider.assignDeviceToUser(
        serialNumber: serialNumberController.text.trim(),
        locationId: locationProvider.getTempSelectedLocation?.locationId ?? 0,
        locationName: locationController.text.trim(),
        pin: pinController.text
    );
    deviceProvider.setIsLoading = false;
    if (deviceProvider.getAssignDeviceToUserResponse?.isSuccess ?? false) {
      saveBatteryIsDeactivated(false);
      mSGAlert(
        context: context,
        title: "Successful",
        msg: "The device has been registered successfully.",
        onCancel: () {
          //clear all provider data of previously selected device details.
          final mqttProvider = Provider.of<MqttProvider>(context, listen: false);
          mqttProvider.clearMqttProviderData();

          //save default location as selected location
          locationProvider.setSelectedLocation = LocationList(
              locationId: deviceProvider.getAssignDeviceToUserResponse!.payload!.locationInfo!.locationId,
              locationName: deviceProvider.getAssignDeviceToUserResponse!.payload!.locationInfo!.locationName);

          //save battery id and serial numbers in shared prefs
          saveIsPowerCellIPaired(false);
          savePowerCellSerialNumber(deviceProvider.getAssignDeviceToUserResponse!.payload?.serialNumber ?? "");
          saveBatteryId(deviceProvider.getAssignDeviceToUserResponse!.payload?.componentId ?? -1);
          saveLocationId(deviceProvider.getAssignDeviceToUserResponse!.payload!.locationInfo!.locationId!);
          saveLocationName(deviceProvider.getAssignDeviceToUserResponse!.payload!.locationInfo!.locationName!);

          //pop alert and go to next page
          Navigator.pop(context);

          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const RouterConnectionScreen()),
          );
        },
        height: deviceProvider.errorMessage.length > 100 ? 30.h : 25.h,
      );
    } else if (deviceProvider.errorMessage == "Invalid access token") {
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    } else {
      if(deviceProvider.errorMessage == "The device already assigned to another user"
     || deviceProvider.errorMessage == "Device not found"){
        mSGAlert(
          context: context,
          title: "Oops",
          msg: deviceProvider.errorMessage,
          onCancel: () {
            Navigator.pop(context);
          },
          height: deviceProvider.errorMessage.length > 100 ? 30.h : 25.h,
        );
      }else{
        confirmationAlert(
            context: context,
            title: deviceProvider.errorMessage == noInternet ? "No Connection" : "Oops",
            msg: deviceProvider.errorMessage,
            onCancel: () {
              Navigator.pop(context);
            },
            onYes: () async {
              Navigator.pop(context);
              registerDevice(deviceProvider, locationProvider);
            },
            yesButtonName: "Retry",
            noButtonName: "No",
            height: deviceProvider.errorMessage.length > 100 ? 30.h : 25.h
        );
      }
    }
  }

  addLocation(LocationProvider locationProvider) {
    return textFieldAlert(
        context: context,
        title: "Add Location",
        widget: Consumer<LocationProvider>(builder: (context, locationProvider, child) {
          return CustomTextField(
            controller: locationController,
            isHint: true,
            hint: "Ex - Home Colombo 07",
            textFieldName: "",
            errorText: locationProvider.isLocationFilled ? null : "Location cannot be empty",
            onChangedText: (dsf) {
              // locationProvider.setIsLocationFilled = true;
            },
            obscureText: false,
            onSubmit: () {},
          );
        }),
        onTap: () async {
          if (locationController.text.isEmpty) {
            locationProvider.setIsLocationFilled = false;
          } else {
            locationProvider.setIsLocationFilled = true;
            Navigator.pop(context);

            //clear and add location names
            locationProvider.clearLocationNameList();
            for (int i = 0; i < locationProvider.getTempLocationList.length; i++) {
              locationProvider.addLocationNameList(locationProvider.getTempLocationList[i].locationName!.toLowerCase());
            }
            if (locationProvider.locationNameList.contains(locationController.text.toLowerCase().trim())) {
              ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Location is already entered!"));
            } else {
              locationProvider.addLocation(DeviceLocationList(locationId: 0, locationName: locationController.text));
              locationProvider.setTempSelectedLocation =
                  DeviceLocationList(locationId: 0, locationName: locationController.text);
            }
          }
        },
        buttonName: "Add",
        height: 30.h);
  }
}
