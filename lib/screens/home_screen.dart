import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/providers/commonProvider.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/reusableWidgets/bottomNavBar.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/screens/login_screen.dart';
import 'package:home/screens/registerDevice_screen.dart';
import 'package:home/screens/routerConnection_screen.dart';
import 'package:home/screens/settings_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/textStyles.dart';

import '../reusableWidgets/customSnackBar.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  List<String> commandList = ["Start", "Stop", "Lock Charger", "Unlock Charger", "Load Balancing Enable", "Load Balancing Enable", "Factory Reset", "Reboot"];

  String _getCommand({required int index}){
    if(index == 0){
      return "start";
    }else if(index == 1){
      return "stop";
    }else if(index == 2){
      return "lockCharger";
    }else if(index == 3){
      return "unlockCharger";
    }else if(index == 4){
      return "loadBalancingEnable";
    }else if(index == 5){
      return "loadBalancingDisable";
    }else if(index == 6){
      return "factoryReset";
    }else{
      return "Reboot";
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
          backgroundColor: Colors.white,
          bottomNavigationBar: Container(height: 10.h),
          appBar: CustomAppBar(
            isAction: true,
            backOnTap: () {
              Navigator.pop(context);
            },
            iconOnTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsScreen()));
            },
            title: getLocationName() ?? lAppName,
            iconData: Icons.settings,
            isBackButtonExist: false,
            isHome: true,
          ),
          body: SingleChildScrollView(
            child: Consumer<MqttProvider>(builder: (context, mqttProvider, child) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // mqttProvider.isMqttLoading
                  //     ? SizedBox(height: 60.h, child: const Center(child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor))))
                  //     :
                  Padding(
                        padding: EdgeInsets.symmetric(horizontal: 10.w),
                        child: SizedBox(
                            height: 50.h,
                            child: Column(
                              children: [
                                _itemCard(name: "Command", value: mqttProvider.powerFlowStatus),
                                _itemCard(name: "State", value: mqttProvider.state),
                                _itemCard(name: "Current", value: mqttProvider.current),
                                _itemCard(name: "Power", value: mqttProvider.power),
                                _itemCard(name: "Voltage", value: mqttProvider.voltage),
                                _itemCard(name: "Charge Duration", value: mqttProvider.chargeDuration),
                                _itemCard(name: "Energy", value: mqttProvider.energy),
                                _itemCard(name: "Charger Lock State", value: mqttProvider.chargeLockState),
                                _itemCard(name: "Fault State", value: mqttProvider.faultState),
                                _itemCard(name: "Load Balancing State", value: mqttProvider.loadBalancingState),
                                _itemCard(name: "Schedule Charge Enable State", value: mqttProvider.scheduleChargeEnableState),
                                _itemCard(name: "Running App Version", value: mqttProvider.runningAppVersion),
                              ],
                            ),
                          ),
                      ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 5.w, top: 15),
                      child: Text("Command List", style: gilroyBoldTextStyle.copyWith(fontSize: 13.sp)),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 15, bottom: 15),
                    child: SizedBox(
                        height: 8.h,
                        width: 100.w,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: commandList.length,
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          itemBuilder: (context, index) {
                            return  Padding(
                              padding: const EdgeInsets.only(left: 8.0),
                              child: CustomButton(
                                  buttonColor: kColorBlack,
                                  onTap: () async {
                                    ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "\"${commandList[index]}\" has been applied.."));
                                    mqttProvider.publishMyMsgFromAppToDevice(_getCommand(index: index), false);
                                  },
                                  name: commandList[index],
                                  fontColor: kColorWhite,
                                  height: 25,
                                  width: 150
                              ),
                            );
                          },
                      )
                    ),
                  ),
                  SizedBox(height: 1.h),
                  CustomButton(
                      buttonColor: kColorBlack,
                      onTap: () async {
                        final mqttProvider = Provider.of<MqttProvider>(context, listen: false);
                        mqttProvider.mqttLoading = true;
                        await mqttProvider.main(context);
                      },
                      name: "REFRESH",
                      fontColor: kColorWhite,
                      height: 35,
                      width: 150
                  )
                ],
              );
            }),
          )),
    );
  }

  Widget _itemCard({required String name, required String value}) {
    return Row(
      children: [
        Text(name, style: gilroyBoldTextStyle),
        SizedBox(width: 10.w),
        Text(value, style: gilroyRegularTextStyle),
        SizedBox(height: 4.0.h),
      ],
    );
  }

  popDeviceRegisterAlert() {
    return showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => WillPopScope(
        onWillPop: () => Future.value(false),
        child: Dialog(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          backgroundColor: kPrimaryColor,
          child: Container(
            height: 35.h,
            margin: const EdgeInsets.all(15),
            padding: const EdgeInsets.symmetric(horizontal: 15),
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(15)),
              color: kPrimaryColor,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Text(
                    "Register Device",
                    style: gilroyBoldTextStyle.copyWith(
                      fontSize: Dimensions.fontSizeDefault,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Text(
                  "Please confirm to Register the device!",
                  style: gilroyRegularTextStyle.copyWith(
                    fontSize: Dimensions.fontSizeSmall,
                  ),
                  textAlign: TextAlign.center,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    "OR",
                    style: gilroyBoldTextStyle,
                    textAlign: TextAlign.center,
                  ),
                ),
                GestureDetector(
                  onTap: () async {
                    Navigator.pop(context);
                    clearSharedPrefs();
                    await Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginScreen()),
                    );
                  },
                  child: Text(
                    "Login with another user",
                    style: gilroyRegularTextStyle.copyWith(
                      fontSize: Dimensions.fontSizeSmall,
                      decoration: TextDecoration.underline,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                Container(
                  margin: const EdgeInsets.only(bottom: 10, top: 12),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: CustomButton(
                          buttonColor: kPrimaryColor,
                          fontColor: kColorWhite,
                          onTap: () {
                            exit(0);
                          },
                          name: "Quit",
                          height: 40,
                          width: 35.w,
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Expanded(
                        child: CustomButton(
                          buttonColor: kColorWhite,
                          fontColor: kPrimaryColor,
                          onTap: () async {
                            Navigator.pop(context);
                            await Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => const RegisterDeviceScreen()),
                            );
                          },
                          name: "Register",
                          height: 40,
                          width: 35.w,
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
    // return confirmationAlert(
    //     context: context,
    //     title: "Register Device",
    //     msg: "Please confirm to Register the device!",
    //     onCancel: (){
    //       exit(0);
    //     },
    //     onYes: ()async{
    //       Navigator.pop(context);
    //       await Navigator.push(
    //         context,
    //         MaterialPageRoute(builder: (context) => const RegisterDeviceScreen()),
    //       );
    //     },
    //     yesButtonName: "Pair",
    //     noButtonName: "Quit",
    //     height: 25.h
    // );
  }

  popDevicePairAlert() {
    return confirmationAlert(
        context: context,
        title: "Pair Device",
        msg: "Please confirm to pair the device!",
        onCancel: () {
          Navigator.pop(context);
        },
        onYes: () async {
          Navigator.pop(context);
          await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const RouterConnectionScreen()),
          );
        },
        yesButtonName: "Pair",
        noButtonName: "Cancel",
        height: 25.h);
  }

  Widget imageComponent({required String imgUrl, required String name}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SvgPicture.asset(
          imgUrl,
          fit: BoxFit.contain,
          height: 70.sp,
          width: 70.sp,
        ),
        Text(
          name,
          style: gilroyBoldTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault),
        ),
      ],
    );
  }

  Widget horizontalLineComponent() {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 1.0),
          child: Container(
            height: 3,
            color: kSecondaryColor,
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            CircleAvatar(
              radius: 3,
              backgroundColor: kColorWhite,
            ),
            CircleAvatar(
              radius: 3,
              backgroundColor: kColorWhite,
            )
          ],
        )
      ],
    );
  }

  Widget verticalLineComponent() {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 1.0),
          child: Container(
            height: 50,
            width: 3,
            color: kSecondaryColor,
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: const [
            CircleAvatar(
              radius: 3,
              backgroundColor: kColorWhite,
            ),
            CircleAvatar(
              radius: 3,
              backgroundColor: kColorWhite,
            )
          ],
        )
      ],
    );
  }
}
