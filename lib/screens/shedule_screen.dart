import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import '../data/base/sharedPrefs.dart';
import '../models/scheduleUpdateCommand.dart';
import '../providers/mqttProvider.dart';
import '../reusableWidgets/customButton.dart';
import '../reusableWidgets/customSnackBar.dart';
import '../utils/colors.dart';
import '../utils/dimensions.dart';
import '../utils/textStyles.dart';

class ScheduleScreen extends StatelessWidget {
  ScheduleScreen({Key? key}) : super(key: key);

  Widget longCard({required String value, required String topic, required Function() onPress}) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        height: 8.h,
        width: 100.w,
        decoration: BoxDecoration(color: kPrimaryColor, borderRadius: BorderRadius.circular(15), boxShadow: [
          BoxShadow(
            color: kSecondaryColor.withAlpha(70),
            blurRadius: 7.0,
            spreadRadius: 3.0,
            offset: const Offset(0.0, 4.0),
          ),
        ]),
        child: Padding(
          padding: const EdgeInsets.all(Dimensions.paddingDefault),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                topic,
                style: gilroyRegularTextStyle.copyWith(
                    fontWeight: FontWeight.w600,
                    color: kColorBlack,
                    fontSize: Dimensions.fontSizeLarge,
                    letterSpacing: 1.5),
              ),
              Text(
                value,
                style: gilroyRegularTextStyle.copyWith(
                    fontWeight: FontWeight.w600,
                    color: kSecondaryColor,
                    fontSize: Dimensions.fontSizeDefault,
                    letterSpacing: 1.5),
              ),
            ],
          ),
        ),
      ),
    );
  }

  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
          key: scaffoldMessengerKey,
          backgroundColor: Colors.white,
          appBar: CustomAppBar(
            isAction: false,
            backOnTap: () {
              Navigator.pop(context);
            },
            iconOnTap: () {},
            title: "Schedule Charger",
            iconData: Icons.settings,
            isBackButtonExist: false,
            isHome: false,
          ),
          body: Column(
            children: [
              Consumer<MqttProvider>(builder: (context, mqttProvider, child) {
                return Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.w),
                  child: Row(
                    children: [
                      const Text("Schedule charger Enable State", style: gilroyBoldTextStyle),
                      SizedBox(width: 10.w),
                      Text(mqttProvider.scheduleChargeEnableState, style: gilroyRegularTextStyle),
                      SizedBox(height: 5.h),
                    ],
                  ),
                );
              }),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8.w),
                child: Consumer<MqttProvider>(builder: (context, mqttProvider, child) {
                  return mqttProvider.scheduleChargeEnableState == "1"
                      ? CustomButton(
                          buttonColor: kColorBlack,
                          onTap: () async {
                            ScaffoldMessenger.of(context).showSnackBar(
                                customSnackBar(context, "\"Disable Schedule Charger\" has been applied.."));
                            mqttProvider.publishMyMsgFromAppToDevice("scheduleChargeDisable", false);
                          },
                          name: "Disable Schedule Charger",
                          fontColor: kColorWhite,
                          height: 6.h,
                          width: 170)
                      : CustomButton(
                          buttonColor: kColorBlack,
                          onTap: () async {
                            ScaffoldMessenger.of(context).showSnackBar(
                                customSnackBar(context, "\"Enable Schedule Charger\" has been applied.."));
                            mqttProvider.publishMyMsgFromAppToDevice("scheduleChargeEnable", false);
                          },
                          name: "Enable Schedule Charger",
                          fontColor: kColorWhite,
                          height: 6.h,
                          width: 150);
                }),
              ),
              Consumer<MqttProvider>(builder: (context, mqttProvider, child) {
                return Stack(
                  children: [
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.w),
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 3.h),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text("Weekdays"),
                                const SizedBox(
                                  height: 15,
                                ),
                                Consumer<MqttProvider>(builder: (context, mqttProvider, child) {
                                  return longCard(
                                      topic: "Start",
                                      value: DateFormat("hh:mm a")
                                          .format(DateTime.parse(mqttProvider.scheduleStartTimeWeekDays.toString()))
                                          .toString(),
                                      onPress: () {
                                        DatePicker.showTimePicker(context,
                                            showTitleActions: true,
                                            showSecondsColumn: false,
                                            onChanged: (time) {}, onConfirm: (time) {
                                          mqttProvider.scheduleStartTimeWeekDays = time;
                                          mqttProvider.scheduleStopTimeWeekDays =
                                              DateTime.parse(time.toString()).add(const Duration(minutes: 5));
                                        }, currentTime: DateTime.now(), locale: LocaleType.en);
                                      });
                                }),
                                Padding(
                                  padding: const EdgeInsets.only(top: Dimensions.paddingDefault),
                                  child: Consumer<MqttProvider>(builder: (context, mqttProvider, child) {
                                    return longCard(
                                        topic: "Stop",
                                        value: DateFormat("hh:mm a")
                                            .format(DateTime.parse(mqttProvider.scheduleStopTimeWeekDays.toString()))
                                            .toString(),
                                        onPress: () {
                                          DatePicker.showTimePicker(context,
                                              showTitleActions: true, showSecondsColumn: false, onChanged: (time) {
                                            print('change $time');
                                          }, onConfirm: (time) {
                                            if (mqttProvider.scheduleStartTimeWeekDays.isBefore(time)) {
                                              mqttProvider.scheduleStopTimeWeekDays = time;
                                            } else {
                                              mqttProvider.scheduleStopTimeWeekDays = time.add(const Duration(days: 1));
                                            }
                                          }, currentTime: DateTime.now(), locale: LocaleType.en);
                                        });
                                  }),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 3.h),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text("WeekEnds"),
                                const SizedBox(
                                  height: 15,
                                ),
                                Consumer<MqttProvider>(builder: (context, mqttProvider, child) {
                                  return longCard(
                                      topic: "Start",
                                      value: DateFormat("hh:mm a")
                                          .format(DateTime.parse(mqttProvider.scheduleStartTimeWeekEnds.toString()))
                                          .toString(),
                                      onPress: () {
                                        DatePicker.showTimePicker(context,
                                            showTitleActions: true, showSecondsColumn: false, onChanged: (time) {
                                          print('change $time');
                                        }, onConfirm: (time) {
                                          mqttProvider.scheduleStartTimeWeekEnds = time;
                                          mqttProvider.scheduleStopTimeWeekEnds =
                                              DateTime.parse(time.toString()).add(const Duration(minutes: 5));
                                        }, currentTime: DateTime.now(), locale: LocaleType.en);
                                      });
                                }),
                                Padding(
                                  padding: const EdgeInsets.only(top: Dimensions.paddingDefault),
                                  child: Consumer<MqttProvider>(builder: (context, mqttProvider, child) {
                                    return longCard(
                                        topic: "Stop",
                                        value: DateFormat("hh:mm a")
                                            .format(DateTime.parse(mqttProvider.scheduleStopTimeWeekEnds.toString()))
                                            .toString(),
                                        onPress: () {
                                          DatePicker.showTimePicker(context,
                                              showTitleActions: true, showSecondsColumn: false, onChanged: (time) {
                                            print('change $time');
                                          }, onConfirm: (time) {
                                            if (mqttProvider.scheduleStartTimeWeekEnds.isBefore(time)) {
                                              mqttProvider.scheduleStopTimeWeekEnds = time;
                                            } else {
                                              mqttProvider.scheduleStopTimeWeekEnds = time.add(const Duration(days: 1));
                                            }
                                          }, currentTime: DateTime.now(), locale: LocaleType.en);
                                        });
                                  }),
                                ),
                              ],
                            ),
                          ),
                          Consumer<MqttProvider>(builder: (context, mqttProvider, child) {
                            return mqttProvider.scheduleChargeEnableState == "0"
                                ? const SizedBox()
                                : (mqttProvider.isScheduling
                                    ? const Center(
                                        child: CircularProgressIndicator(
                                            valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor)))
                                    : CustomButton(
                                        buttonColor: kColorBlack,
                                        onTap: () async {
                                          mqttProvider.setIsScheduling = true;

                                          mqttProvider.publishMyMsgFromAppToDevice(jsonEncode(ScheduleUpdateCommand(
                                              command: "scheduleUpdate",
                                              scheduleStartTimeWeekDays:
                                                  DateFormat("HH:mm").format(mqttProvider.scheduleStartTimeWeekDays),
                                              scheduleStopTimeWeekEnds:
                                                  DateFormat("HH:mm").format(mqttProvider.scheduleStopTimeWeekEnds),
                                              scheduleStartTimeWeekEnds:
                                                  DateFormat("HH:mm").format(mqttProvider.scheduleStartTimeWeekEnds),
                                              scheduleStopTimeWeekDays:
                                                  DateFormat("HH:mm").format(mqttProvider.scheduleStopTimeWeekDays))), true);

                                          await mqttProvider.publishMyMsgFromAppToDevice("updateAlarm", false);

                                          Future.delayed(const Duration(seconds: 3), () async {
                                            String formattedDate =
                                                DateFormat('HH:mm').format(mqttProvider.scheduleStartTimeWeekDays);
                                            print(
                                                "FORMATED DATE>> $formattedDate ${mqttProvider.mqttResponse?.scheduleStartTimeWeekDays}");

                                            if (formattedDate == mqttProvider.mqttResponse?.scheduleStartTimeWeekDays) {
                                              await mqttProvider.publishMyMsgFromAppToDevice("alarmUpdated", false);

                                              mqttProvider.setIsScheduling = false;

                                              saveScheduleStartTimeWeekDays(mqttProvider.scheduleStartTimeWeekDays);
                                              saveScheduleStopTimeWeekDays(mqttProvider.scheduleStopTimeWeekDays);
                                              saveScheduleStartTimeWeekEnds(mqttProvider.scheduleStartTimeWeekEnds);
                                              saveScheduleStopTimeWeekEnds(mqttProvider.scheduleStopTimeWeekEnds);

                                              ScaffoldMessenger.of(context).showSnackBar(
                                                  customSnackBar(context, "\"Update Schedule\" has been applied.."));
                                            } else {
                                              mqttProvider.setIsScheduling = false;

                                              try{
                                                mqttProvider.scheduleStartTimeWeekDays = getScheduleStartTimeWeekDays();
                                                mqttProvider.scheduleStopTimeWeekDays = getScheduleStopTimeWeekDays();
                                                mqttProvider.scheduleStartTimeWeekEnds = getScheduleStartTimeWeekEnds();
                                                mqttProvider.scheduleStopTimeWeekEnds = getScheduleStopTimeWeekEnds();
                                              }catch(e){
                                              }

                                              // ignore: use_build_context_synchronously
                                              showDialog(
                                                context: context,
                                                barrierDismissible: false,
                                                builder: (BuildContext context) => Dialog(
                                                  shape:
                                                      RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
                                                  // backgroundColor: kPrimaryColor,
                                                  child: Container(
                                                    margin: const EdgeInsets.all(15),
                                                    padding: const EdgeInsets.symmetric(horizontal: 15),
                                                    decoration: const BoxDecoration(
                                                      borderRadius: BorderRadius.all(Radius.circular(15)),
                                                      // color: kPrimaryColor,
                                                    ),
                                                    child: Column(
                                                      mainAxisSize: MainAxisSize.min,
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        const Padding(
                                                            padding: EdgeInsets.only(top: 10, bottom: 10),
                                                            child: // Delete Articles
                                                                Text(
                                                              "Warning",
                                                              textAlign: TextAlign.center,
                                                            )),
                                                        const Text(
                                                          "Couldn't update the schedule times",
                                                          textAlign: TextAlign.center,
                                                        ),
                                                        Container(
                                                          margin: const EdgeInsets.only(bottom: 10, top: 12),
                                                          child: Row(
                                                            mainAxisAlignment: MainAxisAlignment.center,
                                                            crossAxisAlignment: CrossAxisAlignment.center,
                                                            children: [
                                                              Expanded(
                                                                child: CustomButton(
                                                                    buttonColor: kColorBlack,
                                                                    onTap: () {
                                                                      Navigator.pop(context);
                                                                    },
                                                                    name: 'OK',
                                                                    fontColor: kColorWhite,
                                                                    height: 25,
                                                                    width: 75),
                                                              ),
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }
                                          });
                                        },
                                        name: "Update Schedule",
                                        fontColor: kColorWhite,
                                        height: 6.h,
                                        width: 150));
                          }),
                        ],
                      ),
                    ),
                    (mqttProvider.scheduleChargeEnableState == "0")
                        ? Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(height: 65.h, color: Colors.grey.withOpacity(0.6)),
                          )
                        : const SizedBox()
                  ],
                );
              }),
            ],
          )),
    );
  }
}
