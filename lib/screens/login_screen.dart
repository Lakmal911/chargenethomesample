import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/providers/commonProvider.dart';
import 'package:home/providers/deviceProvider.dart';
import 'package:home/providers/locationProvider.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/providers/userProvider.dart';
import 'package:home/reusableWidgets/bottomNavBar.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customSnackBar.dart';
import 'package:home/reusableWidgets/customTextField.dart';
import 'package:home/screens/forgotPassword_screen.dart';
import 'package:home/screens/settings_screen.dart';
import 'package:home/screens/signUp_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';

class LoginScreen extends StatelessWidget {
  LoginScreen({Key? key}) : super(key: key);

  final userNameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {

    return SafeArea(
        child: WillPopScope(
          onWillPop: () => Future.value(false),
          child: Scaffold(
            body: Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40.0),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: 8.h,
                      ),

                      SizedBox(
                        height: 5.h,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          //UserName TextField
                          CustomTextField(
                            controller: userNameController,
                            isHint: true,
                            hint: "John321",
                            textFieldName: lUserName,
                            onChangedText: (dsf){
                            },
                            obscureText: false,
                            onSubmit: (){

                            },
                          ),
                          SizedBox(
                            height: 6.h,
                          ),

                          //Password TextField
                          Consumer<CommonProvider>(
                              builder: (context, commonProvider, child) {
                              return CustomTextField(
                                controller: passwordController,
                                isHint: true,
                                hint: "x x x x x",
                                textFieldName: lPassword,
                                onChangedText: (dsf){
                                },
                                obscureText: commonProvider.getIsObscureTextPassword,
                                onSubmit: (){

                                },
                                suffix: GestureDetector(
                                  onTap: (){
                                    if(commonProvider.getIsObscureTextPassword){
                                      commonProvider.isObscureTextPassword = false;
                                    }else{
                                      commonProvider.isObscureTextPassword = true;
                                    }
                                  },
                                  child: Icon(
                                      commonProvider.getIsObscureTextPassword
                                          ? Icons.visibility_outlined
                                          : Icons.visibility_off_outlined,
                                      color: kColorWhite
                                  ),
                                ),
                              );
                            }
                          ),

                          SizedBox(
                            height: 4.h,
                          ),

                          GestureDetector(
                            onTap: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => ForgotPasswordScreen()),
                              );
                            },
                            child: Text(
                              lForgotPassword,
                              style: gilroyBoldTextStyle.copyWith(
                                  color: kSecondaryColor,
                                  fontSize: Dimensions.fontSizeSmall
                              ),
                              textAlign: TextAlign.end,
                            ),
                          ),

                          SizedBox(
                            height: 6.h,
                          ),

                          //Login Button
                          Center(
                            child: Material(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(10),
                              child: Consumer<UserProvider>(
                                  builder: (context, userProvider, child) {
                                  return userProvider.isLoading
                                      ? const CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor))
                                      : CustomButton(
                                    buttonColor: kPrimaryColor,
                                    fontColor: kColorWhite,
                                    onTap: () async{
                                      if(userNameController.text.isEmpty){
                                        ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "User Name cannot be empty!"));
                                      }else if(RegExp(r"\s").hasMatch(userNameController.text)){
                                        ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "User Name must be entered without whitespace!"));
                                      }else if(passwordController.text.isEmpty){
                                        ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Password cannot be empty!"));
                                      }else{
                                        loginWithCredentials(userProvider: userProvider, context: context);
                                      }
                                    },
                                    name: lLogin,
                                    height: 50,
                                    width: 35.w,
                                  );
                                }
                              ),
                            ),
                          ),
                        ],
                      ),

                      //New to Vega, Register Note
                      Padding(
                        padding: EdgeInsets.only(top: 20.h),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text.rich(
                              TextSpan(
                                  children: [
                                    TextSpan(
                                      text: lNewToVegaPower,
                                      style: gilroyBoldTextStyle.copyWith(
                                          color: kColorWhite,
                                          fontSize: Dimensions.fontSizeExtraSmall
                                      ),
                                    ),
                                    TextSpan(
                                      text: lSignUp,
                                      recognizer: TapGestureRecognizer()..onTap = () {

                                        //set obscure text false
                                        if(!Provider.of<CommonProvider>(context, listen: false).getIsObscureTextPassword){
                                          Provider.of<CommonProvider>(context, listen: false).isObscureTextPassword = true;
                                        }


                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(builder: (context) => SignUpScreen()),
                                        );
                                      },
                                      style: gilroyBoldTextStyle.copyWith(
                                          color: kSecondaryColor,
                                          fontSize: Dimensions.fontSizeExtraSmall
                                      ),
                                    ),
                                  ]
                              )
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        )
    );
  }

  loginWithCredentials({required UserProvider userProvider, required BuildContext context})async{
    userProvider.setIsLoading = true;
    await userProvider.getLoginResponse(
      userName: userNameController.text.trim(),
      password: passwordController.text.trim(),
    );
    if(userProvider.loginResponse?.isSuccess?? false){

      //save user details on Shared prefs
      saveUserToken(userProvider.loginResponse!.payload!.token!);
      saveUserId(userProvider.loginResponse!.payload!.userId!);
      saveUserLoginId(userProvider.loginResponse!.payload!.loginId!);

      print("${getUserLoginId()} ${getUserToken()} ${getUserId()}");

      //set userLocations
      final locationProvider = Provider.of<LocationProvider>(context, listen: false);
      if(userProvider.loginResponse!.payload!.locationList != null
          && userProvider.loginResponse!.payload!.locationList!.isNotEmpty){
        locationProvider.setLocationList = userProvider.loginResponse!.payload!.locationList!;
        locationProvider.setSelectedLocation = userProvider.loginResponse!.payload!.defaultLocation!;
        saveLocationId(locationProvider.getSelectedLocation!.locationId!);
        saveLocationName(locationProvider.getSelectedLocation!.locationName!);

        print("${getUserId()} ${getUserLoginId()} ${getUserToken()}");
        final deviceProvider = Provider.of<DeviceProvider>(context, listen: false);
        await deviceProvider.getDevicesByUserLocation(locationId: getLocationId());

        userProvider.setIsLoading = false;

        //save battery id and serial numbers in shared prefs
        if(deviceProvider.getDevicesByUserLocationResponse?.isSuccess ?? false){
          if(deviceProvider.getDeviceList != null && deviceProvider.getDeviceList!.isNotEmpty){
            for(int i=0; i<deviceProvider.getDeviceList!.length; i++){
              if(deviceProvider.getDeviceList![i].deviceTypeInfo!.deviceTypeId == 1){
                //powerCell
                saveBatteryId(deviceProvider.getDevicesByUserLocationResponse!.payload!.deviceList![i].componentId!);
                savePowerCellSerialNumber(deviceProvider.getDevicesByUserLocationResponse!.payload?.deviceList![i].serialNumber?? "");

                //check device active or not
                if(deviceProvider.getDeviceList![i].isConnected!){
                  saveIsPowerCellIPaired(true);

                  //start MQTT
                  final mqttProvider = Provider.of<MqttProvider>(context, listen: false);
                  mqttProvider.main(context);
                }else{
                  saveIsPowerCellIPaired(false);
                }
              }else if(deviceProvider.getDeviceList![i].deviceTypeInfo!.deviceTypeId == 2){
                //Inverter
              }else{
                //Solar
              }
            }}

          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => BottomNavBar()),
          );

        }else if((deviceProvider.getDevicesByUserLocationResponse?.errorCode ?? 508) == 508){
          saveBatteryIsDeactivated(true);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SettingsScreen()),
          );
        }else{
          confirmationAlert(
              context: context,
              title: userProvider.errorMessage == noInternet ? "No Connection" : "Oops",
              msg: userProvider.errorMessage,
              onCancel: () {
                Navigator.pop(context);
              },
              onYes: () async {
                Navigator.pop(context);
              },
              yesButtonName: "Retry",
              noButtonName: "No",
              height: userProvider.errorMessage.length > 100 ? 30.h : 25.h
          );
        }
      }else{
        userProvider.setIsLoading = false;
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => BottomNavBar()),
        );
      }

    }else{
      userProvider.setIsLoading = false;
      if(userProvider.errorMessage == "Authentication credentials are incorrect"
      || userProvider.errorMessage == "User not found"){
        mSGAlert(
          context: context,
          title: "Oops",
          msg: userProvider.errorMessage,
          onCancel: () {
            Navigator.pop(context);
          },
          height: userProvider.errorMessage.length > 100 ? 30.h : 25.h,
        );
      }else{
        confirmationAlert(
            context: context,
            title: userProvider.errorMessage == noInternet ? "No Connection" : "Oops",
            msg: userProvider.errorMessage,
            onCancel: () {
              Navigator.pop(context);
            },
            onYes: () async {
              Navigator.pop(context);
              loginWithCredentials(userProvider: userProvider, context: context);
            },
            yesButtonName: "Retry",
            noButtonName: "No",
            height: userProvider.errorMessage.length > 100 ? 30.h : 25.h
        );
      }
    }
  }
}
