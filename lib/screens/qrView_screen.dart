import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:sizer/sizer.dart';
import 'package:home/providers/commonProvider.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/textStyles.dart';
import '../reusableWidgets/customAppBar.dart';

class QrViewScreen extends StatefulWidget {
  const QrViewScreen({Key? key}) : super(key: key);

  @override
  State<QrViewScreen> createState() => _QrViewScreenState();
}

class _QrViewScreenState extends State<QrViewScreen> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? controller;

  // In order to get hot reload to work we need to pause the camera if the platform
  // is android, or resume the camera if the platform is iOS.
  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: CustomAppBar(
            title: "",
            isBackButtonExist: false,
            isAction: true,
            iconData: Icons.close,
            backOnTap: (){
              Navigator.pop(context);
            },
            iconOnTap: () {
              Navigator.pop(context);
            },
          ),
          body: Consumer<CommonProvider>(
              builder: (context, commonProvider, child) {
              return Column(
                children: <Widget>[
                  Expanded(
                    flex: 6,
                    child: QRView(
                      key: qrKey,
                      onQRViewCreated: _onQRViewCreated,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Center(
                      child: (commonProvider.getBarcodeResult != null)
                          ? Text(
                          'Barcode Type: ${describeEnum(commonProvider.getBarcodeResult!.format)}   Data: ${commonProvider.getBarcodeResult!.code}')
                          : Text(
                          'Scan a code',
                        style: gilroyBoldTextStyle.copyWith(
                          fontSize: Dimensions.fontSizeDefault,
                          color: kColorWhite,
                        ),
                      ),
                    ),
                  )
                ],
              );
            }
          ),
        ),
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      Provider.of<CommonProvider>(context, listen: false).barcodeResult = scanData;
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
