import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/providers/userProvider.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customSnackBar.dart';
import 'package:home/reusableWidgets/customTextField.dart';
import 'package:home/reusableWidgets/loadingComponent.dart';
import 'package:home/reusableWidgets/sessionExpired.dart';
import 'package:home/screens/updatePassword_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';

class ProfileScreen extends StatelessWidget {
  ProfileScreen({Key? key}) : super(key: key);

  final userNameController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final emailController = TextEditingController();
  final phoneController = TextEditingController();
  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        child: Scaffold(
          key: scaffoldMessengerKey,
          backgroundColor: Colors.transparent,
          appBar: CustomAppBar(
            title: lProfile,
            isBackButtonExist: true,
            isAction: true,
            backOnTap: () {
              Navigator.pop(context);
            },
            iconData: Icons.create_outlined,
            iconOnTap: () {
              final userProvider = Provider.of<UserProvider>(context, listen: false);
              if (userProvider.isEditPressed) {
                userProvider.setIsEditPressed = false;
                ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Edit Mode off!"));
              } else {
                userProvider.setIsEditPressed = true;
                ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Edit Mode On!"));
              }
            },
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: Dimensions.paddingOverLarge, vertical: Dimensions.paddingDefault),
            child: SingleChildScrollView(
              child: FutureBuilder(
                  future: getUserDetails(context),
                  builder: (ctx, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Padding(
                        padding: EdgeInsets.only(top: 20.h),
                        child: const LoadingComponent(loadingMsg: "Loading Profile..."),
                      );
                    } else {
                      return Consumer<UserProvider>(
                          builder: (context, userProvider, child) {
                        var user = userProvider.getUserByIdResponse?.payload?.user;

                        if(!userProvider.isEditPressed){
                          userNameController.text = user?.username ?? "";
                          firstNameController.text = user?.firstName ?? "";
                          lastNameController.text = user?.lastName ?? "";
                          emailController.text = user?.email ?? "";
                          phoneController.text = user?.contactNumber ?? "";
                        }

                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 2.h,
                            ),
                            //UserName TextField
                            GestureDetector(
                              onTap: (){
                                ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "You're not allowed to change the User Name!"));
                              },
                              child: AbsorbPointer(
                                child: CustomTextField(
                                  controller: userNameController,
                                  isHint: true,
                                  hint: "",
                                  textFieldName: lUserName,
                                  onChangedText: (dsf) {},
                                  obscureText: false,
                                  onSubmit: () {},
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 4.h,
                            ),

                            //first name and last name
                            Row(
                              children: [
                                Expanded(
                                  child: GestureDetector(
                                    onTap: (){
                                      if(!userProvider.isEditPressed){
                                        ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Please select the edit button on top right corner to edit the first name!"));
                                      }
                                    },
                                    child: AbsorbPointer(
                                      absorbing: userProvider.isEditPressed ? false : true,
                                      child: CustomTextField(
                                        controller: firstNameController,
                                        isHint: true,
                                        hint: "",
                                        textFieldName: lFirstName,
                                        onChangedText: (dsf) {},
                                        obscureText: false,
                                        onSubmit: () {},
                                      ),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 30,
                                ),
                                Expanded(
                                  child:  GestureDetector(
                                    onTap: (){
                                      if(!userProvider.isEditPressed){
                                        ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Please select the edit button on top right corner to edit the last name!"));
                                      }
                                    },
                                    child: AbsorbPointer(
                                      absorbing: userProvider.isEditPressed ? false : true,
                                      child: CustomTextField(
                                        controller: lastNameController,
                                        isHint: true,
                                        hint: "",
                                        textFieldName: lLastName,
                                        onChangedText: (dsf) {},
                                        obscureText: false,
                                        onSubmit: () {},
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 4.h,
                            ),

                            //Email TextField
                            GestureDetector(
                              onTap: (){
                                if(!userProvider.isEditPressed){
                                  ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Please select the edit button on top right corner to edit the email!"));
                                }
                              },
                              child: AbsorbPointer(
                                absorbing: userProvider.isEditPressed ? false : true,
                                child: CustomTextField(
                                  controller: emailController,
                                  isHint: true,
                                  hint: "",
                                  textFieldName: lEmail,
                                  onChangedText: (dsf) {},
                                  obscureText: false,
                                  onSubmit: () {},
                                ),
                              ),
                            ),

                            SizedBox(
                              height: 4.h,
                            ),

                            //phone TextField
                            GestureDetector(
                              onTap: (){
                                if(!userProvider.isEditPressed){
                                  ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Please select the edit button on top right corner to edit the Mobile Number!"));
                                }
                              },
                              child: AbsorbPointer(
                                absorbing: userProvider.isEditPressed ? false : true,
                                child: CustomTextField(
                                  controller: phoneController,
                                  isHint: true,
                                  hint: "",
                                  textFieldName: lPhone,
                                  onChangedText: (dsf) {},
                                  obscureText: false,
                                  onSubmit: () {},
                                ),
                              ),
                            ),

                            SizedBox(
                              height: 4.h,
                            ),

                            Center(
                              child: GestureDetector(
                                onTap: ()async{
                                  await Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) => const UpdatePasswordScreen()),
                                  );
                                },
                                child: Text(
                                  "Change My Password",
                                  style: gilroyBoldTextStyle.copyWith(
                                    decoration: TextDecoration.underline,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),

                            SizedBox(
                              height: 6.h,
                            ),
                            Center(
                              child:  Consumer<UserProvider>(
                                  builder: (context, userProvider, child) {
                                  return Material(
                                    color: Colors.transparent,
                                    borderRadius: BorderRadius.circular(10),
                                    child: userProvider.isLoading
                                        ? const CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor),
                                    )
                                        : (!userProvider.isEditPressed
                                        ? const SizedBox.shrink()
                                        : CustomButton(
                                      buttonColor: kPrimaryColor,
                                      fontColor: kColorWhite,
                                      onTap: () async {
                                        if (firstNameController.text.isEmpty){
                                          ScaffoldMessenger.of(context).showSnackBar(
                                              customSnackBar(context, "First Name cannot be empty!"));
                                        }else if (lastNameController.text.isEmpty){
                                          ScaffoldMessenger.of(context).showSnackBar(
                                              customSnackBar(context, "Last Name cannot be empty!"));
                                        }else if(emailController.text.isEmpty){
                                          ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Email cannot be empty!"));
                                        }else if(validateEmail(emailController.text.trim()) == false){
                                          ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Please check your email again!"));
                                        }else if (phoneController.text.isEmpty){
                                          ScaffoldMessenger.of(context).showSnackBar(
                                              customSnackBar(context, "Mobile Number cannot be empty!"));
                                        }else {
                                          updateProfile(userProvider, context);
                                        }
                                      },
                                      name: lUpdate,
                                      height: 50,
                                      width: 50.w,
                                    )),
                                  );
                                }
                              ),
                            )
                          ],
                        );
                      });
                    }
                  }),
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> getUserDetails(BuildContext context) async {
    final userProvider = Provider.of<UserProvider>(context, listen: false);
    await userProvider.getUserById();
    if (userProvider.getUserByIdResponse?.isSuccess ?? false) {
    }else if(userProvider.errorMessage == "Invalid access token"){
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    }else {
      confirmationAlert(
          context: context,
          title: userProvider.errorMessage == noInternet ? "No Connection" : "Oops",
          msg: userProvider.errorMessage,
          onCancel: () {
            Navigator.pop(context);
          },
          onYes: () async {
            Navigator.pop(context);
            await getUserDetails(context);
          },
          yesButtonName: "Retry",
          noButtonName: "No",
          height: userProvider.errorMessage.length > 100 ? 30.h : 25.h
      );
    }
    return true;
  }

  updateProfile(UserProvider userProvider, BuildContext context) async {
    await userProvider.updateUser(
        firstName: firstNameController.text,
        lastName: lastNameController.text,
        email: emailController.text,
        contactNo: phoneController.text
    );
    if (userProvider.commonSuccessResponse?.isSuccess ?? false) {
      userProvider.setIsEditPressed = false;
      await getUserDetails(context);
      mSGAlert(
        context: context,
        title: "Successful",
        msg: "Profile has been updated successfully!",
        onCancel: () {
          Navigator.pop(context);
        },
        height: 25.h,
      );
    }else if(userProvider.errorMessage == "Invalid access token"){
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    }else {
      confirmationAlert(
          context: context,
          title: userProvider.errorMessage == noInternet ? "No Connection" : "Oops",
          msg: userProvider.errorMessage,
          onCancel: () {
            Navigator.pop(context);
          },
          onYes: () async {
            Navigator.pop(context);
            await getUserDetails(context);
          },
          yesButtonName: "Retry",
          noButtonName: "No",
          height: userProvider.errorMessage.length > 100 ? 30.h : 25.h
      );
    }
    userProvider.setIsLoading = false;
  }

  bool? validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern as String);
    if (!regex.hasMatch(value)) {
      return false;
    } else {
      return null;
    }
  }

}
