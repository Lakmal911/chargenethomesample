import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/providers/deviceProvider.dart';
import 'package:home/providers/locationProvider.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/providers/userProvider.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customSnackBar.dart';
import 'package:home/reusableWidgets/customTextField.dart';
import 'package:home/screens/updatePassword_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';

class ForgotPasswordScreen extends StatelessWidget {
  ForgotPasswordScreen({Key? key}) : super(key: key);

  final userNameController = TextEditingController();
  final emailController = TextEditingController();
  final pinController = TextEditingController();
  late final PageController _pageController = PageController(initialPage: 0);
  GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: scaffoldMessengerKey,
        backgroundColor: Colors.transparent,
        appBar: CustomAppBar(
          title: lResetPassword,
          isBackButtonExist: true,
          isAction: false,
          backOnTap: () {
            Navigator.pop(context);
          },
          iconData: Icons.create_outlined,
          iconOnTap: () {},
        ),
        body: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: _pageController,
          scrollDirection: Axis.horizontal,
          children: [
            //username and email
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: Dimensions.paddingOverLarge, vertical: Dimensions.paddingDefault),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 2.h,
                    ),
                    const Text(
                      "Enter the user name and the email associated with your account and we'll send an email with instructions to reset your password.",
                      style: gilroyBoldTextStyle,
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                    CustomTextField(
                      controller: userNameController,
                      isHint: true,
                      hint: "John321",
                      textFieldName: lUserName,
                      onChangedText: (dsf){
                      },
                      obscureText: false,
                      onSubmit: (){

                      },
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    CustomTextField(
                      controller: emailController,
                      isHint: true,
                      hint: "John@gmail.com",
                      textFieldName: lEmail,
                      onChangedText: (dsf){
                      },
                      obscureText: false,
                      onSubmit: (){

                      },
                    ),

                    SizedBox(
                      height: 6.h,
                    ),

                    Center(
                      child: Consumer<UserProvider>(
                          builder: (context, userProvider, child) {
                          return Material(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(10),
                            child: userProvider.isLoading
                                ? const CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor),
                            ): CustomButton(
                              buttonColor: kPrimaryColor,
                              fontColor: kColorWhite,
                              onTap: () async {
                                if (userNameController.text.isEmpty){
                                  ScaffoldMessenger.of(context).showSnackBar(
                                      customSnackBar(context, "User Name cannot be empty!"));
                                }else if(emailController.text.isEmpty){
                                  ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Email cannot be empty!"));
                                }else if(validateEmail(emailController.text.trim()) == false){
                                  ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Please check your email again!"));
                                }else {
                                  saveUserName(userNameController.text.trim());
                                  forgotPassword(userProvider);
                                }
                              },
                              name: lSendInstructions,
                              height: 50,
                              width: 50.w,
                            ),
                          );
                        }
                      ),
                    )
                    // Center(
                    //   child: Material(
                    //     color: Colors.transparent,
                    //     borderRadius: BorderRadius.circular(10),
                    //     child: userProvider.isLoading
                    //         ? const CircularProgressIndicator(
                    //       valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor),
                    //     )
                    //         : (!userProvider.isEditPressed
                    //         ? const SizedBox.shrink()
                    //         : CustomButton(
                    //       buttonColor: kPrimaryColor,
                    //       fontColor: kColorWhite,
                    //       onTap: () async {
                    //         if (firstNameController.text.isEmpty){
                    //           ScaffoldMessenger.of(context).showSnackBar(
                    //               customSnackBar(context, "First Name cannot be empty!"));
                    //         }else if (lastNameController.text.isEmpty){
                    //           ScaffoldMessenger.of(context).showSnackBar(
                    //               customSnackBar(context, "Last Name cannot be empty!"));
                    //         }else if(emailController.text.isEmpty){
                    //           ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Email cannot be empty!"));
                    //         }else if(validateEmail(emailController.text.trim()) == false){
                    //           ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Please check your email again!"));
                    //         }else if (phoneController.text.isEmpty){
                    //           ScaffoldMessenger.of(context).showSnackBar(
                    //               customSnackBar(context, "Mobile Number cannot be empty!"));
                    //         }else {
                    //           bool networkResults = await NetworkHelper.checkNetwork();
                    //           if (networkResults) {
                    //             updateProfile(userProvider, context);
                    //           } else {
                    //             customNoInternetAlert(
                    //               context: context,
                    //               onYes: () async {
                    //                 bool networkResults = await NetworkHelper.checkNetwork();
                    //                 if (networkResults) {
                    //                   Navigator.pop(context);
                    //                   updateProfile(userProvider, context);
                    //                 }
                    //               },
                    //             );
                    //           }
                    //         }
                    //       },
                    //       name: lUpdate,
                    //       height: 50,
                    //       width: 50.w,
                    //     )),
                    //   ),
                    // )
                  ],
                ),
              ),
            ),

            //enter sent OTP
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: Dimensions.paddingOverLarge, vertical: Dimensions.paddingDefault),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.email, color: kColorWhite, size: 75.sp),
                    Text(
                      "Check your Email",
                      style: gilroyRegularTextStyle.copyWith(
                          fontWeight: FontWeight.w600,
                          fontSize: Dimensions.fontSizeDefault,
                          letterSpacing: 1.5
                      ),
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                    const Text(
                      "We have sent a password recover instructions to your email.",
                      style: gilroyBoldTextStyle,
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    CustomTextField(
                      controller: pinController,
                      isHint: true,
                      hint: "x x x x",
                      textFieldName: lPassword,
                      onChangedText: (dsf){
                      },
                      obscureText: false,
                      onSubmit: (){

                      },
                    ),
                    SizedBox(
                      height: 6.h,
                    ),
                    Center(
                      child: Consumer<UserProvider>(
                          builder: (context, userProvider, child) {
                          return Material(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(10),
                              child:
                              userProvider.isLoading
                                  ? const CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor),
                              )
                                  :
                              CustomButton(
                                buttonColor: kPrimaryColor,
                                fontColor: kColorWhite,
                                onTap: () async {
                                  if (pinController.text.isEmpty){
                                    ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Password cannot be empty!"));
                                  }else {
                                    loginWithNewPassword(userProvider, context);
                                  }
                                },
                                name: lResetPassword,
                                height: 50,
                                width: 50.w,
                              )
                    );
                        }
                      )
                    ),
                    SizedBox(
                      height: 6.h,
                    ),
                    GestureDetector(
                      onTap: (){
                        _pageController.jumpToPage(0);
                      },
                      child: Text(
                        "Didn't receive an email ?",
                        style: gilroyBoldTextStyle.copyWith(
                            decoration: TextDecoration.underline,
                            letterSpacing: 2,
                            fontSize: Dimensions.fontSizeDefault
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(
                      height: 6.h,
                    ),
                  ],
                ),
              ),
            ),
          ],
        )
      ),
    );
  }

  bool? validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern as String);
    if (!regex.hasMatch(value)) {
      return false;
    } else {
      return null;
    }
  }

  forgotPassword(UserProvider userProvider) async {
    userProvider.setIsLoading = true;
    await userProvider.userResetPassword(
        username: userNameController.text.trim(),
        email: emailController.text.trim(),
    );
    userProvider.setIsLoading = false;
    if (userProvider.commonSuccessResponse?.isSuccess ?? false) {
      _pageController.jumpToPage(1);
    } else {
      if(userProvider.errorMessage == "User not found"){
        mSGAlert(
          context: scaffoldMessengerKey.currentContext!,
          title: "Oops",
          msg: userProvider.errorMessage,
          onCancel: () {
            Navigator.pop(scaffoldMessengerKey.currentContext!);
          },
          height: userProvider.errorMessage.length > 100 ? 30.h : 25.h,
        );
      }else{
        confirmationAlert(
            context: scaffoldMessengerKey.currentContext!,
            title: userProvider.errorMessage == noInternet ? "No Connection" : "Oops",
            msg: userProvider.errorMessage,
            onCancel: () {
              Navigator.pop(scaffoldMessengerKey.currentContext!);
            },
            onYes: () async {
              Navigator.pop(scaffoldMessengerKey.currentContext!);
              forgotPassword(userProvider);
            },
            yesButtonName: "Retry",
            noButtonName: "No",
            height: userProvider.errorMessage.length > 100 ? 30.h : 25.h
        );
      }

    }
  }

  loginWithNewPassword(UserProvider userProvider, BuildContext context) async {

    userProvider.setIsLoading = true;
    await userProvider.getLoginResponse(
      userName: getUserName()!,
      password: pinController.text.trim(),
    );
    if(userProvider.loginResponse?.isSuccess?? false){

      //save user details on Shared prefs
      saveUserToken(userProvider.loginResponse!.payload!.token!);
      saveUserId(userProvider.loginResponse!.payload!.userId!);
      saveUserLoginId(userProvider.loginResponse!.payload!.loginId!);

      print("${getUserLoginId()} ${getUserToken()} ${getUserId()}");

      //set userLocations
      final locationProvider = Provider.of<LocationProvider>(context, listen: false);
      if(userProvider.loginResponse!.payload!.locationList != null
          && userProvider.loginResponse!.payload!.locationList!.isNotEmpty){
        locationProvider.setLocationList = userProvider.loginResponse!.payload!.locationList!;
        locationProvider.setSelectedLocation = userProvider.loginResponse!.payload!.defaultLocation!;
        saveLocationId(locationProvider.getSelectedLocation!.locationId!);
        saveLocationName(locationProvider.getSelectedLocation!.locationName!);

        print("${getUserId()} ${getUserLoginId()} ${getUserToken()}");
        final deviceProvider = Provider.of<DeviceProvider>(context, listen: false);
        await deviceProvider.getDevicesByUserLocation(locationId: getLocationId());

        userProvider.setIsLoading = false;
        //save battery id and serial numbers in shared prefs
        if(deviceProvider.getDevicesByUserLocationResponse?.isSuccess ?? false){
          if(deviceProvider.getDeviceList != null && deviceProvider.getDeviceList!.isNotEmpty){
            for(int i=0; i<deviceProvider.getDeviceList!.length; i++){
              if(deviceProvider.getDeviceList![i].deviceTypeInfo!.deviceTypeId == 1){
                //powerCell
                saveBatteryId(deviceProvider.getDevicesByUserLocationResponse!.payload!.deviceList![i].componentId!);
                savePowerCellSerialNumber(deviceProvider.getDevicesByUserLocationResponse!.payload?.deviceList![i].serialNumber?? "");

                //check device active or not
                if(deviceProvider.getDeviceList![i].isConnected!){
                  saveIsPowerCellIPaired(true);
                  //start MQTT
                  final mqttProvider = Provider.of<MqttProvider>(context, listen: false);
                  mqttProvider.main(context);
                }else{
                  saveIsPowerCellIPaired(false);
                }
              }else if(deviceProvider.getDeviceList![i].deviceTypeInfo!.deviceTypeId == 2){
                //Inverter
              }else{
                //Solar
              }
            }}

          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => UpdatePasswordScreen(password: pinController.text.trim(),)),
          );

        }else{
          userProvider.setIsLoading = false;
          mSGAlert(
            context: context,
            title: "Oops!",
            msg: deviceProvider.errorMessage,
            onCancel: (){
              Navigator.pop(context);
            },
            height: 25.h,
          );
        }
      }
    }else{
      userProvider.setIsLoading = false;
      mSGAlert(
        context: context,
        title: "Oops!",
        msg: userProvider.errorMessage,
        onCancel: (){
          Navigator.pop(context);
        },
        height: 25.h,
      );
    }
  }

}
