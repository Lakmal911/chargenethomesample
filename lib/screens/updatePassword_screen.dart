import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/providers/commonProvider.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/providers/userProvider.dart';
import 'package:home/reusableWidgets/bottomNavBar.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customSnackBar.dart';
import 'package:home/reusableWidgets/customTextField.dart';
import 'package:home/reusableWidgets/sessionExpired.dart';
import 'package:home/screens/login_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';

class UpdatePasswordScreen extends StatefulWidget {
  final String? password;
  const UpdatePasswordScreen({Key? key, this.password}) : super(key: key);

  @override
  State<UpdatePasswordScreen> createState() => _UpdatePasswordScreenState();
}

class _UpdatePasswordScreenState extends State<UpdatePasswordScreen> {
  final currentPasswordController = TextEditingController();
  final newPasswordController = TextEditingController();
  final newRePasswordController = TextEditingController();
  GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  void initState() {
    super.initState();
    currentPasswordController.text = widget.password?? "";
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async{
        if(widget.password != null){
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => BottomNavBar()),
          );
          return false;
        }else{
          return true;
        }
      },
      child: SafeArea(
        child: Container(
          child: Scaffold(
            key: scaffoldMessengerKey,
            backgroundColor: Colors.transparent,
            appBar: CustomAppBar(
              title: lUpdatePassword,
              isBackButtonExist: true,
              isAction: false,
              backOnTap: () {
                if(widget.password != null){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BottomNavBar()),
                  );
                }else{
                  Navigator.pop(context);
                }
              },
              iconData: Icons.create_outlined,
              iconOnTap: () {},
            ),
            body: Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: Dimensions.paddingOverLarge, vertical: Dimensions.paddingDefault),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 2.h,
                    ),
                    const Text(
                      "Your new password must be different from previous used passwords",
                      style: gilroyBoldTextStyle,
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                    Consumer<CommonProvider>(
                        builder: (context, commonProvider, child) {
                        return CustomTextField(
                          controller: currentPasswordController,
                          isHint: true,
                          hint: "x x x x x",
                          textFieldName: lCurrentPassword,
                          onChangedText: (dsf) {},

                          obscureText: commonProvider.getIsObscureTextPassword,
                          onSubmit: (){},
                          suffix: GestureDetector(
                            onTap: (){
                              if(commonProvider.getIsObscureTextPassword){
                                commonProvider.isObscureTextPassword = false;
                              }else{
                                commonProvider.isObscureTextPassword = true;
                              }
                            },
                            child: Icon(
                                commonProvider.getIsObscureTextPassword
                                    ? Icons.visibility_outlined
                                    : Icons.visibility_off_outlined,
                                color: kColorWhite
                            ),
                          ),
                        );
                      }
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                    Consumer<CommonProvider>(
                        builder: (context, commonProvider, child) {
                        return CustomTextField(
                          controller: newPasswordController,
                          isHint: true,
                          hint: "x x x x x",
                          textFieldName: lPassword,
                          onChangedText: (dsf){
                          },
                          obscureText: commonProvider.getIsObscureTextRePassword,
                          onSubmit: (){

                          },
                          suffix: GestureDetector(
                            onTap: (){
                              if(commonProvider.getIsObscureTextRePassword){
                                commonProvider.isObscureTextRePassword = false;
                              }else{
                                commonProvider.isObscureTextRePassword = true;
                              }
                            },
                            child: Icon(
                                commonProvider.getIsObscureTextRePassword
                                    ? Icons.visibility_outlined
                                    : Icons.visibility_off_outlined,
                                color: kColorWhite
                            ),
                          ),
                        );
                      }
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                    CustomTextField(
                      controller: newRePasswordController,
                      isHint: true,
                      hint: "x x x x x",
                      textFieldName: lRePassword,
                      onChangedText: (dsf){
                      },
                      obscureText: true,
                      onSubmit: (){},
                    ),
                    SizedBox(
                      height: 6.h,
                    ),

                    Center(
                      child: Consumer<UserProvider>(
                          builder: (context, userProvider, child) {
                          return Material(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(10),
                            child:
                            userProvider.isLoading
                                ? const CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor),
                            )
                                : CustomButton(
                              buttonColor: kPrimaryColor,
                              fontColor: kColorWhite,
                              onTap: () async {
                                if (currentPasswordController.text.isEmpty){
                                  ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Current Password cannot be empty!"));
                                }else if (newPasswordController.text.isEmpty){
                                  ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "New Password cannot be empty!"));
                                }else if(newPasswordController.text.trim().toLowerCase() != newRePasswordController.text.trim().toLowerCase()){
                                  ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Password not matched!"));
                                }else {
                                  updatePassword(userProvider);
                                }
                              },
                              name: lUpdate,
                              height: 50,
                              width: 50.w,
                              // )
                            )
                    );
                        },
                      ),
                  ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  updatePassword(UserProvider userProvider) async {
    userProvider.setIsLoading = true;
    await userProvider.userUpdatePassword(
      oldPassword: widget.password ?? currentPasswordController.text.trim(),
      newPassword: newPasswordController.text.trim()
    );
    userProvider.setIsLoading = false;

    if (userProvider.commonSuccessResponse?.isSuccess ?? false) {
      mSGAlert(
        context: scaffoldMessengerKey.currentContext!,
        title: "Successful!",
        msg: "Password has been changed successfully. Please login with new password",
        onCancel: (){
          Navigator.pop(scaffoldMessengerKey.currentContext!);
          //clear providers and shared prefs then go to login page
          clearSharedPrefs();
          final mqttProvider = Provider.of<MqttProvider>(scaffoldMessengerKey.currentContext!, listen: false);
          mqttProvider.clearMqttProviderData();

          Navigator.pushReplacement(scaffoldMessengerKey.currentContext!, MaterialPageRoute(builder: (context) => LoginScreen()),);

        },
        height: userProvider.errorMessage.length > 100 ? 30.h : 25.h,
      );
    }else if(userProvider.errorMessage == "Invalid access token"){
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    }else {
      if(userProvider.errorMessage =="Incorrect password"
      || userProvider.errorMessage == "Password must contain at least one upper-case, lower-case, numeric and special character with minimum password length of 8 characters"){
        mSGAlert(
          context: context,
          title: "Oops",
          msg: userProvider.errorMessage,
          onCancel: () {
            Navigator.pop(context);
          },
          height: userProvider.errorMessage.length > 100 ? 30.h : 25.h,
        );
      }else{
        confirmationAlert(
            context: scaffoldMessengerKey.currentContext!,
            title: userProvider.errorMessage == noInternet ? "No Connection" : "Oops",
            msg: userProvider.errorMessage,
            onCancel: () {
              Navigator.pop(scaffoldMessengerKey.currentContext!);
            },
            onYes: () async {
              Navigator.pop(scaffoldMessengerKey.currentContext!);
              updatePassword(userProvider);
            },
            yesButtonName: "Retry",
            noButtonName: "No",
            height: userProvider.errorMessage.length > 100 ? 30.h : 25.h
        );
      }
    }
  }
}
