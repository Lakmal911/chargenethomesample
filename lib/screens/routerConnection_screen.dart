import 'dart:async';
import 'dart:io';
import 'package:esptouch_flutter/esptouch_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:network_info_plus/network_info_plus.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/reusableWidgets/bottomNavBar.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customSnackBar.dart';
import 'package:home/reusableWidgets/customTextField.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/loadingComponent.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';

class RouterConnectionScreen extends StatefulWidget {
  const RouterConnectionScreen({Key? key}) : super(key: key);

  @override
  _RouterConnectionScreenState createState() => _RouterConnectionScreenState();
}

class _RouterConnectionScreenState extends State<RouterConnectionScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final TextEditingController ssid = TextEditingController();
  final TextEditingController bssid = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController expectedTaskResults = TextEditingController();
  final TextEditingController intervalGuideCode = TextEditingController();
  final TextEditingController intervalDataCode = TextEditingController();
  final TextEditingController timeoutGuideCode = TextEditingController();
  final TextEditingController timeoutDataCode = TextEditingController();
  final TextEditingController repeat = TextEditingController();
  final TextEditingController portListening = TextEditingController();
  final TextEditingController portTarget = TextEditingController();
  final TextEditingController waitUdpReceiving = TextEditingController();
  final TextEditingController waitUdpSending = TextEditingController();
  final TextEditingController thresholdSucBroadcastCount = TextEditingController();
  ESPTouchPacket packet = ESPTouchPacket.multicast;

  @override
  void dispose() {
    ssid.dispose();
    bssid.dispose();
    password.dispose();
    expectedTaskResults.dispose();
    intervalGuideCode.dispose();
    intervalDataCode.dispose();
    timeoutGuideCode.dispose();
    timeoutDataCode.dispose();
    repeat.dispose();
    portListening.dispose();
    portTarget.dispose();
    waitUdpReceiving.dispose();
    waitUdpSending.dispose();
    thresholdSucBroadcastCount.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      setWifiInfo();
    });
  }

  setWifiInfo()async{
    if (Platform.isAndroid) {
      var status = await Permission.location.status;
      // Blocked?
      if (status.isLimited || status.isDenied || status.isRestricted) {
        // Ask the user to unblock
        if (await Permission.location.request().isGranted) {
          getWifiInfo();
        } else {
          print('Location permission not granted');
        }
      } else {
        getWifiInfo();
      }
    }
  }

  getWifiInfo()async{
    final info = NetworkInfo();
    var wifiName = await info.getWifiName(); // FooNetwork
    var wifiBSSID = await info.getWifiBSSID(); // 11:22:33:44:55:66
    setState(() {
      ssid.text = (wifiName?.toString().replaceAll("\"", "").trim()) ?? "N/A";
      bssid.text = wifiBSSID ?? "N/A";
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: CustomAppBar(
          title: "Router Connection",
          isBackButtonExist: true,
          isAction: false,
          backOnTap: (){
            // if(getLocationId() != -1 && getIsPowerCellIPaired()){
            //   Navigator.pop(context);
            // }else{
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => BottomNavBar()),
              );
            // }
           // Navigator.pop(context);
          },
          iconOnTap: (){},
        ),
        // Using builder to get context without creating new widgets
        //  https://docs.flutter.io/flutter/material/Scaffold/of.html
        body: Builder(builder: (context) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: Dimensions.paddingOverLarge),
          child: form(context),
        )),
      ),
    );
  }

  createTask() {
    Duration? durationTryParse(String milliseconds) {
      final parsed = int.tryParse(milliseconds);
      return parsed != null ? Duration(milliseconds: parsed) : null;
    }

    return ESPTouchTask(
      ssid: ssid.text.replaceAll("\"", "").trim(),
      bssid: bssid.text,
      password: password.text.trim(),
      packet: packet,
      taskParameter: const ESPTouchTaskParameter().copyWith(
        intervalGuideCode: durationTryParse(intervalGuideCode.text),
        intervalDataCode: durationTryParse(intervalDataCode.text),
        timeoutGuideCode: durationTryParse(timeoutGuideCode.text),
        timeoutDataCode: durationTryParse(timeoutDataCode.text),
        waitUdpSending: durationTryParse(waitUdpSending.text),
        waitUdpReceiving: durationTryParse(waitUdpReceiving.text),
        repeat: int.tryParse(repeat.text),
        portListening: int.tryParse(portListening.text),
        portTarget: int.tryParse(portTarget.text),
        thresholdSucBroadcastCount:
        int.tryParse(thresholdSucBroadcastCount.text),
        expectedTaskResults: int.tryParse(expectedTaskResults.text),
      ),
    );
  }

  Widget form(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    return Form(
      key: formKey,
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        children: <Widget>[
          // Center(
          //   child: OutlinedButton(
          //     onPressed: fetchingWifiInfo ? null : fetchWifiInfo,
          //     child: Text(
          //       fetchingWifiInfo ? 'Fetching WiFi info' : 'Use current Wi-Fi',
          //     ),
          //   ),
          // ),

          Padding(
            padding: const EdgeInsets.only(top: 20),
            child:  CustomTextField(
              controller: ssid,
              isHint: true,
              hint: "Lakmal\'s iPhone",
              textFieldName: "SSID",
              onChangedText: (dsf){
              },
              obscureText: false,
              onSubmit: (){

              },
            ),
          ),

          // Padding(
          //   padding: const EdgeInsets.only(top: 20),
          //   child:  CustomTextField(
          //     controller: bssid,
          //     isHint: true,
          //     hint: '00:a0:c9:14:c8:29',
          //     textFieldName: "BSSID",
          //     onChangedText: (dsf){
          //     },
          //     obscureText: false,
          //     onSubmit: (){
          //
          //     },
          //   ),
          // ),

          Padding(
            padding: const EdgeInsets.only(top: 20),
            child:  CustomTextField(
              controller: password,
              isHint: true,
              hint: r'V3Ry.S4F3-P@$$w0rD',
              textFieldName: 'Password',
              onChangedText: (dsf){
              },
              obscureText: false,
              onSubmit: (){

              },
            ),
          ),

          //execute Button
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 50),
            child: Center(
              child: Material(
                color: Colors.transparent,
                borderRadius: BorderRadius.circular(10),
                child: CustomButton(
                  buttonColor: kPrimaryColor,
                  fontColor: kColorBlack,
                  onTap: (){
                    //text fields validations
                    if(ssid.text.isEmpty){
                      ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "SSID cannot be empty!"));
                    }
                    // else if(password.text.isEmpty){
                    //   ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Password cannot be empty!"));
                    // }
                    else{
                      //if the pair device screen pop
                      FocusScope.of(context).requestFocus(FocusNode());
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TaskRoute(task: createTask()),
                        ),
                      );
                    }
                  },
                  name: "Execute",
                  height: 50,
                  width: 35.w,
                ),
              ),
            ),
          ),

        ],
      ),
    );
  }

  void setPacket(ESPTouchPacket? packet) {
    if (packet == null) return;
    setState(() {
      this.packet = packet;
    });
  }
}

class TaskRoute extends StatefulWidget {
  TaskRoute({required this.task});

  final ESPTouchTask task;

  @override
  State<StatefulWidget> createState() => TaskRouteState();
}

class TaskRouteState extends State<TaskRoute> {
  late final Stream<ESPTouchResult> stream;
  late final StreamSubscription<ESPTouchResult> streamSubscription;
  late final Timer timer;

  final List<ESPTouchResult> results = [];

  @override
  void initState() {
    stream = widget.task.execute();
    streamSubscription = stream.listen(results.add);
    final receiving = widget.task.taskParameter.waitUdpReceiving;
    final sending = widget.task.taskParameter.waitUdpSending;
    final cancelLatestAfter = receiving + sending;
    timer = Timer(
      receiving,
          () {
        streamSubscription.cancel();
        if (results.isEmpty && mounted) {
          mSGAlert(
              context: context,
              title: "Alert",
              msg: "No devices found",
              onCancel: (){
                // exit(0);
                Navigator.pop(context);
                Navigator.pop(context);
              },
              height: 25.h
          );
        }
      },
    );
    super.initState();
  }

  @override
  dispose() {
    timer.cancel();
    streamSubscription.cancel();
    super.dispose();
  }

  Widget connectionStateComponent({required String msg, required IconData iconName, required ConnectionState state}){
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          iconName,
          color: kColorBlack,
          size: 100.sp,
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          msg,
          style: gilroyRegularTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault),
          textAlign: TextAlign.center,
        ),

        Padding(
          padding: const EdgeInsets.symmetric(vertical: 50),
          child: Center(
            child: Material(
              color: Colors.transparent,
              borderRadius: BorderRadius.circular(10),
              child: CustomButton(
                buttonColor: kPrimaryColor,
                fontColor: kColorBlack,
                onTap: () async{
                  if(state == ConnectionState.none){
                    Navigator.pop(context);
                  }else{
                    saveIsPowerCellIPaired(true);
                    //start MQTT
                    final mqttProvider = Provider.of<MqttProvider>(context, listen: false);
                    mqttProvider.main(context);

                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => BottomNavBar()),
                    );
                  }
                },
                name: lGo,
                height: 50,
                width: 35.w,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget error(BuildContext context, String msg) {
    return connectionStateComponent(
        iconName: Icons.error,
        msg: msg,
        state: ConnectionState.none
    );
  }

  copyValue(BuildContext context, String label, String v) {
    return () {
      Clipboard.setData(ClipboardData(text: v));
      final snackBar = SnackBar(content: Text('Copied $label to clipboard $v'));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    };
  }

  Widget noneState(BuildContext context) {
    return connectionStateComponent(
        iconName: Icons.error,
        msg: "No State",
        state: ConnectionState.none
    );
  }

  Widget resultList(BuildContext context) {
    return connectionStateComponent(
        iconName: Icons.assignment_turned_in_outlined,
        msg: "Device has been paired successfully",
        state: ConnectionState.done
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: SafeArea(
        child: Container(
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: CustomAppBar(
              title: "Router Connection",
              isBackButtonExist: true,
              isAction: false,
              iconOnTap: (){},
              backOnTap: (){
                Navigator.pop(context);
                // if(getIsPowerCellIPaired()){
                //   Navigator.pop(context);
                // }else{
                //   Navigator.push(
                //     context,
                //     MaterialPageRoute(builder: (context) => BottomNavBar()),
                //   );
                // }
              },
            ),
            body: StreamBuilder<ESPTouchResult>(
              builder: (context, AsyncSnapshot<ESPTouchResult> snapshot) {
                if (snapshot.hasError) {
                  return error(context, 'Error in StreamBuilder');
                }
                if (snapshot.hasData && mounted) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.active:
                      return resultList(context);
                    case ConnectionState.none:
                      return noneState(context);
                    case ConnectionState.done:
                      return resultList(context);
                    case ConnectionState.waiting:
                      return const Center(child: LoadingComponent(loadingMsg: 'Waiting...'));
                  }
                }else{
                  return const LoadingComponent(loadingMsg: 'Connecting...');
                }
              },
              stream: stream,
            ),
          ),
        ),
      ),
    );
  }
}

// class TaskParameterDetails extends StatelessWidget {
//   const TaskParameterDetails({
//     Key? key,
//     required this.color,
//     required this.expectedTaskResults,
//     required this.intervalGuideCode,
//     required this.intervalDataCode,
//     required this.timeoutGuideCode,
//     required this.timeoutDataCode,
//     required this.repeat,
//     required this.portListening,
//     required this.portTarget,
//     required this.waitUdpReceiving,
//     required this.waitUdpSending,
//     required this.thresholdSucBroadcastCount,
//   }) : super(key: key);
//
//   final Color color;
//   final TextEditingController expectedTaskResults;
//   final TextEditingController intervalGuideCode;
//   final TextEditingController intervalDataCode;
//   final TextEditingController timeoutGuideCode;
//   final TextEditingController timeoutDataCode;
//   final TextEditingController repeat;
//   final TextEditingController portListening;
//   final TextEditingController portTarget;
//   final TextEditingController waitUdpReceiving;
//   final TextEditingController waitUdpSending;
//   final TextEditingController thresholdSucBroadcastCount;
//
//   @override
//   Widget build(BuildContext context) {
//     return ExpansionTile(
//       title: Text(
//         'Task parameter detail',
//         style: TextStyle(color: color),
//       ),
//       trailing: Icon(
//         Icons.settings,
//         color: color,
//       ),
//       initiallyExpanded: false,
//       children: <Widget>[
//         OptionalIntegerTextField(
//           controller: expectedTaskResults,
//           labelText: 'Expected task results (count)',
//           hintText: 1,
//           helperText: 'The number of devices you expect to scan.',
//         ),
//         OptionalIntegerTextField(
//           controller: intervalGuideCode,
//           labelText: 'Interval guide code (ms)',
//           hintText: 8,
//         ),
//         OptionalIntegerTextField(
//           controller: intervalDataCode,
//           labelText: 'Interval data code (ms)',
//           hintText: 8,
//         ),
//         OptionalIntegerTextField(
//           controller: timeoutGuideCode,
//           labelText: 'Timeout guide code (ms)',
//           hintText: 2000,
//         ),
//         OptionalIntegerTextField(
//           controller: timeoutDataCode,
//           labelText: 'Timeout data code (ms)',
//           hintText: 4000,
//         ),
//         OptionalIntegerTextField(
//           controller: repeat,
//           labelText: 'Repeat',
//           hintText: 1,
//         ),
//         // The mac and ip length are skipped for now.
//         OptionalIntegerTextField(
//           controller: portListening,
//           labelText: 'Listen on port',
//           hintText: 18266,
//         ),
//         OptionalIntegerTextField(
//           controller: portTarget,
//           labelText: 'Target port',
//           hintText: 7001,
//         ),
//         OptionalIntegerTextField(
//           controller: waitUdpReceiving,
//           labelText: 'Wait UDP receiving (ms)',
//           hintText: 15000,
//         ),
//         OptionalIntegerTextField(
//           controller: waitUdpSending,
//           labelText: 'Wait UDP sending (ms)',
//           hintText: 45000,
//         ),
//         OptionalIntegerTextField(
//           controller: thresholdSucBroadcastCount,
//           labelText: 'Broadcast count success threshold',
//           hintText: 1,
//         ),
//       ],
//     );
//   }
// }
//
// class OptionalIntegerTextField extends StatelessWidget {
//   const OptionalIntegerTextField({
//     Key? key,
//     required this.controller,
//     required this.labelText,
//     required this.hintText,
//     this.helperText,
//   }) : super(key: key);
//
//   final TextEditingController controller;
//   final String labelText;
//   final int hintText;
//   final String? helperText;
//
//   @override
//   Widget build(BuildContext context) {
//     return TextFormField(
//       controller: controller,
//       keyboardType: TextInputType.number,
//       validator: (String? value) {
//         // The user didn't edit the field, so can return early without an error
//         // and skip validating (int parsing).
//         value = (value ?? '').trim();
//         if (value == '') return null;
//
//         final v = int.tryParse(value, radix: 10);
//         if (v == null) return 'Please enter an integer number';
//       },
//       autovalidateMode: AutovalidateMode.onUserInteraction,
//       decoration: InputDecoration(
//         labelText: labelText,
//         hintText: hintText.toString(),
//         helperText: helperText,
//       ),
//     );
//   }
// }

const helperSSID = "SSID is the technical term for a network name. "
    "When you set up a wireless home network, "
    "you give it a name to distinguish it from other networks in your neighbourhood.";
const helperBSSID =
    "BSSID is the MAC address of the wireless access point (router).";
const helperPassword = "The password of the Wi-Fi network";
class SimpleWifiInfo {
  static const platform =
  MethodChannel('eng.smaho.com/esptouch_plugin/example');
  static Future<String?> get ssid => platform.invokeMethod('ssid');
  static Future<String?> get bssid => platform.invokeMethod('bssid');
}