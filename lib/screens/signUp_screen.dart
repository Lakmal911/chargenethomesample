import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/providers/commonProvider.dart';
import 'package:home/providers/userProvider.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customSnackBar.dart';
import 'package:home/reusableWidgets/customTextField.dart';
import 'package:home/screens/login_screen.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';

import '../data/base/sharedPrefs.dart';

class SignUpScreen extends StatelessWidget {
  SignUpScreen({Key? key}) : super(key: key);

  final userNameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final rePasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: Container(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 40.0),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 8.h,
                    ),
                    SizedBox(
                      height: 5.h,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        //UserName TextField
                        CustomTextField(
                          controller: userNameController,
                          isHint: true,
                          hint: "John321",
                          textFieldName: lUserName,
                          onChangedText: (dsf){
                          },
                          obscureText: false,
                          onSubmit: (){

                          },
                        ),
                        SizedBox(
                          height: 4.h,
                        ),

                        //Email TextField
                        CustomTextField(
                          controller: emailController,
                          isHint: true,
                          hint: "jone@gmail.com",
                          textFieldName: lEmail,
                          onChangedText: (dsf){
                          },
                          obscureText: false,
                          onSubmit: (){

                          },
                        ),
                        SizedBox(
                          height: 4.h,
                        ),

                        //Password TextField
                        Consumer<CommonProvider>(
                            builder: (context, commonProvider, child) {
                            return CustomTextField(
                              controller: passwordController,
                              isHint: true,
                              hint: "x x x x x",
                              textFieldName: lPassword,
                              onChangedText: (dsf){
                              },
                              obscureText: commonProvider.getIsObscureTextPassword,
                              onSubmit: (){

                              },
                              suffix: GestureDetector(
                                onTap: (){
                                  if(commonProvider.getIsObscureTextPassword){
                                    commonProvider.isObscureTextPassword = false;
                                  }else{
                                    commonProvider.isObscureTextPassword = true;
                                  }
                                },
                                child: Icon(
                                    commonProvider.getIsObscureTextPassword
                                        ? Icons.visibility_outlined
                                        : Icons.visibility_off_outlined,
                                    color: kColorWhite
                                ),
                              ),
                            );
                          }
                        ),

                        SizedBox(
                          height: 4.h,
                        ),

                        //re-Password TextField
                        Consumer<CommonProvider>(
                            builder: (context, commonProvider, child) {
                              return CustomTextField(
                                controller: rePasswordController,
                                isHint: true,
                                hint: "x x x x x",
                                textFieldName: lRePassword,
                                onChangedText: (dsf){
                                },
                                obscureText: commonProvider.getIsObscureTextRePassword,
                                onSubmit: (){

                                },
                                suffix: GestureDetector(
                                  onTap: (){
                                    if(commonProvider.getIsObscureTextRePassword){
                                      commonProvider.isObscureTextRePassword = false;
                                    }else{
                                      commonProvider.isObscureTextRePassword = true;
                                    }
                                  },
                                  child: Icon(
                                      commonProvider.getIsObscureTextRePassword
                                          ? Icons.visibility_outlined
                                          : Icons.visibility_off_outlined,
                                      color: kColorWhite
                                  ),
                                ),
                              );
                            }
                        ),

                        SizedBox(
                          height: 4.h,
                        ),

                        Text.rich(
                            TextSpan(
                                children: [
                                  TextSpan(
                                    text: lAgreeTo,
                                    style: gilroyBoldTextStyle.copyWith(
                                        color: kColorWhite,
                                        fontSize: Dimensions.fontSizeExtraSmall
                                    ),
                                  ),
                                  TextSpan(
                                    text: lTerms,
                                    style: gilroyBoldTextStyle.copyWith(
                                        color: kSecondaryColor,
                                        fontSize: Dimensions.fontSizeExtraSmall,
                                        height: 1.5
                                    ),
                                  ),
                                ]
                            )
                        ),

                        SizedBox(
                          height: 6.5.h,
                        ),
                        Center(
                          child: Material(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(10),
                            child: Consumer<UserProvider>(
                                builder: (context, userProvider, child) {
                                return userProvider.isLoading
                                    ? const CircularProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(kSecondaryColor))
                                    : CustomButton(
                                  buttonColor: kPrimaryColor,
                                  fontColor: kColorWhite,
                                  onTap: () async{
                                    if(userNameController.text.isEmpty){
                                      ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "User Name cannot be empty!"));
                                    }else if(RegExp(r"\s").hasMatch(userNameController.text)){
                                      ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "User Name must be entered without whitespace!"));
                                    }else if(emailController.text.isEmpty){
                                      ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Email cannot be empty!"));
                                    }else if(validateEmail(emailController.text.trim()) == false){
                                      ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Please check your email again!"));
                                    }else if(passwordController.text.isEmpty){
                                      ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Password cannot be empty!"));
                                    }else if(passwordController.text.trim() != rePasswordController.text.trim()){
                                      ScaffoldMessenger.of(context).showSnackBar(customSnackBar(context, "Passwords not matched!"));
                                    }else{
                                      clearSharedPrefs();
                                      registerWithCredentials(userProvider: userProvider, context: context);
                                    }
                                  },
                                  name: lSignUp,
                                  height: 50,
                                  width: 35.w,
                                );
                              }
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 4.h, bottom: 2.h),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text.rich(
                            TextSpan(
                                children: [
                                  TextSpan(
                                    text: lJoinedUsBefore,
                                    style: gilroyBoldTextStyle.copyWith(
                                        color: kColorWhite,
                                        fontSize: Dimensions.fontSizeExtraSmall
                                    ),
                                  ),
                                  TextSpan(
                                    text: lLogin,
                                    recognizer: TapGestureRecognizer()..onTap = () {

                                      //set obscure text true
                                      Provider.of<CommonProvider>(context, listen: false).isObscureTextPassword = true;

                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => LoginScreen()),
                                      );
                                    },
                                    style: gilroyBoldTextStyle.copyWith(
                                        color: kSecondaryColor,
                                        fontSize: Dimensions.fontSizeExtraSmall
                                    ),
                                  ),
                                ]
                            )
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
    );
  }

  bool? validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = RegExp(pattern as String);
    if (!regex.hasMatch(value)) {
      return false;
    } else {
      return null;
    }
  }

  registerWithCredentials({required UserProvider userProvider, required BuildContext context})async{
    userProvider.setIsLoading = true;
    await userProvider.getRegisterResponse(
      username: userNameController.text.trim(),
      email: emailController.text.trim(),
      password: passwordController.text.trim(),
    );
    userProvider.setIsLoading = false;
    if(userProvider.registerResponse?.isSuccess?? false){

      //clear text controllers
      userNameController.clear();
      emailController.clear();
      passwordController.clear();
      rePasswordController.clear();

      mSGAlert(
        context: context,
        title: "Successful",
        msg: "You have been registered successfully. Please enter your credentials and login to the Vega Power",
        onCancel: (){
          //pop alert and go to next page
          FocusScope.of(context).requestFocus(FocusNode());

          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => LoginScreen()),
          );
        },
        height: userProvider.errorMessage.length > 100 ? 30.h : 25.h,
      );
    }else{
      if(userProvider.errorMessage == "Password must contain at least one upper-case, lower-case, numeric and special character with minimum password length of 8 characters"){
        mSGAlert(
          context: context,
          title: "Oops",
          msg: userProvider.errorMessage,
          onCancel: () {
            Navigator.pop(context);
          },
          height: userProvider.errorMessage.length > 100 ? 30.h : 25.h,
        );
      }else{
        confirmationAlert(
            context: context,
            title: userProvider.errorMessage == noInternet ? "No Connection" : "Oops",
            msg: userProvider.errorMessage,
            onCancel: () {
              Navigator.pop(context);
            },
            onYes: () async {
              Navigator.pop(context);
              registerWithCredentials(userProvider: userProvider, context: context);
            },
            yesButtonName: "Retry",
            noButtonName: "No",
            height: userProvider.errorMessage.length > 100 ? 30.h : 25.h
        );
      }
    }
  }
}
