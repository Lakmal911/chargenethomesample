import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:syncfusion_flutter_sliders/sliders.dart';
import 'package:home/models/chargeLevelTypesResponse.dart';
import 'package:home/providers/deviceProvider.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/customSnackBar.dart';
import 'package:home/reusableWidgets/customTimePicker.dart';
import 'package:home/providers/batteryCellProvider.dart';
import 'package:home/reusableWidgets/customAppBar.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/reusableWidgets/loadingComponent.dart';
import 'package:home/reusableWidgets/sessionExpired.dart';
import 'package:home/utils/app_constants.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';
import 'package:syncfusion_flutter_core/theme.dart';

class PowerCustomizeScreen extends StatelessWidget {
   PowerCustomizeScreen({Key? key}) : super(key: key);

  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();
  final ScrollController listController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        child: DefaultTabController(
          length: 2,
          child: Scaffold(
            key: scaffoldMessengerKey,
            backgroundColor: Colors.transparent,
            appBar: CustomAppBar(
              title: lPowerCell,
              isBackButtonExist: true,
              isAction: false,
              backOnTap: () {
                Navigator.pop(context);
              },
              iconData: Icons.lock_outlined,
              iconOnTap: () {},
            ),
            body: Column(
              children: [
                const SizedBox(
                  height: 15,
                ),
                Expanded(
                  flex: 1,
                  child: TabBar(
                    labelColor: kColorBlack,
                    labelStyle:
                    gilroyBoldTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault, fontWeight: FontWeight.w400),
                    unselectedLabelColor: kColorBlack.withOpacity(0.5),
                    // indicator: const BoxDecoration(),
                    indicatorColor: kSecondaryColor,
                    tabs: const [Text("PEAK/ OFF PEAK"), Text("CHARGE LEVEL")],
                  ),
                ),
                Expanded(
                  flex: 12,
                  child: TabBarView(
                    children: <Widget>[
                      //Peak/ Off peak tab
                      backUpTimeTab(context),

                      //Charge Level Tab
                      chargeLevelTab(context)
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget powerCellComponent({required String topic, required String value}) {
    return Expanded(
      child: Container(
        height: 100,
        decoration: BoxDecoration(color: kPrimaryColor, borderRadius: BorderRadius.circular(15), boxShadow: [
          BoxShadow(
            color: kSecondaryColor.withAlpha(70),
            blurRadius: 7.0,
            spreadRadius: 3.0,
            offset: const Offset(0.0, 4.0),
          ),
        ]),
        child: Padding(
          padding: const EdgeInsets.all(Dimensions.paddingDefault),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                topic,
                style: gilroyRegularTextStyle.copyWith(
                    fontWeight: FontWeight.w600, fontSize: Dimensions.fontSizeDefault, letterSpacing: 1.5),
              ),
              const SizedBox(
                height: 10,
              ),

              //must consider the power value font size and color
              Text(
                value,
                style: gilroyRegularTextStyle.copyWith(
                    fontWeight: FontWeight.w600, color: kSecondaryColor, fontSize: 8.sp, letterSpacing: 1.5),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget longCard({required String value, required String topic, required Function() onPress}) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        height: 100,
        width: 100.w,
        decoration: BoxDecoration(color: kPrimaryColor, borderRadius: BorderRadius.circular(15), boxShadow: [
          BoxShadow(
            color: kSecondaryColor.withAlpha(70),
            blurRadius: 7.0,
            spreadRadius: 3.0,
            offset: const Offset(0.0, 4.0),
          ),
        ]),
        child: Padding(
          padding: const EdgeInsets.all(Dimensions.paddingDefault),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                topic,
                style: gilroyRegularTextStyle.copyWith(
                    fontWeight: FontWeight.w600,
                    color: kColorBlack,
                    fontSize: Dimensions.fontSizeLarge,
                    letterSpacing: 1.5),
              ),
              Text(
                value,
                style: gilroyRegularTextStyle.copyWith(
                    fontWeight: FontWeight.w600,
                    color: kSecondaryColor,
                    fontSize: Dimensions.fontSizeDefault,
                    letterSpacing: 1.5),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget chargeLevelTab(BuildContext context) {
    return FutureBuilder(
      future: getChargeLevels(context),
      builder: (ctx, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Padding(
            padding: EdgeInsets.only(top: 5.h),
            child: const LoadingComponent(loadingMsg: "Loading Charge Levels..."),
          );
        }else{
          return Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
              return batteryCellProvider.isLoading
                  ? Padding(
                padding: EdgeInsets.only(top: 2.h),
                child: const LoadingComponent(loadingMsg: "Loading Charge Levels..."),
              )
                  : Stack(
                    children: [
                      SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: Dimensions.paddingOverLarge),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "CHARGE LEVEL TYPE",
                                style: gilroyBoldTextStyle.copyWith(
                                  fontSize: Dimensions.fontSizeSmall,
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Theme(
                                data: ThemeData(
                                  textTheme: TextTheme(
                                      subtitle1:
                                      gilroyRegularTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault)),
                                ),
                                child: Consumer<DeviceProvider>(
                                    builder: (context, deviceProvider, child) {
                                      return DropdownSearch<ChargeLevelTypes>(
                                        selectedItem: deviceProvider.getSelectedChargeLevelType,
                                        items: deviceProvider.getChargeLevelTypesResponse?.chargeLevelTypes ?? [],
                                        dropdownButtonProps: const DropdownButtonProps(color: kAccentColor),
                                        dropdownDecoratorProps: const DropDownDecoratorProps(
                                          dropdownSearchDecoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(color: kAccentColor, width: 3),
                                            ),
                                            iconColor: kSecondaryColor,
                                          ),
                                        ),
                                        popupProps: const PopupProps.menu(
                                          menuProps: MenuProps(backgroundColor: kPrimaryColor),
                                          showSearchBox: false,
                                          fit: FlexFit.tight,
                                          constraints: BoxConstraints(maxHeight: 200),
                                        ),
                                        onChanged: (chargeLevelType) {
                                          deviceProvider.setSelectedChargeLevelType = chargeLevelType ?? ChargeLevelTypes();

                                          //scroll pixels
                                          listController.animateTo(
                                            listController.position.maxScrollExtent,
                                            duration: const Duration(milliseconds: 800),
                                            curve: Curves.fastOutSlowIn,
                                          );
                                        },
                                        // selectedItem: "Brazil",
                                      );
                                    }),
                              ),
                              const SizedBox(
                                height: 40,
                              ),
                              Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
                                return Text(
                                  "CHARGE LEVEL (${batteryCellProvider.chargeValue.floor()}%)",
                                  style: gilroyBoldTextStyle.copyWith(
                                    fontSize: Dimensions.fontSizeSmall,
                                  ),
                                );
                              }),
                              const SizedBox(
                                height: 5,
                              ),
                              Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
                                return SfSlider(
                                  min: 50.0,
                                  max: 100.0,
                                  value: batteryCellProvider.chargeValue.floor(),
                                  showTicks: false,
                                  showLabels: false,
                                  enableTooltip: true,
                                  minorTicksPerInterval: 1,
                                  onChanged: (dynamic value) {
                                    batteryCellProvider.setChargeValue = value;
                                  },
                                );
                              }),
                              const SizedBox(
                                height: 30,
                              ),
                              Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
                                return Text(
                                  "DISCHARGE LEVEL (${batteryCellProvider.disChargeValue.floor()}%)",
                                  style: gilroyBoldTextStyle.copyWith(
                                    fontSize: Dimensions.fontSizeSmall,
                                  ),
                                );
                              }),
                              const SizedBox(
                                height: 5,
                              ),
                              Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
                                return SfSliderTheme(
                                  data: SfSliderThemeData(
                                      activeTickColor: kSecondaryColor,
                                      inactiveTickColor: kSecondaryColor.withOpacity(0.3),
                                      activeLabelStyle: gilroyRegularTextStyle,
                                      inactiveLabelStyle: gilroyRegularTextStyle),
                                  child: SfSlider(
                                    min: 0,
                                    max: 49,
                                    value: batteryCellProvider.disChargeValue,
                                    activeColor: kSecondaryColor,
                                    showTicks: false,
                                    showLabels: false,
                                    enableTooltip: true,
                                    minorTicksPerInterval: 1,
                                    onChanged: (dynamic value) {
                                      batteryCellProvider.setDisChargeValue = value;
                                    },
                                  ),
                                );
                              }),
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 5.h, bottom: 6.h),
                          child: Center(
                              child: Container(
                                color: kAccentColor,
                                height: 1,
                                width: 80.w,
                              )),
                        ),
                        Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
                          return Text(
                            "USER RESERVE CAPACITY (${batteryCellProvider.isEnableReverseCapacity ? "ON" : "OFF"})",
                            style: gilroyBoldTextStyle.copyWith(
                              fontSize: Dimensions.fontSizeDefault,
                            ),
                          );
                        }),
                        Padding(
                          padding: EdgeInsets.only(top: 2.h, bottom: 15.h),
                          child: SizedBox(
                            height: 100.sp,
                            width: 100.sp,
                            child: Center(
                              child: Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
                                return GestureDetector(
                                  onTap: () {
                                    if (batteryCellProvider.isEnableReverseCapacity) {
                                      batteryCellProvider.setIsEnableReverseCapacity = false;
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(customSnackBar(context, "User Reserve capacity has been OFF!"));
                                    } else {
                                      batteryCellProvider.setIsEnableReverseCapacity = true;
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(customSnackBar(context, "User Reserve capacity has been ON!"));
                                    }
                                  },
                                );
                              }),
                            ),
                          ),
                        ),

                ],
              ),
                      ),
                      Positioned(
                        bottom: 0,
                        width: 100.w,
                        child: Container(
                          color: kPrimaryColor,
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top: Dimensions.paddingOverLarge, bottom: Dimensions.paddingOverLarge),
                              child: Center(
                                child: Material(
                                  borderRadius: BorderRadius.circular(10),
                                  child: CustomButton(
                                    buttonColor: kPrimaryColor,
                                    fontColor: kColorBlack,
                                    onTap: () async {
                                      final deviceProvider = Provider.of<DeviceProvider>(context, listen: false);
                                      if((deviceProvider.getSelectedChargeLevelType?.idInverterType?? "-1") != "-1"){
                                        await updateChargeLevels(batteryCellProvider, context, deviceProvider.getSelectedChargeLevelType!.idInverterType!);
                                      }else{
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(customSnackBar(context, "Charge Level Type is not selected!"));
                                      }
                                    },
                                    name: lApply,
                                    height: 50,
                                    width: 35.w,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
            }),
          );
        }},);
  }

  Widget backUpTimeTab(BuildContext context) {
    return FutureBuilder(
        future: loadBackupTimeData(context),
        builder: (ctx, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Padding(
              padding: EdgeInsets.only(top: 5.h),
              child: const LoadingComponent(loadingMsg: "Loading Backup Times..."),
            );
          }else{
            return Padding(
              padding: const EdgeInsets.only(top: Dimensions.paddingOverLarge),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Column(
                      children: [
                        Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  if (batteryCellProvider.isWeekEndSelected) {
                                    batteryCellProvider.setIsWeekEndSelected = false;
                                    batteryCellProvider.setIsWeekDaySelected = true;
                                  }
                                },
                                child: Container(
                                  height: 50,
                                  width: 40.w,
                                  color: batteryCellProvider.isWeekDaySelected ? kSecondaryColor : Colors.transparent,
                                  child: Center(
                                    child: Text(
                                      "WEEK DAYS",
                                      style: gilroyRegularTextStyle.copyWith(
                                          fontWeight: FontWeight.bold,
                                          fontSize: Dimensions.fontSizeDefault,
                                          color: batteryCellProvider.isWeekDaySelected
                                              ? kColorBlack
                                              : kSecondaryColor.withOpacity(0.5),
                                          letterSpacing: 1.5),
                                    ),
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  if (batteryCellProvider.isWeekDaySelected) {
                                    batteryCellProvider.setIsWeekEndSelected = true;
                                    batteryCellProvider.setIsWeekDaySelected = false;
                                  }
                                },
                                child: Container(
                                  height: 50,
                                  width: 40.w,
                                  color: batteryCellProvider.isWeekEndSelected ? kSecondaryColor : Colors.transparent,
                                  child: Center(
                                    child: Text(
                                      "WEEKENDS",
                                      style: gilroyRegularTextStyle.copyWith(
                                          fontWeight: FontWeight.bold,
                                          color: batteryCellProvider.isWeekEndSelected
                                              ? kColorBlack
                                              : kSecondaryColor.withOpacity(0.5),
                                          fontSize: Dimensions.fontSizeDefault,
                                          letterSpacing: 1.5),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          );
                        }),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(Dimensions.paddingOverLarge, Dimensions.paddingDefault,
                              Dimensions.paddingOverLarge, Dimensions.paddingDefault),
                          child: Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
                            return batteryCellProvider.isLoading
                                ? Padding(
                              padding: EdgeInsets.only(top: 10.h),
                              child: const LoadingComponent(loadingMsg: "Loading Backup Times..."),
                            )
                                : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: Dimensions.paddingOverLarge),
                                  child: Row(
                                    children: [
                                      CircleAvatar(
                                        radius: 5.sp,
                                        backgroundColor: kSecondaryAccentColor,
                                      ),
                                      SizedBox(
                                        width: 3.w,
                                      ),
                                      Text(
                                        "BACKUP TIME",
                                        style: gilroyBoldTextStyle.copyWith(
                                          fontWeight: FontWeight.bold,
                                          fontSize: Dimensions.fontSizeDefault,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: Dimensions.paddingOverLarge),
                                      child:
                                      Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
                                        return longCard(
                                            topic: "Morning",
                                            value: batteryCellProvider.isWeekEndSelected
                                                ? "${batteryCellProvider.weekEndMorningStartTime.substring(0, 5)} - ${batteryCellProvider.weekEndMorningEndTime.substring(0, 5)}"
                                                : "${batteryCellProvider.weekDayMorningStartTime.substring(0, 5)} - ${batteryCellProvider.weekDayMorningEndTime.substring(0, 5)}",
                                            onPress: () {
                                              batteryCellProvider.clearTimeRange();
                                              customFromToPicker(
                                                  context: scaffoldMessengerKey.currentContext!,
                                                  title: "Select morning time rage",
                                                  isMorning: true,
                                                  onCancel: () {
                                                    Navigator.pop(scaffoldMessengerKey.currentContext!);
                                                  },
                                                  onYes: () async {
                                                    Navigator.pop(scaffoldMessengerKey.currentContext!);
                                                    final batteryCellProvider = Provider.of<BatteryCellProvider>(context, listen: false);
                                                    if (batteryCellProvider.selectedTimeRange!.start !=
                                                        const TimeOfDay(hour: 55, minute: 55)) {
                                                      batteryCellProvider.validateMyBackupTime(true);
                                                      await updateBackupTimes(
                                                          batteryCellProvider,
                                                          batteryCellProvider.isWeekEndSelected
                                                              ? batteryCellProvider.weekEndMorningStartTime
                                                              : batteryCellProvider.weekDayMorningStartTime,
                                                          batteryCellProvider.isWeekEndSelected
                                                              ? batteryCellProvider.weekEndMorningEndTime
                                                              : batteryCellProvider.weekDayMorningEndTime,
                                                          1,
                                                        scaffoldMessengerKey.currentContext!
                                                      );
                                                    } else {
                                                      mSGAlert(
                                                        context: context,
                                                        title: "Oops!",
                                                        msg: "Please select the time range before continue!",
                                                        onCancel: () {
                                                          Navigator.pop(context);
                                                        },
                                                        height:
                                                        batteryCellProvider.errorMessage.length > 100 ? 30.h : 25.h,
                                                      );
                                                    }
                                                  },
                                                  yesButtonName: "OK",
                                                  noButtonName: "Cancel",
                                                  height: 55.h,
                                                  firstTimeHour: 0,
                                                  firstTimeMinute: 0,
                                                  lastTimeHour: 12,
                                                  lastTimeMinute: 00);
                                            });
                                      }),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: Dimensions.paddingOverLarge),
                                      child:
                                      Consumer<BatteryCellProvider>(builder: (context, batteryCellProvider, child) {
                                        return longCard(
                                            topic: "Evening",
                                            value: batteryCellProvider.isWeekEndSelected
                                                ? "${batteryCellProvider.weekEndEveningStartTime.substring(0, 5)} - ${batteryCellProvider.weekEndEveningEndTime.substring(0, 5)}"
                                                : "${batteryCellProvider.weekDayEveningStartTime.substring(0, 5)} - ${batteryCellProvider.weekDayEveningEndTime.substring(0, 5)}",
                                            onPress: () {
                                              batteryCellProvider.clearTimeRange();
                                              customFromToPicker(
                                                  context: scaffoldMessengerKey.currentContext!,
                                                  title: "Select evening time rage",
                                                  isMorning: false,
                                                  onCancel: () {
                                                    Navigator.pop(scaffoldMessengerKey.currentContext!);
                                                  },
                                                  onYes: () async {
                                                    Navigator.pop(scaffoldMessengerKey.currentContext!);
                                                    if (batteryCellProvider.selectedTimeRange!.start !=
                                                        const TimeOfDay(hour: 55, minute: 55)) {
                                                      batteryCellProvider.validateMyBackupTime(false);
                                                      await updateBackupTimes(
                                                          batteryCellProvider,
                                                          batteryCellProvider.isWeekEndSelected
                                                              ? batteryCellProvider.weekEndEveningStartTime
                                                              : batteryCellProvider.weekDayEveningStartTime,
                                                          batteryCellProvider.isWeekEndSelected
                                                              ? batteryCellProvider.weekEndEveningEndTime
                                                              : batteryCellProvider.weekDayEveningEndTime,
                                                          2,
                                                        scaffoldMessengerKey.currentContext!,
                                                      );

                                                    } else {
                                                      mSGAlert(
                                                        context: context,
                                                        title: "Oops!",
                                                        msg: "Please select the time range before continue!",
                                                        onCancel: () {
                                                          Navigator.pop(context);
                                                        },
                                                        height:
                                                        batteryCellProvider.errorMessage.length > 100 ? 30.h : 25.h,
                                                      );
                                                    }
                                                  },
                                                  yesButtonName: "OK",
                                                  noButtonName: "Cancel",
                                                  height: 55.h,
                                                  firstTimeHour: 12,
                                                  firstTimeMinute: 0,
                                                  lastTimeHour: 24,
                                                  lastTimeMinute: 00);
                                            });
                                      }),
                                    ),
                                  ],
                                ),

                                // Padding(
                                //   padding: EdgeInsets.only(top: 10.h),
                                //   child: Center(
                                //     child: GestureDetector(
                                //       onTap: ()async{
                                //
                                //       },
                                //       child: SizedBox(
                                //         width: 60.w,
                                //         child: Row(
                                //           mainAxisAlignment: MainAxisAlignment.spaceAround,
                                //           children: [
                                //             const Icon(Icons.build_circle_outlined, color: Colors.white),
                                //             Text(
                                //               "Additional Features",
                                //               style: gilroyBoldTextStyle.copyWith(
                                //                 decoration: TextDecoration.underline,
                                //                 letterSpacing: 2,
                                //                 fontSize: Dimensions.fontSizeDefault
                                //               ),
                                //               textAlign: TextAlign.center,
                                //             ),
                                //           ],
                                //         ),
                                //       ),
                                //     ),
                                //   ),
                                // ),
                              ],
                            );
                          }),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            );
          }
        }
    );
  }

  Future<bool> loadBackupTimeData(BuildContext context) async {
    await loadBackupTime(context);
    return true;
  }

  Future<bool> getChargeLevels(BuildContext context) async {
    await loadChargeLevel(context);
    return true;
  }

  Future<bool> loadBackupTime(BuildContext context) async {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final batteryCellProvider = Provider.of<BatteryCellProvider>(context, listen: false);
      batteryCellProvider.setIsWeekDaySelected = true;
      batteryCellProvider.setIsWeekEndSelected = false;
      batteryCellProvider.setIsLoading = true;
      //get Backup time slots
      await batteryCellProvider.getBackupTimeSlots();
      batteryCellProvider.setIsLoading = false;
      if (batteryCellProvider.getBackupTimeSlotsResponse?.isSuccess ?? false) {
      }else if(batteryCellProvider.errorMessage == "Invalid access token"){
        sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
      }else {
        confirmationAlert(
            context: context,
            title: batteryCellProvider.errorMessage == noInternet ? "No Connection" : "Oops",
            msg: batteryCellProvider.errorMessage,
            onCancel: () {
              Navigator.pop(context);
            },
            onYes: () async {
              Navigator.pop(context);
              loadBackupTime(context);
            },
            yesButtonName: "Retry",
            noButtonName: "No",
            height: batteryCellProvider.errorMessage.length > 100 ? 30.h : 25.h
        );
      }
    });

    return true;
  }

  Future<bool> loadChargeLevel(BuildContext context) async {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      final batteryCellProvider = Provider.of<BatteryCellProvider>(context, listen: false);

      batteryCellProvider.setIsLoading = true;
      //get chargeLevel types
      final deviceProvider = Provider.of<DeviceProvider>(context, listen: false);
      await deviceProvider.getAllChargeLevelTypes();

      //get charge levels
      deviceProvider.setSelectedChargeLevelType = ChargeLevelTypes(
          idInverterType: "-1",
          type: ""
      );
      await batteryCellProvider.getChargeLevels();
      batteryCellProvider.setIsLoading = false;
      if (batteryCellProvider.getChargeLevelsResponse?.isSuccess ?? false) {
        //set default selected charge level type
        for(int i=0; i < deviceProvider.getChargeLevelTypesResponse!.chargeLevelTypes!.length; i++){
          if(batteryCellProvider.inverterTypeId == (deviceProvider.getChargeLevelTypesResponse!.chargeLevelTypes![i].idInverterType)){
            deviceProvider.setSelectedChargeLevelType = deviceProvider.getChargeLevelTypesResponse!.chargeLevelTypes![i];
          }
        }
      }else if(batteryCellProvider.errorMessage == "Invalid access token"){
      }else {
        if(batteryCellProvider.errorMessage != noInternet){
          confirmationAlert(
              context: context,
              title: "Oops",
              msg: batteryCellProvider.errorMessage,
              onCancel: () {
                Navigator.pop(context);
              },
              onYes: () async {
                Navigator.pop(context);
                loadChargeLevel(context);
              },
              yesButtonName: "Retry",
              noButtonName: "No",
              height: batteryCellProvider.errorMessage.length > 100 ? 30.h : 25.h
          );
        }
      }
    });

    return true;
  }

  updateChargeLevels(BatteryCellProvider batteryCellProvider, BuildContext context, String idInverterType) async {
    batteryCellProvider.setIsLoading = true;
    await batteryCellProvider.updateChargeLevels(inverterTypeId: idInverterType);
    batteryCellProvider.setIsLoading = false;
    if (batteryCellProvider.commonSuccessResponse?.isSuccess ?? false) {
      mSGAlert(
        context: context,
        title: "Successful",
        msg: "Charge levels has been updated successfully!",
        onCancel: () {
          Navigator.pop(context);
        },
        height: batteryCellProvider.errorMessage.length > 100 ? 30.h : 25.h,
      );
    }else if(batteryCellProvider.errorMessage == "Invalid access token"){
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    }else {
      confirmationAlert(
          context: context,
          title: batteryCellProvider.errorMessage == noInternet ? "No Connection" : "Oops",
          msg: batteryCellProvider.errorMessage,
          onCancel: () {
            Navigator.pop(context);
          },
          onYes: () async {
            Navigator.pop(context);
            updateChargeLevels(batteryCellProvider, context, idInverterType);
          },
          yesButtonName: "Retry",
          noButtonName: "No",
          height: batteryCellProvider.errorMessage.length > 100 ? 30.h : 25.h
      );
    }
  }

  updateBackupTimes(BatteryCellProvider batteryCellProvider, startTime, endTime, periodOfDay, BuildContext context) async {
    batteryCellProvider.setIsLoading = true;
    await batteryCellProvider.updateBackupTimes(
        startTime: startTime,
        endTime: endTime,
        dateGroup: batteryCellProvider.isWeekEndSelected ? 2 : 1,
        periodOfDay: periodOfDay);
    batteryCellProvider.setIsLoading = false;
    if (batteryCellProvider.commonSuccessResponse?.isSuccess ?? false) {
      mSGAlert(
        context: context,
        title: "Successful",
        msg: "BackUp Time has been updated successfully!",
        onCancel: () {
          Navigator.pop(context);
        },
        height: batteryCellProvider.errorMessage.length > 100 ? 30.h : 25.h,
      );
    }else if(batteryCellProvider.errorMessage == "Invalid access token"){
      sessionExpired(context: context, scaffoldMessengerKey: scaffoldMessengerKey);
    }else {
      confirmationAlert(
          context: context,
          title: batteryCellProvider.errorMessage == noInternet ? "No Connection" : "Oops",
          msg: batteryCellProvider.errorMessage,
          onCancel: () {
            Navigator.pop(context);
          },
          onYes: () async {
            Navigator.pop(context);
            updateBackupTimes(batteryCellProvider, startTime, endTime, periodOfDay, context);
          },
          yesButtonName: "Retry",
          noButtonName: "No",
          height: batteryCellProvider.errorMessage.length > 100 ? 30.h : 25.h
      );
    }
  }

}

