import 'package:dio/dio.dart';
import 'package:home/data/base/api_response.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/data/repository/dio/dio_client.dart';
import 'package:home/data/repository/exception/api_error_handler.dart';
import 'package:home/utils/base.dart';

class BatteryCellService {
  final DioClient dioClient;
  BatteryCellService({required this.dioClient});

  Future<ApiResponse> getBatteryCellStatus({required int deviceId}) async {
    try {
      Response? response = await dioClient.post(
          Base.getBatteryCellStatus,
          data: {
            "deviceId": deviceId,
            'token': getUserToken(),
          },
          options: Options(
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getSocHistory({required DateTime from, required DateTime to}) async {
    try {
      Response? response = await dioClient.post(
          Base.getSocHistory,
          data: {
            "batteryId": getBatteryId(),
            "from": from,
            "to": to,
            "token": getUserToken()
          },
          options: Options(
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getChargeLevels() async {
    try {
      Response? response = await dioClient.post(
          Base.getChargeLevels,
          data: {
            "batteryId": getBatteryId(),
            'token': getUserToken(),
          },
          options: Options(
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getBackupTimeSlots() async {
    try {
      Response? response = await dioClient.post(
        Base.getBackupTimeSlots,
        data: {
          "batteryId": getBatteryId(),
          'token': getUserToken(),
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> updateBackupTiming({
    required String startTime, required String endTime,
    required int dateGroup, required int periodOfDay
  }) async {
    try {
      Response? response = await dioClient.post(
          Base.updateBackupTiming,
          data: {
            "batteryId": getBatteryId(),
            "dateGroup": dateGroup,    //dateGroup: [1: Weekday | 2: Weekend]
            "periodOfDay": periodOfDay,  //periodOfDay: [1: Morning | 2: Evening]
            "startTime": startTime,
            "endTime": endTime,
            'token': getUserToken(),
          },
          options: Options(
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> updateChargeLevels({
    required double chargeLimit, required double dischargeLimit,
    required bool enableReverseCapacity, required String inverterTypeId
  }) async {
    try {
      Response? response = await dioClient.post(
          Base.updateChargeLevels,
          data: {
            "batteryId": getBatteryId(),
            "chargeLimit": chargeLimit,
            "dischargeLimit": dischargeLimit,
            "enableReverseCapacity": enableReverseCapacity,
            'token': getUserToken(),
            "inverterTypeId": inverterTypeId
          },
          options: Options(
              headers: {'Content-Type': 'application/x-www-form-urlencoded'},
          ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}