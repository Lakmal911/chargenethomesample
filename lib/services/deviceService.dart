import 'package:dio/dio.dart';
import 'package:home/data/base/api_response.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/data/repository/dio/dio_client.dart';
import 'package:home/data/repository/exception/api_error_handler.dart';
import 'package:home/utils/base.dart';

class DeviceService {
  final DioClient dioClient;
  DeviceService({required this.dioClient});

  Future<ApiResponse> addDeviceType({required String deviceTypeName}) async {
    try {
      Response? response = await dioClient.post(
        Base.addDevice,
        data: {
          "deviceTypeName": deviceTypeName,
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getDevicesByUserLocation({required int locationId}) async {
    try {
      Response? response = await dioClient.post(
        Base.getDevicesByUserLocation,
        data: {
          "userId": getUserId(),
          "locationId": locationId,
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getAllDeviceTypes() async {
    try {
      Response? response = await dioClient.post(
        Base.getAllDeviceTypes,
        data: {
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> addDevice({required int deviceTypeId, required String serialNumber}) async {
    try {
      Response? response = await dioClient.post(
        Base.addDevice,
        data: {
          "serialNumber": serialNumber,
          "deviceTypeId": deviceTypeId,
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getDeviceById({required int deviceId}) async {
    try {
      Response? response = await dioClient.post(
        Base.getDeviceById,
        data: {
          "deviceId": int,
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getDevicesByDevicesType({required int deviceTypeId}) async {
    try {
      Response? response = await dioClient.post(
        Base.getDevicesByDeviceType,
        data: {
          "deviceTypeId": deviceTypeId,
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getDevicesByUser() async {
    try {
      Response? response = await dioClient.post(
        Base.getDevicesByUser,
        data: {
          "userId": getUserLoginId(),
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> assignDeviceToUser({
    required String serialNumber, required int locationId,
    String? locationName, required String pin
  }) async {
    try {
      Response? response = await dioClient.post(
        Base.assignDeviceToUser,
        data: {
          "userId": getUserId(),
          "locationId": locationId,
          "locationName": locationName ?? "",
          "serialNumber": serialNumber,
          "pin": pin,
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getAllChargeLevelTypes() async {
    try {
      Response? response = await dioClient.post(
        Base.getAllInverterTypes,
        data: {
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getDeletedDevices(int locationId) async {
    try {
      Response? response = await dioClient.post(
        Base.getDeletedDevices,
        data: {
          "token": getUserToken(),
          "userId": getUserId(),
          "locationId": locationId,
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> updateDeviceDeleteStatus(int deviceId, bool isDelete) async {
    try {
      print("${getUserToken()}, $deviceId $isDelete");
      Response? response = await dioClient.post(
        Base.changeDeleteState,
        data: {
          "token": getUserToken(),
          "deviceId": deviceId,
          "isDelete": isDelete,
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

}