import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:home/data/base/api_response.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/data/repository/dio/dio_client.dart';
import 'package:home/data/repository/exception/api_error_handler.dart';
import 'package:home/utils/base.dart';

class UserService {
  final DioClient dioClient;
  UserService({required this.dioClient});

  Future<ApiResponse> userRegister({
    // required String firstName, required String lastName,
    required String username,
    required String password, required String email,
    // required String contactNo,
    // required int roleId, required bool isActive,
  }) async {
    try {

      //make requestToken
      var bytes = utf8.encode("$username$password"); // data being hashed
      var requestToken = sha1.convert(bytes);
      print("Digest as hex string: ${requestToken.toString().toUpperCase()}");

      Response? response = await dioClient.post(
        Base.register,
        data: {
          "firstName": "",
          "lastName": "",
          "username": username,
          "password": password,
          "email": email,
          "contactNo": "0718523677",
          "roleId": "2",
          "isActive": true,
          "requestToken": requestToken.toString().toUpperCase()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> userLogin({
    required String userName, required String password, required String loginMode,
  }) async {
    try {
      Response? response = await dioClient.post(
        Base.login,
        data: {
          "username": userName,
          "password": password,
          "loginMode": loginMode
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),

      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> userLogout() async {
    print("${getUserLoginId()} ${getUserToken()}");
    try {
      Response? response = await dioClient.post(
        Base.logout,
        data: {
          "loginId": getUserLoginId(),
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getUser() async {
    print(getUserId());
    print(getUserToken());
    try {
      Response? response = await dioClient.post(
        Base.getUserById,
        data: {
          "userId": getUserId(),
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> userUpdate({
   required String firstName, required String lastName, required String email, required String contactNo,
  }) async {
    try {
      Response? response = await dioClient.post(
        Base.updateUser,
        data: {
          "userId": getUserId(),
          "firstName": firstName,
          "lastName": lastName,
          "email": email,
          "contactNo": contactNo,
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> userResetPassword({
   required String username, required String email,
  }) async {
    try {
      //make requestToken
      var bytes = utf8.encode("$username$email"); // data being hashed
      var requestToken = sha1.convert(bytes);
      print("Digest as hex string: ${requestToken.toString().toUpperCase()}");

      Response? response = await dioClient.post(
        Base.forgotPassword,
        data: {
          "username": username,
          "email": email,
          "requestToken": requestToken.toString().toUpperCase()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> userUpdatePassword({required String oldPassword, required String newPassword}) async {
    try {
      Response? response = await dioClient.post(
        Base.updatePassword,
        data: {
          "userId": getUserId(),
          "oldPassword": oldPassword,
          "newPassword": newPassword,
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

}