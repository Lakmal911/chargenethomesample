import 'package:dio/dio.dart';
import 'package:home/data/base/api_response.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/data/repository/dio/dio_client.dart';
import 'package:home/data/repository/exception/api_error_handler.dart';
import 'package:home/utils/base.dart';

class LocationService {
  final DioClient dioClient;
  LocationService({required this.dioClient});

  Future<ApiResponse> addUserLocation({required String locationName}) async {
    try {
      Response? response = await dioClient.post(
        Base.addLocation,
        data: {
          "locationName": locationName,
          "userId": getUserId(),
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getLocationById({required int locationId}) async {
    try {
      Response? response = await dioClient.post(
        Base.getLocationById,
        data: {
          "locationId": locationId,
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getLocationByUser() async {
    try {
      Response? response = await dioClient.post(
        Base.getLocationByUser,
        data: {
          "userId": getUserId(),
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> setDefaultLocation({required int locationId}) async {
    try {
      Response? response = await dioClient.post(Base.setDefaultLocation,
        data: {
          "userId": getUserId(),
          "locationId": locationId,
          "token": getUserToken()
        },
        options: Options(
          headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        ),
      );
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

}