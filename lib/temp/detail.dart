import 'dart:io';

import 'package:flutter/material.dart';
import 'package:home/utils/textStyles.dart';
import 'package:sizer/sizer.dart';

import '../reusableWidgets/customAlert.dart';
import '../reusableWidgets/customAppBar.dart';
import '../utils/dimensions.dart';

class DetailScreen extends StatelessWidget {
  DetailScreen({Key? key}) : super(key: key);

  final GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldMessengerKey,
      backgroundColor: Colors.transparent,
      appBar: CustomAppBar(
        title: "",
        isBackButtonExist: true,
        isAction: false,
        backOnTap: (){
          mSGAlert(
            context: context,
            title: "Alert",
            msg: "Are you sure you want to quite?",
            onCancel: () {
              exit(0);
            },
            height: 25.h,
          );
        },
        iconOnTap: (){},
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(Dimensions.paddingOverLarge, 0, Dimensions.paddingOverLarge, 10),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 45.h,
                child: GridView.count(
                  padding: EdgeInsets.zero,
                  physics: const NeverScrollableScrollPhysics(),
                  crossAxisCount: 2,
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
                      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
                      decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10.0)), color: Colors.grey.withOpacity(0.5)),
                      child: Column(
                        children: [
                          const Text("Speed", style: gilroyBoldTextStyle),
                          SizedBox(height: 2.h),
                          Text("85", style: gilroyBoldTextStyle.copyWith(fontSize: 35.sp)),
                          SizedBox(height: 0.5.h),
                          const Text("km/h", style: gilroyBoldTextStyle),
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
                      decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10.0)), color: Colors.grey.withOpacity(0.5)),
                      child: Column(
                        children: [
                          const Text("Battery", style: gilroyBoldTextStyle),
                          SizedBox(height: 2.h),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Text("Voltage", style: gilroyRegularTextStyle, textAlign: TextAlign.center,),
                                  SizedBox(height: 0.5.h),
                                  const Text("62v", style: gilroyRegularTextStyle),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Text("SOH", style: gilroyRegularTextStyle, textAlign: TextAlign.center,),
                                  SizedBox(height: 0.5.h),
                                  const Text("90%", style: gilroyRegularTextStyle),
                                ],
                              )
                            ],
                          ),
                          SizedBox(height: 1.5.h),
                          Column(
                            children: [
                              const Text("Temperature", style: gilroyRegularTextStyle),
                              SizedBox(height: 1.h),
                              const Text("40 C", style: gilroyRegularTextStyle),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
                      decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10.0)), color: Colors.grey.withOpacity(0.5)),
                      child: Column(
                        children: [
                          const Text("Motor", style: gilroyBoldTextStyle),
                          SizedBox(height: 2.h),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Text("Motor\nPower", style: gilroyRegularTextStyle, textAlign: TextAlign.center,),
                                  SizedBox(height: 0.5.h),
                                  const Text("2 kw", style: gilroyRegularTextStyle),
                                ],
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  const Text("Motor\nTemperature", style: gilroyRegularTextStyle, textAlign: TextAlign.center,),
                                  SizedBox(height: 0.5.h),
                                  const Text("70 C", style: gilroyRegularTextStyle),
                                ],
                              )
                            ],
                          ),
                          SizedBox(height: 1.5.h),
                          Column(
                            children: [
                              const Text("Temprature", style: gilroyRegularTextStyle),
                              SizedBox(height: 1.h),
                              const Text("40 C", style: gilroyRegularTextStyle),
                            ],
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 15.0),
                      decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10.0)), color: Colors.grey.withOpacity(0.5)),
                      child: Column(
                        children: [
                          const Text("Inverter", style: gilroyBoldTextStyle),
                          SizedBox(height: 2.h),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const Text("Inverter\nTemperature", style: gilroyRegularTextStyle, textAlign: TextAlign.center,),
                              SizedBox(height: 2.h),
                              const Text("45 C", style: gilroyRegularTextStyle),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _itemCard(name: "Speed", value: "85 km/h"),
                      _itemCard(name: "RPM", value: "85"),
                      _itemCard(name: "DC Current", value: "3"),
                      _itemCard(name: "MC Current", value: "10"),
                      _itemCard(name: "SOC", value: "10"),
                      _itemCard(name: "Pack voltage", value: "10"),
                    ],
                  ),
                  SizedBox(width: 6.w),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _itemCard(name: "MC Temperature", value: "10"),
                      _itemCard(name: "Motor Temp", value: "10"),
                      _itemCard(name: "Bat Temperature", value: "10"),
                      _itemCard(name: "Speed Mode", value: "10"),
                      _itemCard(name: "ODO", value: "10"),
                    ],
                  )
                ],
              )

            ],
          ),
        )
      ),
    );
  }
  Widget _itemCard({required String name, required String value}) {
    return Row(
      children: [
        Text(name, style: gilroyBoldTextStyle),
        SizedBox(width: 10.w),
        Text(value, style: gilroyRegularTextStyle),
        SizedBox(height: 4.h),
      ],
    );
  }
}
