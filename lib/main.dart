import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:home/providers/batteryCellProvider.dart';
import 'package:home/providers/commonProvider.dart';
import 'package:home/providers/deviceProvider.dart';
import 'package:home/providers/locationProvider.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/providers/userProvider.dart';
import 'package:home/homeApp.dart';
import 'package:provider/provider.dart';
import 'package:home/data/base/di_container.dart' as di;

void main() async{

  WidgetsFlutterBinding.ensureInitialized();
  //register dio client and other providers
  await di.init();

  //orientation - portrait
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

  runApp(
      MultiProvider(
          providers: [
            ChangeNotifierProvider<CommonProvider>(create: (_) => CommonProvider()),
            ChangeNotifierProvider(create: (context) => di.sl<UserProvider>()),
            ChangeNotifierProvider(create: (context) => di.sl<LocationProvider>()),
            ChangeNotifierProvider(create: (context) => di.sl<DeviceProvider>()),
            ChangeNotifierProvider(create: (context) => di.sl<BatteryCellProvider>()),
            ChangeNotifierProvider(create: (context) => MqttProvider()),
          ],
          child: const HomeApp(),
      ),
  );
}
