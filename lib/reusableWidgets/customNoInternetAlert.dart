import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:home/reusableWidgets/customAlert.dart';

customNoInternetAlert(
    {
      required BuildContext context,
      required Function() onYes,
    }
    ) {

  return confirmationAlert(
      context: context,
      title: "No Connection",
      msg: "Please check your Internet Connection!",
      onCancel: (){
        Navigator.pop(context);
      },
      onYes: onYes,
      yesButtonName: "Retry",
      noButtonName: "Cancel",
      height: 25.h
  );
}
