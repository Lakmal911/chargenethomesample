import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/textStyles.dart';

class LoadingComponent extends StatelessWidget{
  final String loadingMsg;

  const LoadingComponent({required this.loadingMsg});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SpinKitFadingCube(
            color: Colors.black,
            size: 50.0,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40),
            child: Text(
              loadingMsg,
              style: gilroyRegularTextStyle.copyWith(
                  fontWeight: FontWeight.w600,
                  fontSize: Dimensions.fontSizeDefault,
                  letterSpacing: 1.5
              ),
            ),
          ),
        ],
      ),
    );
  }
}
