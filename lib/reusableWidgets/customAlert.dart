import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:home/reusableWidgets/customButton.dart';
import 'package:home/reusableWidgets/loadingComponent.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/textStyles.dart';

confirmationAlert(
{
  required BuildContext context,
  required String title,
  required String msg,
  required Function() onCancel,
  required Function() onYes,
  required String yesButtonName,
  required String noButtonName,
  required double height
}
) {

  return showDialog<String>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) => WillPopScope(
      onWillPop: () => Future.value(false),
      child: Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        backgroundColor: kPrimaryColor,
        child: Container(
          height: height,
          margin: const EdgeInsets.all(15),
          padding: const EdgeInsets.symmetric(horizontal: 15),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            color: kPrimaryColor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: Text(
                    title,
                    style: gilroyBoldTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault,),
                    textAlign: TextAlign.center,
                  ),
              ),
              Text(
                msg,
                style: gilroyRegularTextStyle.copyWith(fontSize: Dimensions.fontSizeSmall,),
                textAlign: TextAlign.center,
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 10, top: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: CustomButton(
                          buttonColor: kPrimaryColor,
                          fontColor: kColorBlack,
                          onTap: onCancel,
                          name: noButtonName,
                          height: 40,
                        width: 35.w,
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: CustomButton(
                          buttonColor: kColorWhite,
                          fontColor: kColorBlack,
                          onTap: onYes,
                          name: yesButtonName,
                          height: 40,
                        width: 35.w,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    ),
  );
}

mSGAlert(
    {
      required BuildContext context,
      required String title,
      required String msg,
      required onCancel,
      required height
    }
    ) {

  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) => WillPopScope(
      onWillPop: () => Future.value(false),
      child: Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        backgroundColor: kPrimaryColor,
        child: Container(
          height: height,
          margin: const EdgeInsets.all(15),
          padding: const EdgeInsets.symmetric(horizontal: 15),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            color: kPrimaryColor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: // Delete Articles
                  Text(title,
                      style: gilroyBoldTextStyle.copyWith(
                        fontSize: Dimensions.fontSizeDefault,
                      ),
                      textAlign: TextAlign.center)),
              Text(msg,
                  style: gilroyRegularTextStyle.copyWith(
                    fontSize: Dimensions.fontSizeDefault,
                  ),
                  textAlign: TextAlign.center),
              Container(
                margin: const EdgeInsets.only(bottom: 10, top: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CustomButton(
                        buttonColor: kPrimaryColor,
                        fontColor: kColorBlack,
                        onTap: onCancel,
                        name: "OK",
                        height: 40,
                      width: 35.w,
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    ),
  );
}

customLoading(BuildContext context) {
  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) => WillPopScope(
      onWillPop: () => Future.value(false),
      child: Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        backgroundColor: kPrimaryColor,
        child: Container(
          height: 70,
          margin: const EdgeInsets.all(15),
          padding: const EdgeInsets.symmetric(horizontal: 15),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            color: kPrimaryColor,
          ),
          child: const LoadingComponent(
            loadingMsg: "Loading....",
          ),
        ),
      ),
    ),
  );
}

textFieldAlert({
  required BuildContext context,
  required String title,
  required Widget widget,
  required onTap,
  required String buttonName,
  required height
}
    ) {

  return showDialog(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) => WillPopScope(
      onWillPop: () => Future.value(false),
      child: Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        backgroundColor: kPrimaryColor,
        child: Container(
          height: height,
          margin: const EdgeInsets.all(15),
          padding: const EdgeInsets.symmetric(horizontal: 15),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            color: kPrimaryColor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Padding(
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                  child: // Delete Articles
                  Text(title,
                      style: gilroyBoldTextStyle.copyWith(
                        fontSize: Dimensions.fontSizeDefault,
                      ),
                      textAlign: TextAlign.center)),
              widget,
              Container(
                margin: const EdgeInsets.only(bottom: 10, top: 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: CustomButton(
                        buttonColor: kPrimaryColor,
                        fontColor: kColorBlack,
                        onTap: (){
                          Navigator.pop(context);
                        },
                        name: "Cancel",
                        height: 40,
                        width: 35.w,
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: CustomButton(
                        buttonColor: kColorWhite,
                        fontColor: kColorBlack,
                        onTap: onTap,
                        name: "Add",
                        height: 40,
                        width: 35.w,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    ),
  );
}