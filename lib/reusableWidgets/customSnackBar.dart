import 'package:flutter/material.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/textStyles.dart';

customSnackBar(BuildContext context, String msg) => SnackBar(
  duration: const Duration(seconds: 1),
  content: Text(
    msg,
    style: gilroyRegularTextStyle.copyWith(
      fontSize: Dimensions.fontSizeSmall,
      color: kColorWhite,
    ),
  ),
  backgroundColor: kColorBlack,
  padding: const EdgeInsets.all(Dimensions.paddingDefault),
);
