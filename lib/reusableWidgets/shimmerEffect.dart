import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerEffect{
  Widget shimmer(
      BuildContext context,
      double height,
      double width,
      Color containerColor,
      Color baseColor,
      double borderRadius
      ) {

    return Shimmer.fromColors(
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(borderRadius),
              // color: kDarkPurple1
              color: containerColor
          ),
          height: height,
          width: width,
        ),
        baseColor: baseColor.withOpacity(0.6),
        // highlightColor: kDarkPurple
        highlightColor: baseColor
    );
  }

}
