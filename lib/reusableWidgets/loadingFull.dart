import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:sizer/sizer.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/images.dart';
import 'package:home/utils/textStyles.dart';

class LoadingFull extends StatelessWidget{
  final String loadingMsg;

  const LoadingFull({required this.loadingMsg});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SpinKitFadingCube(
                  color: Colors.white,
                  size: 50.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 40),
                  child: Text(
                    loadingMsg,
                    style: gilroyRegularTextStyle.copyWith(
                        fontWeight: FontWeight.w600,
                        fontSize: Dimensions.fontSizeDefault,
                        letterSpacing: 1.5
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
    );
  }
}
