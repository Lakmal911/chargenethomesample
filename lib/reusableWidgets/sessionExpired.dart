import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:home/data/base/sharedPrefs.dart';
import 'package:home/providers/mqttProvider.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/screens/login_screen.dart';

sessionExpired({required BuildContext context, required GlobalKey<ScaffoldMessengerState> scaffoldMessengerKey}){
  mSGAlert(
    context: context,
    title: "Oops!",
    msg: "Session has expired... Please Login",
    onCancel: () {
      //clear providers and shared prefs then go to login page
      clearSharedPrefs();
      final mqttProvider = Provider.of<MqttProvider>(scaffoldMessengerKey.currentContext!, listen: false);
      mqttProvider.clearMqttProviderData();

      Navigator.pushReplacement(scaffoldMessengerKey.currentContext!, MaterialPageRoute(builder: (context) => LoginScreen()),);
    },
    height: 25.h,
  );
}