import 'package:flutter/material.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/textStyles.dart';

class CustomButton extends StatelessWidget {
  final Color buttonColor;
  final Function() onTap;
  final String name;
  final Color fontColor;
  final double height;
  final double width;

  CustomButton({
    required this.buttonColor,
    required this.onTap,
    required this.name,
    required this.fontColor,
    required this.height,
    required this.width
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
          color: kPrimaryColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: kSecondaryColor.withAlpha(70),
              blurRadius: 7.0,
              spreadRadius: 3.0,
              offset: const Offset(0.0, 4.0),
            ),
          ]),
      child: ElevatedButton(
          onPressed: onTap,
          style: ElevatedButton.styleFrom(
            primary: buttonColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          child: Text(
              name,
              style: gilroyBoldTextStyle.copyWith(
                fontSize: Dimensions.fontSizeDefault,
                color: fontColor
              ),
              textAlign: TextAlign.center)),
    );
  }
}
