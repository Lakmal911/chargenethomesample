import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';
import 'package:sizer/sizer.dart';
import 'package:home/providers/commonProvider.dart';
import 'package:home/reusableWidgets/customAlert.dart';
import 'package:home/screens/home_screen.dart';
import 'package:home/screens/powerCell_screen.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/textStyles.dart';

import '../screens/shedule_screen.dart';
import '../temp/detail.dart';

class BottomNavBar extends StatelessWidget {

  int page = 0;
  BottomNavBar({Key? key, this.page = 0}) : super(key: key);

  List<Widget> pages = [
    const HomeScreen(),
    ScheduleScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    late final PageController _pageController = PageController(initialPage: page);

    return WillPopScope(
      onWillPop: ()async{
        confirmationAlert(
            context: context,
            title: "Exit",
            msg: "Are you sure you want to quit the app!",
            onCancel: (){
              Navigator.pop(context);
            },
            onYes: ()async{
              exit(0);
            },
            yesButtonName: "Yes",
            noButtonName: "No",
            height: 25.h
        );
        return false;
      },
      child: SafeArea(
          child: Consumer<CommonProvider>(
              builder: (context, commonProvider, child) {
                return Scaffold(
                  extendBody: true,
                  bottomNavigationBar: Container(
                    color: kAccentColor,
                    child: SalomonBottomBar(
                      currentIndex: commonProvider.getBottomNavCurrentIndex,
                      onTap: (i){
                        commonProvider.bottomNavCurrentIndexOnChanged = i;
                        _pageController.jumpToPage(i);
                      },
                      items: [
                        /// Home
                        SalomonBottomBarItem(
                          icon: Icon(Icons.ac_unit),
                          title: Text(
                            "Home        ",
                            style: gilroyBoldTextStyle.copyWith(
                                fontWeight: FontWeight.w600,
                                fontSize: Dimensions.fontSizeSmall,
                              color: kColorWhite
                            ),
                            textAlign: TextAlign.center,
                          ),
                          selectedColor: kColorWhite,
                        ),

                        /// Power Cell
                        SalomonBottomBarItem(
                          icon: Icon(Icons.radar),
                          title: Text(
                            "Details",
                            style: gilroyBoldTextStyle.copyWith(
                                fontWeight: FontWeight.w600,
                                fontSize: Dimensions.fontSizeSmall,
                                color: kColorWhite
                            ),
                            textAlign: TextAlign.center,
                          ),
                          selectedColor: kColorWhite,
                        ),
                      ],
                    ),
                  ),
                  body: PageView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    itemCount: pages.length,
                    scrollDirection: Axis.horizontal,
                    controller: _pageController,
                    itemBuilder: (context, index) {
                      return pages[index];
                    },
                  ),
                );
              }
          )
      ),
    );
  }
}
