import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sizer/sizer.dart';
import 'package:time_range/time_range.dart';
import 'package:home/providers/batteryCellProvider.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/textStyles.dart';
import 'customButton.dart';
import 'package:intl/intl.dart';

customFromToPicker(
    {
      required BuildContext context,
      required String title,
      required Function() onCancel,
      required Function() onYes,
      required String yesButtonName,
      required String noButtonName,
      required double height,
      required int firstTimeMinute,
      required int firstTimeHour,
      required int lastTimeMinute,
      required int lastTimeHour,
      required bool isMorning
    }
    ) {

  return showDialog<String>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) => WillPopScope(
      onWillPop: () => Future.value(false),
      child: Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        backgroundColor: kPrimaryColor,
        child: Container(
          height: height,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(15)),
            color: kPrimaryColor,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 10),
                child: Text(
                  title,
                  style: gilroyBoldTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault),
                  textAlign: TextAlign.center,
                ),
              ),
              Consumer<BatteryCellProvider>(
                  builder: (context, batteryCellProvider, child) {
                  return TimeRange(
                    fromTitle: Text(
                      'From',
                      style: gilroyBoldTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault,),
                    ),
                    toTitle: Padding(
                      padding: const EdgeInsets.only(top: Dimensions.paddingLarge),
                      child: Text(
                        'To',
                        style: gilroyBoldTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault,),
                      ),
                    ),
                    titlePadding: 20,
                    textStyle: gilroyBoldTextStyle.copyWith(fontSize: Dimensions.fontSizeDefault,),
                    activeTextStyle: gilroyBoldTextStyle.copyWith(color: kPrimaryColor, fontSize: Dimensions.fontSizeDefault,),
                    borderColor: kSecondaryColor,
                    backgroundColor: Colors.transparent,
                    activeBackgroundColor: kSecondaryColor,
                    firstTime: TimeOfDay(hour: firstTimeHour, minute: firstTimeMinute),
                    lastTime: TimeOfDay(hour: lastTimeHour, minute: lastTimeMinute),
                    timeStep: 30,
                    timeBlock: 15,
                    onRangeCompleted: (range){
                      batteryCellProvider.setSelectedTimeRange = range!;
                      print("batteryCellProvider.setSelectedTimeRange ${range.start} ${range.end}");
                      // print("isWeekDaySelected: ${batteryCellProvider.isWeekDaySelected}\n isMorning: $isMorning\n"
                      //     "weekDayMorningStartTime${batteryCellProvider.weekDayMorningStartTime}\n "
                      //     "weekDayMorningEndTime: ${batteryCellProvider.weekDayMorningEndTime}");
                    },
                  );
                }
              ),
              Container(
                margin: const EdgeInsets.all(15),
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: CustomButton(
                        buttonColor: kPrimaryColor,
                        fontColor: kColorBlack,
                        onTap: onCancel,
                        name: noButtonName,
                        height: 40,
                        width: 35.w,
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    Expanded(
                      child: CustomButton(
                        buttonColor: kColorWhite,
                        fontColor: kColorBlack,
                        onTap: onYes,
                        name: yesButtonName,
                        height: 40,
                        width: 35.w,
                      ),
                    )
                  ],
                ),
              )
            ],
          )
        ),
      ),
    ),
  );
}