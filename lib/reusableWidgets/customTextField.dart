import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sizer/sizer.dart';
import 'package:home/utils/colors.dart';
import 'package:home/utils/dimensions.dart';
import 'package:home/utils/textStyles.dart';

class CustomTextField extends StatefulWidget {

  final String? hint;
  final Widget? prefix;
  final Widget? suffix;
  final String? textFieldName;
  final bool? obscureText;
  final TextEditingController controller;
  final String? typedText;
  final Function? onSubmit;
  final bool isHint;
  final Function? onChangedText;
  final String? errorText;

  const CustomTextField({
    this.hint,
    this.prefix,
    this.suffix,
    this.textFieldName,
    this.obscureText,
    required this.controller,
    this.typedText,
    this.onSubmit,
    required this.isHint,
    this.onChangedText,
    this.errorText
  });

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {

  @override
  Widget build(BuildContext context) {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.textFieldName!.isEmpty ? const SizedBox.shrink() : Padding(
          padding: EdgeInsets.only(top: 1.h),
          child: Text(
              widget.textFieldName!,
            style: gilroyRegularTextStyle.copyWith(
              fontWeight: FontWeight.w600,
              fontSize: Dimensions.fontSizeDefault,
              letterSpacing: 1.5
            ),
          ),
        ),
        TextField(
          controller: widget.controller,
          style: gilroyRegularTextStyle.copyWith(
              fontWeight: FontWeight.w400,
              fontSize: Dimensions.fontSizeDefault,
              letterSpacing: 1.5
          ),
          textCapitalization: TextCapitalization.sentences,
          keyboardType: (widget.textFieldName == "Mobile Number") ? TextInputType.number : TextInputType.text,
          inputFormatters: (widget.textFieldName == "Mobile Number") ? [FilteringTextInputFormatter.digitsOnly] : [],
          decoration: InputDecoration(
              errorText: widget.errorText,
              enabledBorder: const UnderlineInputBorder(
                borderSide: BorderSide(color: kSecondaryColor, width: 3.0),
              ),
              focusedBorder: const UnderlineInputBorder(
                borderSide: BorderSide(color: kSecondaryColor, width: 4.0),
              ),
              labelStyle: gilroyRegularTextStyle.copyWith(
                  fontSize: Dimensions.fontSizeDefault,
                  fontWeight: FontWeight.w300,
              ),
              hintText: widget.hint,
              hintStyle: gilroyRegularTextStyle.copyWith(
                color:  kColorBlack.withOpacity(widget.isHint ? 0.5 : 1),
                fontWeight: FontWeight.w400,
                fontSize: Dimensions.fontSizeDefault,
              ),
              prefixIcon: widget.prefix,
              suffixIcon: widget.suffix
          ),
          onChanged: widget.onChangedText!(widget.controller.text),
          obscureText: widget.obscureText!,
          onSubmitted: widget.onSubmit!(),
        ),
      ],
    );
  }
}
